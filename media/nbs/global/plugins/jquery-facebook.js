var facebook = {
    WaitUntilLoaded: function(){
        FB.getLoginStatus(function(response) {
            if(response.status == 'connected'){
                facebook.count_friend();
            }else{
                setTimeout("facebook.WaitUntilLoaded()",1000);
            }
        });
    },
    count_friend: function(){
        FB.api(
                "/me/friends",
                function (response) {
                    if (response && !response.error) {
                        var a= response.summary;
                        $("#dashboard_stats_total_fb_friend").text(a.total_count);
                    }
                }
            );
        
    },
    facebook_invite_dialog: function(){
    	FB.ui({method: 'apprequests',
          		message: 'Invite rakan Facebook anda'
        	}, function(response){
            	console.log(response);
        	});
    },
    checkLoginState: function() {
        FB.getLoginStatus(function(response) {
          facebook.statusChangeCallback(response);
        });
      },
    statusChangeCallback: function(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      var fb_status = response.authResponse;
      var url = window.location.href;
      var arr = url.split("/");
      var result = arr[0] + "//" + arr[2];
      $.ajax({
        url: result+'/facebook_auth/dologin/',
        dataType: 'json',
        type: 'POST',
        data: { accessToken: fb_status.accessToken, userID: fb_status.userID, expiresIn: fb_status.expiresIn, signedRequest: fb_status.signedRequest },
        success: function(data){
            var is_logged_in = data.is_logged_in;
            console.log(data)
            if(is_logged_in == 'yes'){
                location.reload();
            }
        }
      })
        
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      alert('not logged')
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }
}

$(function(){
    
})