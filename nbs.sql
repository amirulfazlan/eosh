-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2016 at 12:04 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('54c918836fb22dc9790b493b76ba22db', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', 1464599864, 'a:2:{s:9:"user_data";s:0:"";s:10:"flexi_auth";a:7:{s:15:"user_identifier";s:30:"amirulfazlanzakariah@gmail.com";s:7:"user_id";s:1:"4";s:5:"admin";b:1;s:5:"group";a:1:{i:3;s:12:"Master Admin";}s:10:"privileges";a:12:{i:12;s:12:"Apps Setting";i:1;s:10:"View Users";i:3;s:15:"View Privileges";i:4;s:18:"Insert User Groups";i:5;s:17:"Insert Privileges";i:6;s:12:"Update Users";i:7;s:18:"Update User Groups";i:8;s:17:"Update Privileges";i:9;s:12:"Delete Users";i:10;s:18:"Delete User Groups";i:11;s:17:"Delete Privileges";i:2;s:16:"View User Groups";}s:22:"logged_in_via_password";b:1;s:19:"login_session_token";s:40:"8517e09e0e10c61cff97bf04fc60913a0d2a7132";}}'),
('df8c2fec68d5dec5bdd870afd9af2c23', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36', 1464638028, 'a:2:{s:9:"user_data";s:0:"";s:10:"flexi_auth";a:7:{s:15:"user_identifier";s:30:"amirulfazlanzakariah@gmail.com";s:7:"user_id";s:1:"4";s:5:"admin";b:1;s:5:"group";a:1:{i:3;s:12:"Master Admin";}s:10:"privileges";a:12:{i:12;s:12:"Apps Setting";i:1;s:10:"View Users";i:3;s:15:"View Privileges";i:4;s:18:"Insert User Groups";i:5;s:17:"Insert Privileges";i:6;s:12:"Update Users";i:7;s:18:"Update User Groups";i:8;s:17:"Update Privileges";i:9;s:12:"Delete Users";i:10;s:18:"Delete User Groups";i:11;s:17:"Delete Privileges";i:2;s:16:"View User Groups";}s:22:"logged_in_via_password";b:1;s:19:"login_session_token";s:40:"5699f04856ce1246232adaf9b6eb6a541906dac1";}}');

-- --------------------------------------------------------

--
-- Table structure for table `ketua_pegawai_insiden`
--

CREATE TABLE `ketua_pegawai_insiden` (
  `id_ketua_pegawai_insiden` varchar(255) NOT NULL,
  `id_ptj` varchar(255) NOT NULL,
  `nama_kpi` varchar(255) NOT NULL,
  `tel_kpi` varchar(255) NOT NULL,
  `jawatan_kpi` varchar(255) NOT NULL,
  `bilik_kpi` varchar(255) NOT NULL,
  `status_lantikan_kpi` varchar(255) NOT NULL,
  `emel_kpi` varchar(255) NOT NULL,
  `ukmper_kpi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_insiden`
--

CREATE TABLE `pegawai_insiden` (
  `id_pegawai_insiden` varchar(255) NOT NULL,
  `id_ptj` varchar(255) NOT NULL,
  `nama_pegawai_insiden` varchar(255) NOT NULL,
  `bilik_pegawai_insiden` varchar(255) NOT NULL,
  `bahagian_pegawai_insiden` varchar(255) NOT NULL,
  `emel_pegawai_insiden` varchar(255) NOT NULL,
  `ukmper_pegawai_insiden` varchar(255) NOT NULL,
  `status_lantikan` varchar(255) NOT NULL,
  `tel_pegawai_insiden` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemantauan`
--

CREATE TABLE `pemantauan` (
  `id_ptj` varchar(255) NOT NULL,
  `id_kpi` varchar(255) NOT NULL,
  `gambar_pembuktian` varchar(255) NOT NULL,
  `catatan_pembuktian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penilaian_khusus`
--

CREATE TABLE `penilaian_khusus` (
  `id_penilaian_khusus` varchar(255) NOT NULL,
  `id_ptj` varchar(255) NOT NULL,
  `bil_audit` int(11) NOT NULL,
  `skop_audit` varchar(255) NOT NULL,
  `program_pusat` varchar(255) NOT NULL,
  `bilik_aras` varchar(255) NOT NULL,
  `zon` varchar(255) NOT NULL,
  `tarikh_audit` date NOT NULL,
  `masa_audit` time NOT NULL,
  `a1.1` int(11) NOT NULL,
  `a1.2` int(11) NOT NULL,
  `a1.3` int(11) NOT NULL,
  `a1.4` int(11) NOT NULL,
  `a1.5` int(11) NOT NULL,
  `a1.6` int(11) NOT NULL,
  `a1.7` int(11) NOT NULL,
  `a1.8` int(11) NOT NULL,
  `a1.9` int(11) NOT NULL,
  `a2.1` int(11) NOT NULL,
  `a2.2` int(11) NOT NULL,
  `a2.3` int(11) NOT NULL,
  `a2.4` int(11) NOT NULL,
  `a3.1` int(11) NOT NULL,
  `a3.2` int(11) NOT NULL,
  `a4.1` int(11) NOT NULL,
  `a4.2` int(11) NOT NULL,
  `a4.3` int(11) NOT NULL,
  `a4.4` int(11) NOT NULL,
  `a4.5` int(11) NOT NULL,
  `a5.1` int(11) NOT NULL,
  `a5.2` int(11) NOT NULL,
  `a5.3` int(11) NOT NULL,
  `a5.4` int(11) NOT NULL,
  `a6.1` int(11) NOT NULL,
  `a6.2` int(11) NOT NULL,
  `a6.3` int(11) NOT NULL,
  `a7.1` int(11) NOT NULL,
  `a7.2` int(11) NOT NULL,
  `a7.3` int(11) NOT NULL,
  `a7.4` int(11) NOT NULL,
  `b1.1` int(11) NOT NULL,
  `b1.2` int(11) NOT NULL,
  `b1.3` int(11) NOT NULL,
  `b1.4` int(11) NOT NULL,
  `b2.1` int(11) NOT NULL,
  `b2.2` int(11) NOT NULL,
  `b2.3` int(11) NOT NULL,
  `b3.1` int(11) NOT NULL,
  `b3.2` int(11) NOT NULL,
  `b4.1` int(11) NOT NULL,
  `b4.2` int(11) NOT NULL,
  `b5.1` int(11) NOT NULL,
  `b5.2` int(11) NOT NULL,
  `b5.3` int(11) NOT NULL,
  `b5.4` int(11) NOT NULL,
  `b6.1` int(11) NOT NULL,
  `b6.2` int(11) NOT NULL,
  `b6.3` int(11) NOT NULL,
  `b6.4` int(11) NOT NULL,
  `b6.5` int(11) NOT NULL,
  `b6.6` int(11) NOT NULL,
  `b6.7` int(11) NOT NULL,
  `b7.1` int(11) NOT NULL,
  `c1.1` int(11) NOT NULL,
  `c1.2` int(11) NOT NULL,
  `c1.3` int(11) NOT NULL,
  `c1.4` int(11) NOT NULL,
  `c1.5` int(11) NOT NULL,
  `c1.6` int(11) NOT NULL,
  `c1.7` int(11) NOT NULL,
  `d1.1` int(11) NOT NULL,
  `d1.2` int(11) NOT NULL,
  `d1.3` int(11) NOT NULL,
  `d1.4` int(11) NOT NULL,
  `d1.5` int(11) NOT NULL,
  `d1.6` int(11) NOT NULL,
  `d1.7` int(11) NOT NULL,
  `d1.8` int(11) NOT NULL,
  `d1.9` int(11) NOT NULL,
  `d1.10` int(11) NOT NULL,
  `e1.1` int(11) NOT NULL,
  `e1.2` int(11) NOT NULL,
  `e1.3` int(11) NOT NULL,
  `e1.4` int(11) NOT NULL,
  `e1.5` int(11) NOT NULL,
  `e1.6` int(11) NOT NULL,
  `e1.7` int(11) NOT NULL,
  `e2.1` int(11) NOT NULL,
  `e2.2` int(11) NOT NULL,
  `e2.3` int(11) NOT NULL,
  `e2.4` int(11) NOT NULL,
  `e2.5` int(11) NOT NULL,
  `e2.6` int(11) NOT NULL,
  `e2.7` int(11) NOT NULL,
  `e2.8` int(11) NOT NULL,
  `e2.9` int(11) NOT NULL,
  `e2.10` int(11) NOT NULL,
  `e2.11` int(11) NOT NULL,
  `e2.12` int(11) NOT NULL,
  `e2.13` int(11) NOT NULL,
  `e2.14` int(11) NOT NULL,
  `e2.15` int(11) NOT NULL,
  `e2.16` int(11) NOT NULL,
  `e2.17` int(11) NOT NULL,
  `e2.18` int(11) NOT NULL,
  `e2.19` int(11) NOT NULL,
  `e2.20` int(11) NOT NULL,
  `e3.1` int(11) NOT NULL,
  `e3.2` int(11) NOT NULL,
  `e3.3` int(11) NOT NULL,
  `e3.4` int(11) NOT NULL,
  `e3.5` int(11) NOT NULL,
  `e3.6` int(11) NOT NULL,
  `e3.7` int(11) NOT NULL,
  `e4.1` int(11) NOT NULL,
  `e4.2` int(11) NOT NULL,
  `e4.3` int(11) NOT NULL,
  `e4.5` int(11) NOT NULL,
  `e4.6` int(11) NOT NULL,
  `e5.1` int(11) NOT NULL,
  `e5.2` int(11) NOT NULL,
  `e5.3` int(11) NOT NULL,
  `e5.4` int(11) NOT NULL,
  `e5.5` int(11) NOT NULL,
  `e5.6` int(11) NOT NULL,
  `e6.1` int(11) NOT NULL,
  `e6.2` int(11) NOT NULL,
  `e6.3` int(11) NOT NULL,
  `e6.4` int(11) NOT NULL,
  `e6.5` int(11) NOT NULL,
  `e6.6` int(11) NOT NULL,
  `e6.7` int(11) NOT NULL,
  `e6.8` int(11) NOT NULL,
  `e7.1` int(11) NOT NULL,
  `salah` int(11) NOT NULL,
  `e7.2` int(11) NOT NULL,
  `e7.3` int(11) NOT NULL,
  `e8.1` int(11) NOT NULL,
  `e8.2` int(11) NOT NULL,
  `e8.3` int(11) NOT NULL,
  `e8.4` int(11) NOT NULL,
  `e8.5` int(11) NOT NULL,
  `e8.6` int(11) NOT NULL,
  `e8.7` int(11) NOT NULL,
  `e9.1` int(11) NOT NULL,
  `e9.2` int(11) NOT NULL,
  `e9.3` int(11) NOT NULL,
  `e9.4` int(11) NOT NULL,
  `f1.1` int(11) NOT NULL,
  `f1.2` int(11) NOT NULL,
  `f1.3` int(11) NOT NULL,
  `f1.4` int(11) NOT NULL,
  `g1.1` int(11) NOT NULL,
  `g1.2` int(11) NOT NULL,
  `h1.1` int(11) NOT NULL,
  `h1.2` int(11) NOT NULL,
  `h1.3` int(11) NOT NULL,
  `h1.4` int(11) NOT NULL,
  `jumlah_markah_a` int(11) NOT NULL,
  `jumlah_markah_b` int(11) NOT NULL,
  `jumlah_markah_c` int(11) NOT NULL,
  `jumlah_markah_d` int(11) NOT NULL,
  `jumlah_markah_e` int(11) NOT NULL,
  `jumlah_markah_f` int(11) NOT NULL,
  `jumlah_markah_g` int(11) NOT NULL,
  `jumlah_markah_h` int(11) NOT NULL,
  `jumlah_markah_keseluruhan` int(11) NOT NULL,
  `catatan_khusus` int(11) NOT NULL,
  `gred_khusus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penilaian_umum`
--

CREATE TABLE `penilaian_umum` (
  `id_penilaian_umum` int(255) UNSIGNED NOT NULL,
  `id_ptj` varchar(255) NOT NULL,
  `bil_audit` int(11) NOT NULL,
  `skop_audit` varchar(255) NOT NULL,
  `program_pusat` varchar(255) NOT NULL,
  `bilik_aras` varchar(255) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `zon` varchar(255) NOT NULL,
  `tarikh_audit` date NOT NULL,
  `masa_audit` time NOT NULL,
  `tarikh_masa_audit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `a1_1` int(11) NOT NULL,
  `a1_2` int(11) NOT NULL,
  `a1_3` int(11) NOT NULL,
  `a1_4` int(11) NOT NULL,
  `a1_5` int(11) NOT NULL,
  `a1_6` int(11) NOT NULL,
  `a1_7` int(11) NOT NULL,
  `a1_8` int(11) NOT NULL,
  `a1_9` int(11) NOT NULL,
  `a2_1` int(11) NOT NULL,
  `a2_2` int(11) NOT NULL,
  `a2_3` int(11) NOT NULL,
  `a2_4` int(11) NOT NULL,
  `a2_5` int(11) NOT NULL,
  `a2_6` int(11) NOT NULL,
  `a2_7` int(11) NOT NULL,
  `a2_8` int(11) NOT NULL,
  `a2_9` int(11) NOT NULL,
  `a3_1` int(11) NOT NULL,
  `a3_2` int(11) NOT NULL,
  `a3_3` int(11) NOT NULL,
  `a3_4` int(11) NOT NULL,
  `a4_1` int(11) NOT NULL,
  `a4_2` int(11) NOT NULL,
  `a5_1` int(11) NOT NULL,
  `a5_2` int(11) NOT NULL,
  `a5_3` int(11) NOT NULL,
  `a5_4` int(11) NOT NULL,
  `a5_5` int(11) NOT NULL,
  `a6_1` int(11) NOT NULL,
  `a6_2` int(11) NOT NULL,
  `a6_3` int(11) NOT NULL,
  `a6_4` int(11) NOT NULL,
  `a7_1` int(11) NOT NULL,
  `a7_2` int(11) NOT NULL,
  `a7_3` int(11) NOT NULL,
  `a8_1` int(11) NOT NULL,
  `a8_2` int(11) NOT NULL,
  `a8_3` int(11) NOT NULL,
  `a8_4` int(11) NOT NULL,
  `a9_1` int(11) NOT NULL,
  `a9_2` int(11) NOT NULL,
  `a9_3` int(11) NOT NULL,
  `a9_4` int(11) NOT NULL,
  `a9_5` int(11) NOT NULL,
  `b1_1` int(11) NOT NULL,
  `b1_2` int(11) NOT NULL,
  `b1_3` int(11) NOT NULL,
  `b1_4` int(11) NOT NULL,
  `b2_1` int(11) NOT NULL,
  `b2_2` int(11) NOT NULL,
  `b2_3` int(11) NOT NULL,
  `b2_4` int(11) NOT NULL,
  `b3_1` int(11) NOT NULL,
  `b3_2` int(11) NOT NULL,
  `b4_1` int(11) NOT NULL,
  `b4_2` int(11) NOT NULL,
  `b4_3` int(11) NOT NULL,
  `b4_4` int(11) NOT NULL,
  `b5_1` int(11) NOT NULL,
  `b5_2` int(11) NOT NULL,
  `b6_1` int(11) NOT NULL,
  `b6_2` int(11) NOT NULL,
  `b7_1` int(11) NOT NULL,
  `c1_1` int(11) NOT NULL,
  `c1_2` int(11) NOT NULL,
  `c1_3` int(11) NOT NULL,
  `c1_4` int(11) NOT NULL,
  `c1_5` int(11) NOT NULL,
  `c1_6` int(11) NOT NULL,
  `d1_1` int(11) NOT NULL,
  `d1_2` int(11) NOT NULL,
  `d1_3` int(11) NOT NULL,
  `d1_4` int(11) NOT NULL,
  `d1_5` int(11) NOT NULL,
  `d1_6` int(11) NOT NULL,
  `e1_1` int(11) NOT NULL,
  `e1_2` int(11) NOT NULL,
  `e1_3` int(11) NOT NULL,
  `e1_4` int(11) NOT NULL,
  `e1_5` int(11) NOT NULL,
  `e1_6` int(11) NOT NULL,
  `e1_7` int(11) NOT NULL,
  `f1_1` int(11) NOT NULL,
  `f1_2` int(11) NOT NULL,
  `f1_3` int(11) NOT NULL,
  `f1_4` int(11) NOT NULL,
  `f1_5` int(11) NOT NULL,
  `f2_1` int(11) NOT NULL,
  `f2_2` int(11) NOT NULL,
  `f2_3` int(11) NOT NULL,
  `f3_1` int(11) NOT NULL,
  `f3_2` int(11) NOT NULL,
  `f4_1` int(11) NOT NULL,
  `f4_2` int(11) NOT NULL,
  `f4_3` int(11) NOT NULL,
  `f4_4` int(11) NOT NULL,
  `f5_1` int(11) NOT NULL,
  `f5_2` int(11) NOT NULL,
  `f5_3` int(11) NOT NULL,
  `f5_4` int(11) NOT NULL,
  `f6_1` int(11) NOT NULL,
  `f6_2` int(11) NOT NULL,
  `g1_1` int(11) NOT NULL,
  `g1_2` int(11) NOT NULL,
  `h1_1` int(11) NOT NULL,
  `h1_2` int(11) NOT NULL,
  `i1_1` int(11) NOT NULL,
  `i1_2` int(11) NOT NULL,
  `i1_3` int(11) NOT NULL,
  `i1_4` int(11) NOT NULL,
  `jumlah_markah_a` int(11) NOT NULL,
  `jumlah_markah_b` int(11) NOT NULL,
  `jumlah_markah_c` int(11) NOT NULL,
  `jumlah_markah_d` int(11) NOT NULL,
  `jumlah_markah_e` int(11) NOT NULL,
  `jumlah_markah_f` int(11) NOT NULL,
  `jumlah_markah_g` int(11) NOT NULL,
  `jumlah_markah_h` int(11) NOT NULL,
  `jumlah_markah_i` int(11) NOT NULL,
  `jumlah_markah_keseluruhan` int(11) NOT NULL,
  `peratus_markah` int(11) NOT NULL,
  `catatan_umum` int(11) NOT NULL,
  `gred_umum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penilaian_umum`
--

INSERT INTO `penilaian_umum` (`id_penilaian_umum`, `id_ptj`, `bil_audit`, `skop_audit`, `program_pusat`, `bilik_aras`, `lokasi`, `zon`, `tarikh_audit`, `masa_audit`, `tarikh_masa_audit`, `a1_1`, `a1_2`, `a1_3`, `a1_4`, `a1_5`, `a1_6`, `a1_7`, `a1_8`, `a1_9`, `a2_1`, `a2_2`, `a2_3`, `a2_4`, `a2_5`, `a2_6`, `a2_7`, `a2_8`, `a2_9`, `a3_1`, `a3_2`, `a3_3`, `a3_4`, `a4_1`, `a4_2`, `a5_1`, `a5_2`, `a5_3`, `a5_4`, `a5_5`, `a6_1`, `a6_2`, `a6_3`, `a6_4`, `a7_1`, `a7_2`, `a7_3`, `a8_1`, `a8_2`, `a8_3`, `a8_4`, `a9_1`, `a9_2`, `a9_3`, `a9_4`, `a9_5`, `b1_1`, `b1_2`, `b1_3`, `b1_4`, `b2_1`, `b2_2`, `b2_3`, `b2_4`, `b3_1`, `b3_2`, `b4_1`, `b4_2`, `b4_3`, `b4_4`, `b5_1`, `b5_2`, `b6_1`, `b6_2`, `b7_1`, `c1_1`, `c1_2`, `c1_3`, `c1_4`, `c1_5`, `c1_6`, `d1_1`, `d1_2`, `d1_3`, `d1_4`, `d1_5`, `d1_6`, `e1_1`, `e1_2`, `e1_3`, `e1_4`, `e1_5`, `e1_6`, `e1_7`, `f1_1`, `f1_2`, `f1_3`, `f1_4`, `f1_5`, `f2_1`, `f2_2`, `f2_3`, `f3_1`, `f3_2`, `f4_1`, `f4_2`, `f4_3`, `f4_4`, `f5_1`, `f5_2`, `f5_3`, `f5_4`, `f6_1`, `f6_2`, `g1_1`, `g1_2`, `h1_1`, `h1_2`, `i1_1`, `i1_2`, `i1_3`, `i1_4`, `jumlah_markah_a`, `jumlah_markah_b`, `jumlah_markah_c`, `jumlah_markah_d`, `jumlah_markah_e`, `jumlah_markah_f`, `jumlah_markah_g`, `jumlah_markah_h`, `jumlah_markah_i`, `jumlah_markah_keseluruhan`, `peratus_markah`, `catatan_umum`, `gred_umum`) VALUES
(1, '', 1, '5S', 'cubaan ', 'cubaan ', 'cubaan ', 'cubaan ', '0000-00-00', '00:00:00', '2016-05-30 19:55:47', 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 24, 13, 4, 3, 7, 20, 2, 0, 2, 75, 69, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ptj`
--

CREATE TABLE `ptj` (
  `id_ptj` int(255) NOT NULL,
  `nama_ptj` varchar(255) NOT NULL,
  `alamat_ptj` varchar(255) NOT NULL,
  `jenis_ptj` varchar(255) NOT NULL,
  `gambar_ptj` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rd_menu`
--

CREATE TABLE `rd_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `function` varchar(255) DEFAULT NULL,
  `visible` int(3) DEFAULT '1' COMMENT 'true or false',
  `page_title` varchar(255) DEFAULT NULL,
  `page_title_small` varchar(255) DEFAULT NULL,
  `icon-class` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rd_menu`
--

INSERT INTO `rd_menu` (`id`, `name`, `parent_id`, `sort`, `group_id`, `link`, `controller`, `function`, `visible`, `page_title`, `page_title_small`, `icon-class`) VALUES
(1, 'Halaman Utama', 0, 0, '3', 'javascript: void();', '', '', 1, 'Selamat Datang Ke E-OSH UKM', '', 'icon-home'),
(2, 'Pusat Tanggungjawab (PTJ)', 0, 2, '3', 'ci_controller', 'auth_admin', 'dashboard', 1, 'Pengurusan Pusat Tanggungjawab', 'Admin Dashboard', ''),
(3, 'Ketua Pegawai Insiden', 0, 3, '3', 'javascript: void();', '', '', 1, 'Pengurusan Ketua Pegawai Insiden', 'Settings', ''),
(4, 'Pegawai Insiden', 0, 4, '3', 'javascript: void();', '', '', 1, 'Pengurusan Pegawai Insiden', 'Setting', ''),
(5, 'Pemantauan Dan Pembuktian', 0, 5, '3', 'ci_controller', 'auth_admin', 'update_user_account', 1, 'Pemantauan Dan Pembuktian Penambahbaikan', 'Privileges', ''),
(6, 'Laporan', 0, 6, '3', 'ci_controller', 'auth_admin', 'update_user_privileges', 1, 'Laporan', 'Privileges', ''),
(7, 'Daftar Ketua Pegawai Insiden', 3, 1, '3', 'ci_controller', 'auth_admin', 'manage_user_groups', 1, 'Daftar Ketua Pegawai Insiden', 'Privileges', ''),
(8, 'Senarai Ketua Pegawai Insiden', 3, 0, '3', 'ci_controller', 'auth_admin', 'update_group_privileges', 1, 'Senarai Ketua Pegawai Insiden', 'Privileges', ''),
(9, 'Daftar Pegawai Insiden', 4, 0, '3', 'ci_controller', 'auth_admin', 'update_user_group', 1, 'Daftar Pegawai Insiden', 'Privileges', ''),
(10, 'Senarai Pegawai Insiden', 4, 2, '3', 'ci_controller', 'auth_admin', 'manage_user_accounts', 1, 'Senarai Pegawai Insiden', 'Privileges', ''),
(11, 'Daftar Pusat Tanggungjawab', 2, 3, '3', 'ci_controller', 'auth_admin', 'insert_user_group', 1, 'Daftar Pusat Tanggungjawab Baru', 'Privileges', ''),
(12, 'Senarai Pusat Tanggungjawab', 2, 4, '3', 'ci_controller', 'auth_admin', 'manage_privileges', 1, 'Senarai Pusat Tanggungjawab', 'Privileges', ''),
(13, 'Penilaian', 0, 5, '3', 'ci_controller', 'auth_admin', 'insert_privilege', 1, 'Penilaian', 'Privileges', ''),
(14, 'Penilaian Umum', 13, 6, '3', 'ci_controller', 'auth_admin', 'penilaian_umum', 1, 'Penilaian Umum', 'Privileges', ''),
(15, 'Penilaian Khusus', 13, 7, '3', 'ci_controller', 'auth_admin', 'penilaian_khusus', 1, 'Penilaian Khusus', 'Privileges', ''),
(16, 'Jana Laporan PTJ', 6, 8, '3', 'ci_controller', 'auth_admin', 'delete_unactivated_users', 1, 'Jana Laporan PTJ', 'Privileges', ''),
(17, 'Apps', 3, 2, '3', 'javascript: void();', '', '', 1, 'Apps', 'Info', ''),
(18, 'Apps Info', 17, 6, '3', 'ci_controller', 'setting', 'update_app_setting', 0, 'App Info', 'Apps', ''),
(19, 'Menu', 0, 2, '3', 'ci_controller', 'auth_admin', 'menu', 0, 'Menu', 'Menu', ''),
(20, 'Muatnaik Gambar', 5, 1, '3', 'ci_controller', 'auth_admin', 'upload_picture', 1, 'Muatnaik Gambar', 'Upload', NULL),
(77, 'Halaman Utama', 1, 1, '3', NULL, NULL, NULL, 1, 'Halaman Utama', 'Halaman Utama', 'icon-home');

-- --------------------------------------------------------

--
-- Table structure for table `rd_meta`
--

CREATE TABLE `rd_meta` (
  `id` int(11) NOT NULL,
  `meta` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `remark` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rd_meta`
--

INSERT INTO `rd_meta` (`id`, `meta`, `value`, `remark`) VALUES
(1, 'title', 'E-OSH UKM', 'APPLICATION NAME '),
(2, 'logo_url', 'http://localhost/eosh/media/nbs/global/img/logo-url.png', 'LOGO IMAGE (URL)'),
(3, 'page-footer-inner', '2016 &copy; Created with love and sincerity by fazlaniza', 'FOOTER VALUE'),
(4, 'logo_login_url', 'http://localhost/eosh/media/nbs/global/img/logo-login.png', 'LOGO LOGIN IMAGE (URL)'),
(5, 'facebook_default_scope', 'public_profile,email,user_friends', NULL),
(6, 'facebook_pageid', '1570713178', NULL),
(7, 'facebook_access_token', '647667895369548|Vp5P-RW5iyeCPuig59iSSNOKFpI', NULL),
(8, 'facebook_app_id', '1010614935686658', NULL),
(9, 'facebook_api_secret', '5d5416ec3fd155ead22031418d182a34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rd_option`
--

CREATE TABLE `rd_option` (
  `id` int(11) NOT NULL,
  `setting` varchar(25) NOT NULL,
  `value` varchar(25) NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rd_option`
--

INSERT INTO `rd_option` (`id`, `setting`, `value`, `remark`) VALUES
(1, 'theme', 'leave', 'CURRENT THEME USE'),
(2, 'controller', 'leave', 'CURRENT CONTROLLER USE');

-- --------------------------------------------------------

--
-- Table structure for table `rosh_ukm`
--

CREATE TABLE `rosh_ukm` (
  `id_pegawai_rosh` varchar(255) NOT NULL,
  `nama_pegawai_rosh` varchar(255) NOT NULL,
  `ukmper_pegawai_rosh` varchar(255) NOT NULL,
  `jawatan_pegawai_rosh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simpanan_audit`
--

CREATE TABLE `simpanan_audit` (
  `id_audit` varchar(255) NOT NULL,
  `bil_audit` int(11) NOT NULL,
  `tarikh_audit` date NOT NULL,
  `masa_audit` time NOT NULL,
  `jumlah_markah_keseluruhan` int(11) NOT NULL,
  `gred` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `uacc_id` int(11) UNSIGNED NOT NULL,
  `uacc_group_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_email` varchar(100) NOT NULL DEFAULT '',
  `uacc_username` varchar(15) NOT NULL DEFAULT '',
  `uacc_password` varchar(60) NOT NULL DEFAULT '',
  `uacc_raw_password` varchar(20) NOT NULL,
  `uacc_ip_address` varchar(40) NOT NULL DEFAULT '',
  `uacc_salt` varchar(40) NOT NULL DEFAULT '',
  `uacc_activation_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_forgotten_password_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_forgotten_password_expire` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uacc_update_email_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_update_email` varchar(100) NOT NULL DEFAULT '',
  `uacc_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_suspend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_fail_login_attempts` smallint(5) NOT NULL DEFAULT '0',
  `uacc_fail_login_ip_address` varchar(40) NOT NULL DEFAULT '',
  `uacc_date_fail_login_ban` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Time user is banned until due to repeated failed logins',
  `uacc_date_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uacc_date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`uacc_id`, `uacc_group_fk`, `uacc_email`, `uacc_username`, `uacc_password`, `uacc_raw_password`, `uacc_ip_address`, `uacc_salt`, `uacc_activation_token`, `uacc_forgotten_password_token`, `uacc_forgotten_password_expire`, `uacc_update_email_token`, `uacc_update_email`, `uacc_active`, `uacc_suspend`, `uacc_fail_login_attempts`, `uacc_fail_login_ip_address`, `uacc_date_fail_login_ban`, `uacc_date_last_login`, `uacc_date_added`) VALUES
(1, 3, 'adminn@admin.com', 'lololo', '$2a$08$lSOQGNqwBFUEDTxm2Y.hb.mfPEAt/iiGY9kJsZsd4ekLJXLD.tCrq', '', '', 'XKVT29q2Jr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 1, '', '0000-00-00 00:00:00', '2015-12-18 03:30:24', '2011-01-01 00:00:00'),
(2, 2, 'moderator@moderator.com', 'moderator', '$2a$08$q.0ZhovC5ZkVpkBLJ.Mz.O4VjWsKohYckJNx4KM40MXdP/zEZpwcm', '', '0.0.0.0', 'ZC38NNBPjF', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2012-04-10 21:58:02', '2011-08-04 16:49:07'),
(3, 1, 'public@public.com', 'public', '$2a$08$GlxQ00VKlev2t.CpvbTOlepTJljxF2RocJghON37r40mbDl4vJLv2', '', '0.0.0.0', 'CDNFV6dHmn', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2012-04-10 22:01:41', '2011-09-15 12:24:45'),
(4, 3, 'amirulfazlanzakariah@gmail.com', 'A148370', '$2a$08$CDCpLOEyX.zF6aeCXGXhUuCPYxK6Ky4SgJlND.hj.lWI2Q/P.FGwW', '1q2w3er4t5', '::1', 'wwpkp4zsKk', '0ee38b2195a31d66e5c6ece9716c9727b60e85e0', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-05-30 21:53:50', '2015-12-18 05:47:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `uadd_id` int(11) NOT NULL,
  `uadd_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `uadd_alias` varchar(50) NOT NULL DEFAULT '',
  `uadd_recipient` varchar(100) NOT NULL DEFAULT '',
  `uadd_phone` varchar(25) NOT NULL DEFAULT '',
  `uadd_company` varchar(75) NOT NULL DEFAULT '',
  `uadd_address_01` varchar(100) NOT NULL DEFAULT '',
  `uadd_address_02` varchar(100) NOT NULL DEFAULT '',
  `uadd_city` varchar(50) NOT NULL DEFAULT '',
  `uadd_county` varchar(50) NOT NULL DEFAULT '',
  `uadd_post_code` varchar(25) NOT NULL DEFAULT '',
  `uadd_country` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`uadd_id`, `uadd_uacc_fk`, `uadd_alias`, `uadd_recipient`, `uadd_phone`, `uadd_company`, `uadd_address_01`, `uadd_address_02`, `uadd_city`, `uadd_county`, `uadd_post_code`, `uadd_country`) VALUES
(1, 4, 'Home', 'Joe Public', '0123456789', '', '123', '', 'My City', 'My County', 'My Post Code', 'My Country'),
(2, 4, 'Work', 'Joe Public', '0123456789', 'Flexi', '321', '', 'My Work City', 'My Work County', 'My Work Post Code', 'My Work Country');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `ugrp_id` smallint(5) NOT NULL,
  `ugrp_name` varchar(20) NOT NULL DEFAULT '',
  `ugrp_desc` varchar(100) NOT NULL DEFAULT '',
  `ugrp_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`ugrp_id`, `ugrp_name`, `ugrp_desc`, `ugrp_admin`) VALUES
(1, 'Pegawai Insiden', 'Public User : has no admin access rights. luls', 0),
(2, 'ROSH-UKM', 'Admin Moderator : has partial admin access rights.', 1),
(3, 'Master Admin', 'Master Admin : has full admin access rights.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login_sessions`
--

CREATE TABLE `user_login_sessions` (
  `usess_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `usess_series` varchar(40) NOT NULL DEFAULT '',
  `usess_token` varchar(40) NOT NULL DEFAULT '',
  `usess_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login_sessions`
--

INSERT INTO `user_login_sessions` (`usess_uacc_fk`, `usess_series`, `usess_token`, `usess_login_date`) VALUES
(4, '', '5699f04856ce1246232adaf9b6eb6a541906dac1', '2016-05-30 21:55:47'),
(4, '', '8517e09e0e10c61cff97bf04fc60913a0d2a7132', '2016-05-30 11:18:06');

-- --------------------------------------------------------

--
-- Table structure for table `user_privileges`
--

CREATE TABLE `user_privileges` (
  `upriv_id` smallint(5) NOT NULL,
  `upriv_name` varchar(20) NOT NULL DEFAULT '',
  `upriv_desc` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privileges`
--

INSERT INTO `user_privileges` (`upriv_id`, `upriv_name`, `upriv_desc`) VALUES
(1, 'View Users', 'User can view user account details.'),
(2, 'View User Groups', 'User can view user groups.'),
(3, 'View Privileges', 'User can view privileges.'),
(4, 'Insert User Groups', 'User can insert new user groups.'),
(5, 'Insert Privileges', 'User can insert privileges.'),
(6, 'Update Users', 'User can update user account details.'),
(7, 'Update User Groups', 'User can update user groups.'),
(8, 'Update Privileges', 'User can update user privileges.'),
(9, 'Delete Users', 'User can delete user accounts.'),
(10, 'Delete User Groups', 'User can delete user groups.'),
(11, 'Delete Privileges', 'User can delete user privileges.'),
(12, 'Apps Setting', 'Application Info');

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege_groups`
--

CREATE TABLE `user_privilege_groups` (
  `upriv_groups_id` smallint(5) UNSIGNED NOT NULL,
  `upriv_groups_ugrp_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `upriv_groups_upriv_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privilege_groups`
--

INSERT INTO `user_privilege_groups` (`upriv_groups_id`, `upriv_groups_ugrp_fk`, `upriv_groups_upriv_fk`) VALUES
(1, 3, 1),
(3, 3, 3),
(4, 3, 4),
(5, 3, 5),
(6, 3, 6),
(7, 3, 7),
(8, 3, 8),
(9, 3, 9),
(10, 3, 10),
(11, 3, 11),
(12, 2, 2),
(13, 2, 4),
(14, 2, 5),
(15, 3, 2),
(16, 3, 12);

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege_users`
--

CREATE TABLE `user_privilege_users` (
  `upriv_users_id` smallint(5) NOT NULL,
  `upriv_users_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `upriv_users_upriv_fk` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privilege_users`
--

INSERT INTO `user_privilege_users` (`upriv_users_id`, `upriv_users_uacc_fk`, `upriv_users_upriv_fk`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(12, 2, 1),
(13, 2, 2),
(14, 2, 3),
(15, 2, 6),
(21, 1, 11),
(22, 1, 10),
(23, 4, 12),
(24, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `upro_id` int(11) NOT NULL,
  `upro_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `upro_company` varchar(50) NOT NULL DEFAULT '',
  `upro_first_name` varchar(50) NOT NULL DEFAULT '',
  `upro_last_name` varchar(50) NOT NULL DEFAULT '',
  `upro_phone` varchar(25) NOT NULL DEFAULT '',
  `upro_newsletter` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`upro_id`, `upro_uacc_fk`, `upro_company`, `upro_first_name`, `upro_last_name`, `upro_phone`, `upro_newsletter`) VALUES
(1, 1, '', 'John', 'Adminn', '0123456789', 0),
(2, 2, '', 'Jim', 'Moderator', '0123465798', 0),
(3, 3, '', 'Joe', 'Public', '0123456789', 0),
(4, 4, '', 'Amirul', 'Fazlan', '0177978702', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity` (`last_activity`);

--
-- Indexes for table `penilaian_umum`
--
ALTER TABLE `penilaian_umum`
  ADD PRIMARY KEY (`id_penilaian_umum`);

--
-- Indexes for table `ptj`
--
ALTER TABLE `ptj`
  ADD PRIMARY KEY (`id_ptj`);

--
-- Indexes for table `rd_menu`
--
ALTER TABLE `rd_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rd_meta`
--
ALTER TABLE `rd_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rd_option`
--
ALTER TABLE `rd_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`uacc_id`),
  ADD UNIQUE KEY `uacc_id` (`uacc_id`),
  ADD KEY `uacc_group_fk` (`uacc_group_fk`),
  ADD KEY `uacc_email` (`uacc_email`),
  ADD KEY `uacc_username` (`uacc_username`),
  ADD KEY `uacc_fail_login_ip_address` (`uacc_fail_login_ip_address`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`uadd_id`),
  ADD UNIQUE KEY `uadd_id` (`uadd_id`),
  ADD KEY `uadd_uacc_fk` (`uadd_uacc_fk`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`ugrp_id`),
  ADD UNIQUE KEY `ugrp_id` (`ugrp_id`) USING BTREE;

--
-- Indexes for table `user_login_sessions`
--
ALTER TABLE `user_login_sessions`
  ADD PRIMARY KEY (`usess_token`),
  ADD UNIQUE KEY `usess_token` (`usess_token`);

--
-- Indexes for table `user_privileges`
--
ALTER TABLE `user_privileges`
  ADD PRIMARY KEY (`upriv_id`),
  ADD UNIQUE KEY `upriv_id` (`upriv_id`) USING BTREE;

--
-- Indexes for table `user_privilege_groups`
--
ALTER TABLE `user_privilege_groups`
  ADD PRIMARY KEY (`upriv_groups_id`),
  ADD UNIQUE KEY `upriv_groups_id` (`upriv_groups_id`) USING BTREE,
  ADD KEY `upriv_groups_ugrp_fk` (`upriv_groups_ugrp_fk`),
  ADD KEY `upriv_groups_upriv_fk` (`upriv_groups_upriv_fk`);

--
-- Indexes for table `user_privilege_users`
--
ALTER TABLE `user_privilege_users`
  ADD PRIMARY KEY (`upriv_users_id`),
  ADD UNIQUE KEY `upriv_users_id` (`upriv_users_id`) USING BTREE,
  ADD KEY `upriv_users_uacc_fk` (`upriv_users_uacc_fk`),
  ADD KEY `upriv_users_upriv_fk` (`upriv_users_upriv_fk`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`upro_id`),
  ADD UNIQUE KEY `upro_id` (`upro_id`),
  ADD KEY `upro_uacc_fk` (`upro_uacc_fk`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `penilaian_umum`
--
ALTER TABLE `penilaian_umum`
  MODIFY `id_penilaian_umum` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ptj`
--
ALTER TABLE `ptj`
  MODIFY `id_ptj` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rd_menu`
--
ALTER TABLE `rd_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `rd_meta`
--
ALTER TABLE `rd_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `rd_option`
--
ALTER TABLE `rd_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `uacc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `uadd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `ugrp_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_privileges`
--
ALTER TABLE `user_privileges`
  MODIFY `upriv_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_privilege_groups`
--
ALTER TABLE `user_privilege_groups`
  MODIFY `upriv_groups_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_privilege_users`
--
ALTER TABLE `user_privilege_users`
  MODIFY `upriv_users_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `upro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
