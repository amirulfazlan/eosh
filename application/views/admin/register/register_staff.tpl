<?php $this->load->view('nbs/common/header/header.tpl'); 

$post = $this->input->post();
$CI =& get_instance();

 ?>

<div class="row">    
	<div class="col-md-12">		        
		<?php if (!empty($message)) { ?>            
		<?php if(is_array($message)){ ?>             
		<?php foreach($message as $m_key => $m_val){ ?>                
		<div class="note note-success">
			<p><?php echo $m_val; ?></p>
		</div>            
		<?php } ?>            
		<?php }else{ ?>            
		<div class="note note-success">                
			<p><?php echo $message; ?></p>            
		</div>            
		<?php } ?>        
		<?php } ?>    
	</div>        
	
	<div class="col-md-12">        
		<?php if (!empty($msg_error)) { ?>            
			<?php if(is_array($msg_error)){ ?>             
				<?php foreach($msg_error as $m_key => $m_val){ ?>                
				<div class="note note-danger"><p><?php echo $m_val; ?></p></div>            
				<?php } ?>            
				<?php }else{ ?>            
				<div class="note note-danger">                
					<p><?php echo $msg_error; ?></p>            
				</div>            
				<?php } ?>       
				<?php } ?>    
	</div>        
	
	<div class="col-md-12">        
		<div class="portlet box mypasti">									
			<div class="portlet-title">										
				<div class="caption">											
					<i class="fa fa-gift"></i>DAFTAR Rosh										
				</div>																			
			</div>									
			
			<div class="portlet-body form">                                    									
				
				<!-- BEGIN FORM-->										
				<form method="post" action="<?php echo current_url(); ?>" class="form-horizontal">											
					<div class="form-body">                
                       <div class="row">                                                    
						<div class="col-md-6">														
						 <div class="form-group">															
						  <label class="control-label col-md-3">NAMA AWAL</label>															
						   <div class="col-md-9">                                                                
								<input type="text" class="form-control" id="register_first_name" name="register[first_name]"  value="<?php echo $post['register']['first_name']; ?>"/>		
						   </div>                                                            														
						 </div>													
						</div>     
						<div class="col-md-6">	
							<div class="form-group">															
								<label class="control-label col-md-3">NAMA AKHIR</label>															
								<div class="col-md-9">																 
									<input type="text" class="form-control" id="register_last_name" name="register[last_name]"  value="<?php echo $post['register']['last_name']; ?>"/>						
								</div>		
							</div>													
						</div>		
						</div>                                                
					
					<div class="row">                                                    
						<div class="col-md-6">
						  <div class="form-group" id="id_type">          
							<label class="control-label col-md-3">JAWATAN</label>         
							<div class="col-md-9">
								<input type="text" class="form-control" name="register[position]" id="register_position"  value="<?php echo $post['register']['position']; ?>"/>
							</div>  
						   </div>	
						</div>                                                     
						
						 <div class="col-md-6">														
							<div class="form-group">                    										
								<label class="control-label col-md-3">EMEL</label>                    										
								<div class="col-md-9">
									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="text" class="form-control" id="register_email" name="register[email]" value="<?php echo $post['register']['email']; ?>" /> 
									</div>
								</div>                                                                                									
							</div>													
						</div> 	                                             
					</div>                                               

					<div class="row">   
						<div class="col-md-6">														
							<div class="form-group">    
								<label class="control-label col-md-3">NO. TELEFON BIMBIT</label> 
									<div class="col-md-9">  
									<div class="input-group">   
										<span class="input-group-addon"><i class="fa fa-mobile"></i></span>
										<span class="input-group-addon" style="padding: 0px; border: none;">		
										<select class="form-control" name="register[hp_prefix]" id="register_hp_prefix" style="min-width: 80px;">       
										<option <?php if($post['register']['hp_prefix'] == '101'){ echo 'selected'; } ?> value="010">010</option>       
										<option <?php if($post['register']['hp_prefix'] == '011'){ echo 'selected'; } ?> value="011">011</option>       
										<option <?php if($post['register']['hp_prefix'] == '012'){ echo 'selected'; } ?> value="012">012</option>       
										<option <?php if($post['register']['hp_prefix'] == '013'){ echo 'selected'; } ?> value="013">013</option>       
										<option <?php if($post['register']['hp_prefix'] == '014'){ echo 'selected'; } ?> value="014">014</option>       
										<option <?php if($post['register']['hp_prefix'] == '015'){ echo 'selected'; } ?> value="015">015</option>       
										<option <?php if($post['register']['hp_prefix'] == '016'){ echo 'selected'; } ?> value="016">016</option>       
										<option <?php if($post['register']['hp_prefix'] == '017'){ echo 'selected'; } ?> value="017">017</option>       
										<option <?php if($post['register']['hp_prefix'] == '018'){ echo 'selected'; } ?> value="018">018</option>       
										<option <?php if($post['register']['hp_prefix'] == '019'){ echo 'selected'; } ?> value="019">019</option>		
										</select>															
										</span>                                                            
										
										<input type="text" class="form-control" id="hp_number" name="register[hp_number]"  onKeyPress="return numbersonly(this, event)" value="<?php echo $post['register']['hp_number']; ?>">		
									</div>     
									</div>                    									
							</div>																
						</div>  

						<div class="col-md-6">

							<div class="form-group">

								<label class="control-label col-md-3">UKMPER</label>

								<div class="col-md-9">
									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-check"></i></span>
									<input type="text" class="form-control" name="register[ukmper]" id="register_ukmper"    value="<?php echo $post['register']['ukmper']; ?>">
									</div>
								</div>

							</div>

						</div>
                          
					</div>    
					
					<div class="row"> 

						<div class="col-md-6">														
							<div class="form-group">    
								<label class="control-label col-md-3">KATA LALUAN</label> 
									<div class="col-md-9">  
										<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-key"></i></span>
										<input type="text" class="form-control" name="register[password]"  value="<?php echo $post['register']['password']; ?>">	
										</div>
									</div>                    									
							</div>																
						</div>  
                          
					</div> 
                      					
				                                                                                
					<input type="hidden" class="form-control" name="register[user_group]" id="register_user_group"  value="ROSH">
					<div class="form-actions">                                                
						<div class="btn-set pull-right">                                                                                                      
								<button type="submit" class="btn blue" id="btn_submit" name="register_new_moderator" value="submit">DAFTAR</button>        
						</div>																							
					</div>	
				 </div>
				</form>										
				
				<!-- END FORM-->									
				
			</div>    
		</div>    
	</div>
	
	<?php $this->load->view('nbs/common/footer/footer.tpl'); ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">