<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> PENILAIAN KHUSUS <span class="step-title">
								Step 1 of 4 </span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="" class="form-horizontal" name ="test_form" id="submit_form" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Borang Utama </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												A </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian A : KEPERLUAN UMUM </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step active">
												<span class="number">
												B </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian B : KESELAMATAN UMUM </span>
												</a>
											</li>
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
												<span class="number">
												C </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian C : KESELAMATAN ELEKTRIK/KEMUDAHAN ELEKTRIK </span>
												</a>
											</li>
											<li>
												<a href="#tab5" data-toggle="tab" class="step">
												<span class="number">
												D </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian D : KESELAMATAN KEBAKARAN </span>
												</a>
											</li>
											<li>
												<a href="#tab6" data-toggle="tab" class="step">
												<span class="number">
												E </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian E : MAKMAL </span>
												</a>
											</li>
											<li>
												<a href="#tab7" data-toggle="tab" class="step">
												<span class="number">
												F </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian F : KAWASAN PERSEKITARAN </span>
												</a>
											</li>
											<li>
												<a href="#tab8" data-toggle="tab" class="step">
												<span class="number">
												G </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian G : AMALAN TERBAIK </span>
												</a>
											</li>
											<li>
												<a href="#tab9" data-toggle="tab" class="step">
												<span class="number">
												H </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian H : IMPAK KKP/5S </span>
												</a>
											</li>
											
											<?php if($CI->flexi_auth->get_user_group_id() == "5") {?>											<li>
												<a href="#tab10" data-toggle="tab" class="step">
												<span class="number">
												 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Pengesahan Ketua Pegawai Insiden</span>
												</a>
											</li>
											<?php }?>
											
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												Terdapat Ralat dalam pemarkahan, sila semak semula.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Masukkan Butiran Penilaian</h3>
												<div class="form-group">
													<label class="control-label col-md-3">PTJ <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<select name="ptj_group" class="form-control">
															<option value="">- SILA PILIH -</option>
															
															<?php foreach($ptj_list as $a => $b){ 
																echo "<option value='$b[id_ptj]'>$b[nama_ptj]</option/>";
															} ?>
														</select>
														<span class="help-block">
														Pilih PTJ yang ingin dinilai </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3" for="bilaudit">Bilangan Audit</label>
														<div class="col-md-4">
															<label class="radio-inline" for="bilaudit-1">
																<input type="radio" name="bilaudit" id="bilaudit-1" value="1">
																1
															</label>
															<label class="radio-inline" for="bilaudit-2">
																<input type="radio" name="bilaudit" id="bilaudit-2" value="2">
																2
															</label>
															<label class="radio-inline" for="bilaudit-3">
																<input type="radio" name="bilaudit" id="bilaudit-3" value="3">
																3
															</label>
															<label class="radio-inline" for="bilaudit-4">
																<input type="radio" name="bilaudit" id="bilaudit-4" value="4">
																4
															</label>
														</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3" for="skopaudit">Skop Audit</label>
														<div class="col-md-8">
															<label class="radio-inline" for="skopaudit-1">
																<input type="radio" name="skopaudit" id="skopaudit-1" value="Keselamatan Dan Kesihatan Tempat Kerja">
																Keselamatan Dan Kesihatan Tempat Kerja
															</label>
															<label class="radio-inline" for="skopaudit-2">
																<input type="radio" name="skopaudit" id="skopaudit-2" value="Persekitaran Berkualiti 5S">
																Persekitaran Berkualiti 5S
															</label>
															<span class="help-block">
														Pilih Skop Audit </span>
														</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Program/Pusat Pengajian <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="progpusat"/>
														<span class="help-block">
														Isikan Nama Program/Pusat Pengajian </span>
													</div>
												</div>
											
											
											<div class="form-group">
													<label class="control-label col-md-3">Bilik/Aras/Bangunan <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="bilikaras"/>
														<span class="help-block">
														Isikan Nama Bilik/Aras/Bangunan </span>
													</div>
											</div>
											
											<div class="form-group">
													<label class="control-label col-md-3">ZON 5S <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="zon5s"/>
														<span class="help-block">
														Isikan Zon 5S </span>
													</div>
											</div>
											
											<div class="form-group">
													<label class="control-label col-md-3">Lokasi <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="lokasi"/>
														<span class="help-block">
														Isikan Lokasi </span>
													</div>
											</div>
											</div>
											
											<div class="tab-pane" id="tab2">
												<h3 class="block">A1 : Sudut Keselamatan Dan Kesihatan Tempat Kerja</h3>
												
												<div class="form-group">
  <label class="col-md-4 control-label" for="a1.1">Mempamerkan Dasar KKP yang ditandatangani oleh Naib Canselor dan terkini</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.1-1">
      <input type="radio" name="a1.1" id="a1.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.1-2">
      <input type="radio" name="a1.1" id="a1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.1-3">
      <input type="radio" name="a1.1" id="a1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
												
												
												
												
												<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.2">Mempamerkan carta organisasi JKKP</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.2-1">
      <input type="radio" name="a1.2" id="a1.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.2-2">
      <input type="radio" name="a1.2" id="a1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.2-3">
      <input type="radio" name="a1.2" id="a1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.3">Mempamerkan Pelan Laluan Kecemasan / Lokasi Alat Pemadam Api </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.3-1">
      <input type="radio" name="a1.3" id="a1.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.3-2">
      <input type="radio" name="a1.3" id="a1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.3-3">
      <input type="radio" name="a1.3" id="a1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.4">Mempamerkan keputusan audit KKP yang terdahulu.</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.4-1">
      <input type="radio" name="a1.4" id="a1.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.4-2">
      <input type="radio" name="a1.4" id="a1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.4-3">
      <input type="radio" name="a1.4" id="a1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.5">Mempamerkan Maklumat Pegawai Insiden</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.5-1">
      <input type="radio" name="a1.5" id="a1.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.5-2">
      <input type="radio" name="a1.5" id="a1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.5-3">
      <input type="radio" name="a1.5" id="a1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.6">Mempamerkan No Telefon Kecemasan Terkini</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.6-1">
      <input type="radio" name="a1.6" id="a1.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.6-2">
      <input type="radio" name="a1.6" id="a1.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.6-3">
      <input type="radio" name="a1.6" id="a1.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.7">Mempamerkan Pelan Tindakan Kecemasan/ Prosedur Pengungsian Bangunan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.7-1">
      <input type="radio" name="a1.7" id="a1.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.7-2">
      <input type="radio" name="a1.7" id="a1.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.7-3">
      <input type="radio" name="a1.7" id="a1.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.8">Lokasi  sudut KKP mudah dilihat </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.8-1">
      <input type="radio" name="a1.8" id="a1.8-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.8-2">
      <input type="radio" name="a1.8" id="a1.8-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.8-3">
      <input type="radio" name="a1.8" id="a1.8-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Checkboxes (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="a1.9">Bahan/ maklumat di sudut KKP terkini dan sesuai  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.9-1">
      <input type="radio" name="a1.9" id="a1.9-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.9-2">
      <input type="radio" name="a1.9" id="a1.9-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.9-3">
      <input type="radio" name="a1.9" id="a1.9-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>


<h2>A2 : Keperluan Asas Ruang/ Bilik </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="a2.1">Maklumat Pegawai Bertanggungjawab/ kakitangan dipamerkan pada pintu masuk ruang/ bilik</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.1">
      <input type="radio" name="a2.1" id="a2.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.1-2">
      <input type="radio" name="a2.1" id="a2.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.1-3">
      <input type="radio" name="a2.1" id="a2.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a2.2">Mempamerkan pelan suis di ruang/bilik </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.2">
      <input type="radio" name="a2.2" id="a2.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.2-2">
      <input type="radio" name="a2.2" id="a2.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.2-3">
      <input type="radio" name="a2.2" id="a2.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a2.3">Mempamerkan Borang KEW.PA-7 (Senarai Aset Alih) </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.3">
      <input type="radio" name="a2.3" id="a2.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.3-2">
      <input type="radio" name="a2.3" id="a2.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.3-3">
      <input type="radio" name="a2.3" id="a2.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a2.4">Mempamerkan Etika Penggunaan ruang/bilik </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.4">
      <input type="radio" name="a2.4" id="a2.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.4-2">
      <input type="radio" name="a2.4" id="a2.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.4-3">
      <input type="radio" name="a2.4" id="a2.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A3: Pernyataan Pergerakan Kakitangan </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="a3.1">Mempamerkan maklumat pergerakan kakitangan yang jelas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a3.1">
      <input type="radio" name="a3.1" id="a3.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a3.1-2">
      <input type="radio" name="a3.1" id="a3.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a3.1-3">
      <input type="radio" name="a3.1" id="a3.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a3.2">Maklumat pergerakan kakitangan terkini "(latihan, urusan rasmi,cuti, ada, dan sebagainya)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a3.2">
      <input type="radio" name="a3.2" id="a3.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a3.2-2">
      <input type="radio" name="a3.2" id="a3.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a3.2-3">
      <input type="radio" name="a3.2.3" id="a3.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A4 : Pengurusan Fail</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="a4.1">Kabinet fail dilabel dan diindeks </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.1">
      <input type="radio" name="a4.1" id="a4.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.1-2">
      <input type="radio" name="a4.1" id="a4.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.1-3">
      <input type="radio" name="a4.1" id="a4.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a4.2">Fail disusun dengan teratur mengikut nombor siri/ indeks yang ditetapkan di dalam kabinet fail, mudah diperoleh/ disimpan dan bersistem </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.2">
      <input type="radio" name="a4.2" id="a4.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.2-2">
      <input type="radio" name="a4.2" id="a4.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.2-3">
      <input type="radio" name="a4.2" id="a4.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a4.3">Fail berbentuk ”ring” dilabel mengikut format yang telah diseragamkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.3">
      <input type="radio" name="a4.3" id="a4.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.3-2">
      <input type="radio" name="a4.3" id="a4.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.3-3">
      <input type="radio" name="a4.3" id="a4.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a4.4">Fail mudah diperoleh/ disimpan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.4">
      <input type="radio" name="a4.4" id="a4.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.4-2">
      <input type="radio" name="a4.4" id="a4.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.4-3">
      <input type="radio" name="a4.4" id="a4.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a4.5">Pencarian/ pencapaian fail  dalam tempoh kurang 30 saat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.5">
      <input type="radio" name="a4.5" id="a4.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.5-2">
      <input type="radio" name="a4.5" id="a4.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.5-3">
      <input type="radio" name="a4.5" id="a4.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A5: Penyimpanan Kunci</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="a5.1">Berada di tempat yang selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.1">
      <input type="radio" name="a5.1" id="a5.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.1-2">
      <input type="radio" name="a5.1" id="a5.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.1-3">
      <input type="radio" name="a5.1" id="a5.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a5.2">Kunci tempat penyimpanan disimpan oleh kakitangan  bertanggungjawab</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.2">
      <input type="radio" name="a5.2" id="a5.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.2-2">
      <input type="radio" name="a5.2" id="a5.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.2-3">
      <input type="radio" name="a5.2" id="a5.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a5.3">Kunci disusun, dilabel dan diindeks</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.3">
      <input type="radio" name="a5.3" id="a5.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.3-2">
      <input type="radio" name="a5.3" id="a5.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.3-3">
      <input type="radio" name="a5.3" id="a5.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="a5.4">Indeks kunci diletakkan di tempat yang selamat dan berasingan dari tempat penyimpanan kunci</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.4">
      <input type="radio" name="a5.4" id="a5.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.4-2">
      <input type="radio" name="a5.4" id="a5.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.4-3">
      <input type="radio" name="a5.4" id="a5.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A6: Meja (Ruang Kerja/Bilik Kakitangan)  </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="a6.1">Keadaan di atas meja bersih, kemas dan teratur</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.1">
      <input type="radio" name="a6.1" id="a6.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.1-2">
      <input type="radio" name="a6.1" id="a6.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.1-3">
      <input type="radio" name="a6.1" id="a6.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a6.2">Tiada bahan/ dokumen/ peralatan di bawah meja </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.2">
      <input type="radio" name="a6.2" id="a6.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.2-2">
      <input type="radio" name="a6.2" id="a6.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.2-3">
      <input type="radio" name="a6.2" id="a6.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a6.3">Laci meja dilabel mengikut item seperti “ALAT TULIS”, “DOKUMEN” dan “PERIBADI” dan kemas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.3">
      <input type="radio" name="a6.3" id="a6.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.3-2">
      <input type="radio" name="a6.3" id="a6.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.3-3">
      <input type="radio" name="a6.3" id="a6.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A7: Tanda Arah/Petunjuk</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="a7.1">Tanda arah/ petunjuk disediakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.1">
      <input type="radio" name="a7.1" id="a7.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.1-2">
      <input type="radio" name="a7.1" id="a7.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.1-3">
      <input type="radio" name="a7.1" id="a7.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a7.2">Tanda arah/ petunjuk mencukupi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.2">
      <input type="radio" name="a7.2" id="a7.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.2-2">
      <input type="radio" name="a7.2" id="a7.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.2-3">
      <input type="radio" name="a7.2" id="a7.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a7.3">Tanda arah/ petunjuk jelas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.3">
      <input type="radio" name="a7.3" id="a7.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.3-2">
      <input type="radio" name="a7.3" id="a7.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.3-3">
      <input type="radio" name="a7.3" id="a7.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="a7.4">Dipasang di tempat yang strategic</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.4">
      <input type="radio" name="a7.4" id="a7.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.4-2">
      <input type="radio" name="a7.4" id="a7.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.4-3">
      <input type="radio" name="a7.4" id="a7.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

	<div class="tab-pane" id="tab3">
	<h3 class="block">B1:Laluan Utama/Kecemasan</h3>

<div class="form-group">
  <label class="col-md-4 control-label" for="b1.1">Laluan dan tangga keluar bebas dari halangan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.1-1">
      <input type="radio" name="b1.1" id="b1.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b1.1-2">
      <input type="radio" name="b1.1" id="b1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.1-3">
      <input type="radio" name="b1.1" id="b1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b1.2">Lampu kecemasan yang berfungsi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.2-1">
      <input type="radio" name="b1.2" id="b1.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b1.2-2">
      <input type="radio" name="b1.2" id="b1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.2-3">
      <input type="radio" name="b1.2" id="b1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b1.3">Mempunyai label tanda arah keluar</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.3-1">
      <input type="radio" name="b1.3" id="b1.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b1.3-2">
      <input type="radio" name="b1.3" id="b1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.3-3">
      <input type="radio" name="b1.3" id="b1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b1.4">Pelan lantai ke arah tempat berkumpul semasa kecemasan dipamerkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.4-1">
      <input type="radio" name="b1.4" id="b1.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b1.4-2">
      <input type="radio" name="b1.4" id="b1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.4-3">
      <input type="radio" name="b1.4" id="b1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B2: Pintu Rintangan Api/Pintu Kecemasan</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="b2.1">Pintu kecemasan dikunci dan mudah dibuka dari dalam</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b2.1-1">
      <input type="radio" name="b2.1" id="b2.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b2.1-2">
      <input type="radio" name="b2.1" id="b2.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b2.1-3">
      <input type="radio" name="b2.1" id="b2.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b2.2">Pintu kecemasan bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b2.2-1">
      <input type="radio" name="b2.2" id="b2.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b2.2-2">
      <input type="radio" name="b2.2" id="b2.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b2.2-3">
      <input type="radio" name="b2.2" id="b2.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B3: Pintu Keluar</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="b3.1">Mempunyai lampu tanda “KELUAR” yang dapat dilihat dengan jelas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b3.1-1">
      <input type="radio" name="b3.1" id="b3.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b3.1-2">
      <input type="radio" name="b3.1" id="b3.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b3.1-3">
      <input type="radio" name="b3.1" id="b3.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b3.2">Lampu tanda “KELUAR” berfungsi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b3.2-1">
      <input type="radio" name="b3.2" id="b3.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b3.2-2">
      <input type="radio" name="b3.2" id="b3.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b3.2-3">
      <input type="radio" name="b3.2" id="b3.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B4: Lantai/Dinding/Siling </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="b4.1">Bersih/ kemas "(Tidak bocor/ berkulat/ berlubang)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b4.1-1">
      <input type="radio" name="b4.1" id="b4.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b4.1-2">
      <input type="radio" name="b4.1" id="b4.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b4.1-3">
      <input type="radio" name="b4.1" id="b4.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b4.2">Lantai tidak licin</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b4.2-1">
      <input type="radio" name="b4.2" id="b4.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b4.2-2">
      <input type="radio" name="b4.2" id="b4.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b4.2-3">
      <input type="radio" name="b4.2" id="b4.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B5: Peti Pertolongan Cemas </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="b5.1">Mempunyai peti pertolongan cemas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b5.1-1">
      <input type="radio" name="b5.1" id="b5.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b5.1-2">
      <input type="radio" name="b5.1" id="b5.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b5.1-3">
      <input type="radio" name="b5.1" id="b5.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b5.2">Kedudukan mudah dicapai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b5.2-1">
      <input type="radio" name="b5.2" id="b5.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b5.2-2">
      <input type="radio" name="b5.2" id="b5.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b5.2-3">
      <input type="radio" name="b5.2" id="b5.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b5.3">Kandungan peti pertolongan cemas diselenggara secara berkala oleh kakitangan yang bertanggungjawab</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b5.3-1">
      <input type="radio" name="b5.3" id="b5.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b5.3-2">
      <input type="radio" name="b5.3" id="b5.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b5.3-3">
      <input type="radio" name="b5.3" id="b5.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b5.4">Tidak menyimpan ubat makan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b5.4-1">
      <input type="radio" name="b5.4" id="b5.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b5.4-2">
      <input type="radio" name="b5.4" id="b5.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b5.4-3">
      <input type="radio" name="b5.4" id="b5.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B6: Peralatan Dan Barang Makmal/Bengkel</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.1">Mudah akses kepada tempat simpanan tinggi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.1-1">
      <input type="radio" name="b6.1" id="b6.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.1-2">
      <input type="radio" name="b6.1" id="b6.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.1-3">
      <input type="radio" name="b6.1" id="b6.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.2">Peralatan tajam disimpan dengan betul dan selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.2-1">
      <input type="radio" name="b6.2" id="b6.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.2-2">
      <input type="radio" name="b6.2" id="b6.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.2-3">
      <input type="radio" name="b6.2" id="b6.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.3">Mempunyai kipas ventilasi udara yang sesuai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.3-1">
      <input type="radio" name="b6.3" id="b6.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.3-2">
      <input type="radio" name="b6.3" id="b6.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.3-3">
      <input type="radio" name="b6.3" id="b6.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.4">Mempunyai troli khas untuk mengangkat bahan/barang</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.4-1">
      <input type="radio" name="b6.4" id="b6.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.4-2">
      <input type="radio" name="b6.4" id="b6.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.4-3">
      <input type="radio" name="b6.4" id="b6.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.5">Mempunyai alat nyahcemar (spill kit)/ peralatan membersihkan tumpahan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.5-1">
      <input type="radio" name="b6.5" id="b6.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.5-2">
      <input type="radio" name="b6.5" id="b6.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.5-3">
      <input type="radio" name="b6.5" id="b6.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.6">Bahan/ barang disusun dengan teratur mengikut keutamaan penggunaan dan pengeluaran secara Masuk Dahulu, Keluar Dahulu "(First In,First Out)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.6-1">
      <input type="radio" name="b6.6" id="b6.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.6-2">
      <input type="radio" name="b6.6" id="b6.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.6-3">
      <input type="radio" name="b6.6" id="b6.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="b6.7">Bahan/ barang rosak tidak bercampur dengan bahan/ barang yang masih boleh digunakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.7-1">
      <input type="radio" name="b6.7" id="b6.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b6.7-2">
      <input type="radio" name="b6.7" id="b6.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.7-3">
      <input type="radio" name="b6.7" id="b6.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B7: Bahan Mudah Terbakar/Berbahaya</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="b7.1">Disimpan di tempat yang sesuai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b7.1-1">
      <input type="radio" name="b7.1" id="b7.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="b7.1-2">
      <input type="radio" name="b7.1" id="b7.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b7.1-3">
      <input type="radio" name="b7.1" id="b7.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

	
	
	<div class="tab-pane" id="tab4">
	<h3 class="block">Bahagian C : Keselamatan Elektrik/Kemudahan Elektrik</h3>
		<div class="form-group">
  <label class="col-md-4 control-label" for="c1.1">Peralatan dan kemudahan elektrik berfungsi dengan baik dan selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.1-1">
      <input type="radio" name="c1.1" id="c1.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.1-2">
      <input type="radio" name="c1.1" id="c1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.1-3">
      <input type="radio" name="c1.1" id="c1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="c1.2">Pendawaian tambahan dilakukan oleh kakitangan yang bertauliah</label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.2-1">
      <input type="radio" name="c1.2" id="c1.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.2-2">
      <input type="radio" name="c1.2" id="c1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.2-3">
      <input type="radio" name="c1.2" id="c1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="c1.3">Wayar/ kabel (ICT dan elektrik) dalam keadaan kemas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.3-1">
      <input type="radio" name="c1.3" id="c1.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.3-2">
      <input type="radio" name="c1.3" id="c1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.3-3">
      <input type="radio" name="c1.3" id="c1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="c1.4">Bilik elektrik/ AHU tidak disalahguna</label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.4-1">
      <input type="radio" name="c1.4" id="c1.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.4-2">
      <input type="radio" name="c1.4" id="c1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.4-3">
      <input type="radio" name="c1.4" id="c1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="c1.5">Peralatan elektrik ditutup setelah digunakan dan menyediakan label peringatan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.5-1">
      <input type="radio" name="c1.5" id="c1.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.5-2">
      <input type="radio" name="c1.5" id="c1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.5-3">
      <input type="radio" name="c1.5" id="c1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="c1.6">Peralatan yang rosak dilabel dan dilaporkan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.6-1">
      <input type="radio" name="c1.6" id="c1.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.6-2">
      <input type="radio" name="c1.6" id="c1.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.6-3">
      <input type="radio" name="c1.6" id="c1.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="c1.7">Peralatan dan kemudahan elektrik berfungsi dengan baik dan selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="c1.7-1">
      <input type="radio" name="c1.7" id="c1.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="c1.7-2">
      <input type="radio" name="c1.7" id="c1.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="c1.7-3">
      <input type="radio" name="c1.7" id="c1.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>
	
	<div class="tab-pane" id="tab5">
	<h3 class="block">D1: Alat Pemadam Api/Alat Pencegah Kebakaran</h3>
		
		<div class="form-group">
  <label class="col-md-4 control-label" for="d1.1">APA dilabel dan diletak pada tempat yang sesuai "(mudah dicapai dan bebas daripada halangan)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.1-1">
      <input type="radio" name="d1.1" id="d1.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.1-2">
      <input type="radio" name="d1.1" id="d1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.1-3">
      <input type="radio" name="d1.1" id="d1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.2">APA masih sah digunakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.2-1">
      <input type="radio" name="d1.2" id="d1.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.2-2">
      <input type="radio" name="d1.2" id="d1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.2-3">
      <input type="radio" name="d1.2" id="d1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.3">Kad Rekod Pemeriksaan Berkala APA digantungkan pada alat/ kotak alat pemadam api</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.3-1">
      <input type="radio" name="d1.3" id="d1.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.3-2">
      <input type="radio" name="d1.3" id="d1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.3-3">
      <input type="radio" name="d1.3" id="d1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.4">Gelung Hos berada dalam keadaan baik dan bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.4-1">
      <input type="radio" name="d1.4" id="d1.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.4-2">
      <input type="radio" name="d1.4" id="d1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.4-3">
      <input type="radio" name="d1.4" id="d1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.5">Gelung Hos bersih dan tidak berhabuk</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.5-1">
      <input type="radio" name="d1.5" id="d1.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.5-2">
      <input type="radio" name="d1.5" id="d1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.5-3">
      <input type="radio" name="d1.5" id="d1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.6">Alat Pecah Kaca berada dalam keadaan baik dan bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.6-1">
      <input type="radio" name="d1.6" id="d1.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.6-2">
      <input type="radio" name="d1.6" id="d1.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.6-3">
      <input type="radio" name="d1.6" id="d1.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.7">Alat Pecah Kaca bersih dan tidak berhabuk</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.7-1">
      <input type="radio" name="d1.7" id="d1.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.7-2">
      <input type="radio" name="d1.7" id="d1.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.7-3">
      <input type="radio" name="d1.7" id="d1.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.8">Selimut api digantung di tempat yang sesuai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.8-1">
      <input type="radio" name="d1.8" id="d1.8-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.8-2">
      <input type="radio" name="d1.8" id="d1.8-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.8-3">
      <input type="radio" name="d1.8" id="d1.8-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.9">Selimut api bersih dan tidak berhabuk</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.9-1">
      <input type="radio" name="d1.9" id="d1.9-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.9-2">
      <input type="radio" name="d1.9" id="d1.9-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.9-3">
      <input type="radio" name="d1.9" id="d1.9-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="d1.10">Terdapat alat pengesan kebakaran "(Contoh: Sprinkler/ Alat pengesan haba/ Alat pengesan asap)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.10-1">
      <input type="radio" name="d1.10" id="d1.10-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="d1.10-2">
      <input type="radio" name="d1.10" id="d1.10-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.10-3">
      <input type="radio" name="d1.10" id="d1.10-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>
	
	<div class="tab-pane" id="tab6">
	<h3 class="block">E1: KELENGKAPAN PERLINDUNGAN DIRI</h3>
	
	<div class="form-group">
  <label class="col-md-4 control-label" for="e1.1">Mempunyai kakitangan yang bertanggungjawab menguruskan PPE</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.1-1">
      <input type="radio" name="e1.1" id="e1.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.1-2">
      <input type="radio" name="e1.1" id="e1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.1-3">
      <input type="radio" name="e1.1" id="e1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.2">Mempunyai rekod PPE</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.2-1">
      <input type="radio" name="e1.2" id="e1.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.2-2">
      <input type="radio" name="e1.2" id="e1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.2-3">
      <input type="radio" name="e1.2" id="e1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.3">Mempunyai rekod penyerahan PPE dan latihan/ taklimat penggunaan PPE</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.3-1">
      <input type="radio" name="e1.3" id="e1.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.3-2">
      <input type="radio" name="e1.3" id="e1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.3-3">
      <input type="radio" name="e1.3" id="e1.3 -3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.4">Memakai Peralatan perlindungan peribadi "(PPE)"yang bersesuaian dengan tugas dan mencukupi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.4-1">
      <input type="radio" name="e1.4" id="e1.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.4-2">
      <input type="radio" name="e1.4" id="e1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.4-3">
      <input type="radio" name="e1.4" id="e1.4 -3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.5">PPE diletakkan di tempat yang sesuai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.5-1">
      <input type="radio" name="e1.5" id="e1.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.5-2">
      <input type="radio" name="e1.5" id="e1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.5-3">
      <input type="radio" name="e1.5" id="e1.5 -3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.6">PPE diselenggara dengan baik</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.6-1">
      <input type="radio" name="e1.6" id="e1.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.6-2">
      <input type="radio" name="e1.6" id="e1.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.6-3">
      <input type="radio" name="e1.6" id="e1.6 -3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.7">PPE berada dalam keadaan bersih</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e1.7-1">
      <input type="radio" name="e1.7" id="e1.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e1.7-2">
      <input type="radio" name="e1.7" id="e1.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e1.7-3">
      <input type="radio" name="e1.7" id="e1.7 -3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E2: Keselamatan Bahan Kimia </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.1">Mempunyai rekod inventori bahan yang disimpan dalam stor</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.1-1">
      <input type="radio" name="e2.1" id="e2.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.1-2">
      <input type="radio" name="e2.1" id="e2.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.1-3">
      <input type="radio" name="e2.1" id="e2.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.2">Mempunyai Daftar Keselamatan Bahan Kimia Berbahaya Kepada Kesihatan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.2-1">
      <input type="radio" name="e2.2" id="e2.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.2-2">
      <input type="radio" name="e2.2" id="e2.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.2-3">
      <input type="radio" name="e2.2" id="e2.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.3">Mempunyai Penaksiran Risiko Bahan Kimia Berbahaya Kepada Kesihatan Secara Kendiri"(SeCHRA)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.3-1">
      <input type="radio" name="e2.3" id="e2.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.3-2">
      <input type="radio" name="e2.3" id="e2.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.3-3">
      <input type="radio" name="e2.3" id="e2.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.4">Helaian Data Keselamatan (SDS) ada "(< 5 tahun)" dan mudah dicapai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.4-1">
      <input type="radio" name="e2.4" id="e2.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.4-2">
      <input type="radio" name="e2.4" id="e2.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.4-3">
      <input type="radio" name="e2.4" id="e2.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.5">Peralatan yang digunakan untuk mengangkat atau menyimpan bahan kimia berkeadaan baik</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.5-1">
      <input type="radio" name="e2.5" id="e2.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.5-2">
      <input type="radio" name="e2.5" id="e2.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.5-3">
      <input type="radio" name="e2.5" id="e2.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.6">Rak yang digunakan berada dalam keadaan kukuh</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.6-1">
      <input type="radio" name="e2.6" id="e2.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.6-2">
      <input type="radio" name="e2.6" id="e2.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.6-3">
      <input type="radio" name="e2.6" id="e2.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.7">Bahan kimia diletakkan di dalam bekas sekunder</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.7-1">
      <input type="radio" name="e2.7" id="e2.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.7-2">
      <input type="radio" name="e2.7" id="e2.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.7-3">
      <input type="radio" name="e2.7" id="e2.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.8">Bahan kimia diletakkan di tempat yang tidak mudah terjatuh</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.8-1">
      <input type="radio" name="e2.8" id="e2.8-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.8-2">
      <input type="radio" name="e2.8" id="e2.8-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.8-3">
      <input type="radio" name="e2.8" id="e2.8-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.9">Bekas bahan kimia yang berat disimpan pada aras  rendah</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.9-1">
      <input type="radio" name="e2.9" id="e2.9-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.9-2">
      <input type="radio" name="e2.9" id="e2.9-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.9-3">
      <input type="radio" name="e2.9" id="e2.9-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.10">Kabinet khas bagi menyimpan bahan berbahaya disediakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.10-1">
      <input type="radio" name="e2.10" id="e2.10-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.10-2">
      <input type="radio" name="e2.10" id="e2.10-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.10-3">
      <input type="radio" name="e2.10" id="e2.10-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.11">Tiada bahan kimia lama yang sudah rosak / luput jangka hayat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.11-1">
      <input type="radio" name="e2.11" id="e2.11-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.11-2">
      <input type="radio" name="e2.11" id="e2.11-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.11-3">
      <input type="radio" name="e2.11" id="e2.11-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.12">Bahan kimia dilupuskan dengan selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.12-1">
      <input type="radio" name="e2.12" id="e2.12-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.12-2">
      <input type="radio" name="e2.12" id="e2.12-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.12-3">
      <input type="radio" name="e2.12" id="e2.12-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.13">Botol bahan kimia disusun mengikut kelas hazard</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.13-1">
      <input type="radio" name="e2.13" id="e2.13-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.13-2">
      <input type="radio" name="e2.13" id="e2.13-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.13-3">
      <input type="radio" name="e2.13" id="e2.13-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.14">Rak bersih dan susunan bahan kimia teratur</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.14-1">
      <input type="radio" name="e2.14" id="e2.14-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.14-2">
      <input type="radio" name="e2.14" id="e2.14-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.14-3">
      <input type="radio" name="e2.14" id="e2.14-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.15">Botol bahan kimia sentiasa ditutup apabila tidak digunakan "(bagi mengelakkan tumpahan/ pendedahan)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.15-1">
      <input type="radio" name="e2.15" id="e2.15-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.15-2">
      <input type="radio" name="e2.15" id="e2.15-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.15-3">
      <input type="radio" name="e2.15" id="e2.15-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.16">Bahan kimia mudah terbakar distorkan di kabinet khas bagi bahan kimia mudah terbakar</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.16-1">
      <input type="radio" name="e2.16" id="e2.16-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.16-2">
      <input type="radio" name="e2.16" id="e2.16-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.16-3">
      <input type="radio" name="e2.16" id="e2.16-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.17">Bahan kimia mudah terbakar tidak diletakkan berdekatan dengan punca api</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.17-1">
      <input type="radio" name="e2.17" id="e2.17-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.17-2">
      <input type="radio" name="e2.17" id="e2.17-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.17-3">
      <input type="radio" name="e2.17" id="e2.17-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.18">Bahan kimia yang berisiko berbahaya terhadap kesihatan distorkan di tempat yang terkawal, dan berlabel</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.18-1">
      <input type="radio" name="e2.18" id="e2.18-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.18-2">
      <input type="radio" name="e2.18" id="e2.18-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.18-3">
      <input type="radio" name="e2.18" id="e2.18-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.19">Peti sejuk simpanan bahan kimia diselenggara dan bersih</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.19-1">
      <input type="radio" name="e2.19" id="e2.19-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.19-2">
      <input type="radio" name="e2.19" id="e2.19-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.19-3">
      <input type="radio" name="e2.19" id="e2.19-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e2.20">Peti sejuk mempunyai label larangan makan dan minum</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e2.20-1">
      <input type="radio" name="e2.20" id="e2.20-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e2.20-2">
      <input type="radio" name="e2.20" id="e2.20-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e2.20-3">
      <input type="radio" name="e2.20" id="e2.20-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E3: Kebuk Wasap </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.1">Kebuk wasap disediakan sesuai dengan hazard dan berlabel</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.1-1">
      <input type="radio" name="e3.1" id="e3.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.1-2">
      <input type="radio" name="e3.1" id="e3.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.1-3">
      <input type="radio" name="e3.1" id="e3.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.2">Menyediakan prosedur penggunaan kebuk wasap</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.2-1">
      <input type="radio" name="e3.2" id="e3.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.2-2">
      <input type="radio" name="e3.2" id="e3.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.2-3">
      <input type="radio" name="e3.2" id="e3.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.3">Diselenggara secara berkala mengikut jadual yang ditetapkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.3-1">
      <input type="radio" name="e3.3" id="e3.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.3-2">
      <input type="radio" name="e3.3" id="e3.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.3-3">
      <input type="radio" name="e3.3" id="e3.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.4">Kebuk wasap tidak menyimpan bahan kimia dan alat yang tidak diperlukan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.4-1">
      <input type="radio" name="e3.4" id="e3.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.4-2">
      <input type="radio" name="e3.4" id="e3.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.4-3">
      <input type="radio" name="e3.4" id="e3.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.5">Kebuk wasap berfungsi dengan baik "(kuasa sedutan melebihi 100 ft/ min)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.5-1">
      <input type="radio" name="e3.5" id="e3.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.5-2">
      <input type="radio" name="e3.5" id="e3.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.5-3">
      <input type="radio" name="e3.5" id="e3.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.6">Tingkap kebuk wasap sentiasa ditutup apabila tidak digunakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.6-1">
      <input type="radio" name="e3.6" id="e3.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.6-2">
      <input type="radio" name="e3.6" id="e3.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.6-3">
      <input type="radio" name="e3.6" id="e3.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e3.7">Kebuk wasap bersih "(lantai dan sinki kebuk wasap bersih)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e3.7-1">
      <input type="radio" name="e3.7" id="e3.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e3.7-2">
      <input type="radio" name="e3.7" id="e3.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e3.7-3">
      <input type="radio" name="e3.7" id="e3.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E4: Safety Shower/ Eye Wash </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e4.1">Safety shower berfungsi dengan baik dan mempunyai tekanan air yang mencukupi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e4.1-1">
      <input type="radio" name="e4.1" id="e4.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e4.1-2">
      <input type="radio" name="e4.1" id="e4.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e4.1-3">
      <input type="radio" name="e4.1" id="e4.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e4.2">Eye wash berfungsi dengan baik dan mempunyai tekanan air yang mencukupi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e4.2-1">
      <input type="radio" name="e4.2" id="e4.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e4.2-2">
      <input type="radio" name="e4.2" id="e4.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e4.2-3">
      <input type="radio" name="e4.2" id="e4.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e4.3">Diselenggara secara berkala mengikut  jadual yang ditetapkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e4.3-1">
      <input type="radio" name="e4.3" id="e4.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e4.3-2">
      <input type="radio" name="e4.3" id="e4.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e4.3-3">
      <input type="radio" name="e4.3" id="e4.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e4.4">Laluan ke safety shower dan eye wash bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e4.4-1">
      <input type="radio" name="e4.4" id="e4.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e4.4-2">
      <input type="radio" name="e4.4" id="e4.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e4.4-3">
      <input type="radio" name="e4.4" id="e4.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e4.5">Safety shower dan Eye Wash bersih dan tidak berhabuk</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e4.5-1">
      <input type="radio" name="e4.5" id="e4.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e4.5-2">
      <input type="radio" name="e4.5" id="e4.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e4.5-3">
      <input type="radio" name="e4.5" id="e4.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e4.6">Mempunyai kelengkapan membersihkan tumpahan bahan berbahaya</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e4.6-1">
      <input type="radio" name="e4.6" id="e4.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e4.6-2">
      <input type="radio" name="e4.6" id="e4.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e4.6-3">
      <input type="radio" name="e4.6" id="e4.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E5: Keselamatan Sinaran</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e5.1">Mempamerkan lambang radioaktif </label>
  <div class="col-md-4">
    <label class="radio-inline" for="e5.1-1">
      <input type="radio" name="e5.1" id="e5.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e5.1-2">
      <input type="radio" name="e5.1" id="e5.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e5.1-3">
      <input type="radio" name="e5.1" id="e5.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e5.2">Alat pengesan sinaran yang sedia digunakan dan tentukuran masih sah </label>
  <div class="col-md-4">
    <label class="radio-inline" for="e5.2-1">
      <input type="radio" name="e5.2" id="e5.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e5.2-2">
      <input type="radio" name="e5.2" id="e5.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e5.2-3">
      <input type="radio" name="e5.2" id="e5.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e5.3">Mempamerkan Lesen (bahan radioaktif/ radas penyinaran "(Lampiran A)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e5.3-1">
      <input type="radio" name="e5.3" id="e5.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e5.3-2">
      <input type="radio" name="e5.3" id="e5.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e5.3-3">
      <input type="radio" name="e5.3" id="e5.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e5.4">Mempunyai no. telefon orang yang boleh dihubungi sekiranya berlaku kecemasan (Pegawai Perlindungan Sinaran/ AELB/ Penyelia Makmal)</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e5.4-1">
      <input type="radio" name="e5.4" id="e5.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e5.4-2">
      <input type="radio" name="e5.4" id="e5.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e5.4-3">
      <input type="radio" name="e5.4" id="e5.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e5.5">Prosedur tindakan semasa kecemasan dipamerkan di peralatan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e5.5-1">
      <input type="radio" name="e5.5" id="e5.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e5.5-2">
      <input type="radio" name="e5.5" id="e5.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e5.5-3">
      <input type="radio" name="e5.5" id="e5.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e5.6">Bahan  radioaktif yang tidak digunakan disimpan di dalam stor/ tempat penyimpanan yang mempunyai perisai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e5.6-1">
      <input type="radio" name="e5.6" id="e5.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e5.6-2">
      <input type="radio" name="e5.6" id="e5.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e5.6-3">
      <input type="radio" name="e5.6" id="e5.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E6 : Keselamatan Silinder Gas </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.1">Silinder gas mempunyai Helaian Data Keselamatan "(SDS)" </label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.1-1">
      <input type="radio" name="e6.1" id="e6.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.1-2">
      <input type="radio" name="e6.1" id="e6.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.1-3">
      <input type="radio" name="e6.1" id="e6.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.2">Silinder gas dirantai pada penyokong/ tempat yang tetap</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.2-1">
      <input type="radio" name="e6.2" id="e6.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.2-2">
      <input type="radio" name="e6.2" id="e6.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.2-3">
      <input type="radio" name="e6.2" id="e6.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.3">Silinder gas dilabelkan  status penggunaan "(penuh, sedang digunakan, kosong)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.3-1">
      <input type="radio" name="e6.3" id="e6.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.3-2">
      <input type="radio" name="e6.3" id="e6.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.3-3">
      <input type="radio" name="e6.3" id="e6.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.4">Silinder gas diasingkan mengikut kelas hazard</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.4-1">
      <input type="radio" name="e6.4" id="e6.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.4-2">
      <input type="radio" name="e6.4" id="e6.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.4-3">
      <input type="radio" name="e6.4" id="e6.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.5">Injap silinder gas ditutup sekiranya tidak digunakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.5-1">
      <input type="radio" name="e6.5" id="e6.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.5-2">
      <input type="radio" name="e6.5" id="e6.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.5-3">
      <input type="radio" name="e6.5" id="e6.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.6">Silinder gas berada jauh daripada sumber percikan api</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.6-1">
      <input type="radio" name="e6.6" id="e6.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.6-2">
      <input type="radio" name="e6.6" id="e6.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.6-3">
      <input type="radio" name="e6.6" id="e6.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.7">Mempunyai peredaran udara yang cukup dan berkesan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.7-1">
      <input type="radio" name="e6.7" id="e6.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.7-2">
      <input type="radio" name="e6.7" id="e6.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.7-3">
      <input type="radio" name="e6.7" id="e6.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e6.8">Penstoran dan silinder gas bersih dan tidak berhabuk</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e6.8-1">
      <input type="radio" name="e6.8" id="e6.8-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e6.8-2">
      <input type="radio" name="e6.8" id="e6.8-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e6.8-3">
      <input type="radio" name="e6.8" id="e6.8-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E7: Keselamatan Biologi </h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e7.1">Peralatan khusus mempunyai Sijil Perakuan Kelayakan yang sah "(cth : autoclave, compressor)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e7.1-1">
      <input type="radio" name="e7.1" id="e7.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e7.1-2">
      <input type="radio" name="e7.1" id="e7.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e7.1-3">
      <input type="radio" name="e7.1" id="e7.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e7.2">Mempunyai kabinet keselamatan biologi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e7.2-1">
      <input type="radio" name="e7.2" id="e7.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e7.2-2">
      <input type="radio" name="e7.2" id="e7.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e7.2-3">
      <input type="radio" name="e7.2" id="e7.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e7.3">Kabinet keselamatan biologi diselenggara secara berkala</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e7.3-1">
      <input type="radio" name="e7.3" id="e7.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e7.3-2">
      <input type="radio" name="e7.3" id="e7.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e7.3-3">
      <input type="radio" name="e7.3" id="e7.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E8: Pengumpulan Sisa Berbahaya</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e8.1"> Mempunyai inventori sisa berbahaya "(kimia/ biologi/ radioaktif)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.1-1">
      <input type="radio" name="e8.1" id="e8.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.1-2">
      <input type="radio" name="e8.1" id="e8.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.1-3">
      <input type="radio" name="e8.1" id="e8.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e8.2">Mempunyai  bekas  untuk menyimpan sisa berbahaya (kimia/ biologi/ radioaktif) dan berlabel</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.2-1">
      <input type="radio" name="e8.2" id="e8.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.2-2">
      <input type="radio" name="e8.2" id="e8.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.2-3">
      <input type="radio" name="e8.2" id="e8.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div><div class="form-group">
  <label class="col-md-4 control-label" for="e8.3">Bekas melupuskan peralatan tajam dan kaca disediakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.3-1">
      <input type="radio" name="e8.3" id="e8.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.3-2">
      <input type="radio" name="e8.3" id="e8.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.3-3">
      <input type="radio" name="e8.3" id="e8.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div><div class="form-group">
  <label class="col-md-4 control-label" for="e8.4">Tempat penyimpanan sisa yang teratur dan selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.4-1">
      <input type="radio" name="e8.4" id="e8.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.4-2">
      <input type="radio" name="e8.4" id="e8.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.4-3">
      <input type="radio" name="e8.4" id="e8.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div><div class="form-group">
  <label class="col-md-4 control-label" for="e8.5">Sisa disimpan tidak melebihi tempoh waktu "(Sisa kimia tidak melebihi 180 hari, sisa biologi tidak melebihi 14 hari)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.5-1">
      <input type="radio" name="e8.5" id="e8.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.5-2">
      <input type="radio" name="e8.5" id="e8.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.5-3">
      <input type="radio" name="e8.5" id="e8.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div><div class="form-group">
  <label class="col-md-4 control-label" for="e8.6">Bekas sisa diletakkan di dalam bekas sekunder</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.6-1">
      <input type="radio" name="e8.6" id="e8.6-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.6-2">
      <input type="radio" name="e8.6" id="e8.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.6-3">
      <input type="radio" name="e8.6" id="e8.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div><div class="form-group">
  <label class="col-md-4 control-label" for="e8.7">Penstoran sisa bersih</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e8.7-1">
      <input type="radio" name="e8.7" id="e8.7-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e8.7-2">
      <input type="radio" name="e8.7" id="e8.7-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e8.7-3">
      <input type="radio" name="e8.7" id="e8.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>E9:Keselamatan Mekanikal</h2>

<div class="form-group">
  <label class="col-md-4 control-label" for="e9.1">Jentera mempunyai Sijil Perakuan Kelayakan yang sah  (contoh : autoclave, compressor, mesin angkat/lif)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e9.1-1">
      <input type="radio" name="e9.1" id="e9.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e9.1-2">
      <input type="radio" name="e9.1" id="e9.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e9.1-3">
      <input type="radio" name="e9.1" id="e9.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e9.2">Mesin dilengkapi dengan pelindung (guards) bagi mengelakkan kecederaan kepada pekerja</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e9.2-1">
      <input type="radio" name="e9.2" id="e9.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e9.2-2">
      <input type="radio" name="e9.2" id="e9.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e9.2-3">
      <input type="radio" name="e9.2" id="e9.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e9.3"> Peralatan/mesin diselenggara secara berkala</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e9.3-1">
      <input type="radio" name="e9.3" id="e9.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e9.3-2">
      <input type="radio" name="e9.3" id="e9.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e9.3-3">
      <input type="radio" name="e9.3" id="e9.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e9.4"> Peralatan/ mesin bersih dan tidak berhabuk</label>
  <div class="col-md-4">
    <label class="radio-inline" for="e9.4-1">
      <input type="radio" name="e9.4" id="e9.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="e9.4-2">
      <input type="radio" name="e9.4" id="e9.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="e9.4-3">
      <input type="radio" name="e9.4" id="e9.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

	<div class="tab-pane" id="tab7">
	
		<h3 class="block">BAHAGIAN F: KAWASAN PERSEKITARAN</h3>
		
		<div class="form-group">
  <label class="col-md-4 control-label" for="f1.1">Pokok hiasan dijaga dengan baik</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.1-1">
      <input type="radio" name="f1.1" id="f1.1-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="f1.1-2">
      <input type="radio" name="f1.1" id="f1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.1-3">
      <input type="radio" name="f1.1" id="f1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="f1.2">Tong sampah mencukupi dan bertutup</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.2-1">
      <input type="radio" name="f1.2" id="f1.2-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="f1.2-2">
      <input type="radio" name="f1.2" id="f1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.2-3">
      <input type="radio" name="f1.2" id="f1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="f1.3">Meja makmal/ bengkel tidak mempunyai kesan tumpahan/ kotoran</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.3-1">
      <input type="radio" name="f1.3" id="f1.3-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="f1.3-2">
      <input type="radio" name="f1.3" id="f1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.3-3">
      <input type="radio" name="f1.3" id="f1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="f1.4">Perabot makmal/ bengkel teratur, berfungsi dengan baik dan selamat digunakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.4-1">
      <input type="radio" name="f1.4" id="f1.4-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="f1.4-2">
      <input type="radio" name="f1.4" id="f1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.4-3">
      <input type="radio" name="f1.4" id="f1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="f1.5">Makmal menyediakan sinki mencuci tangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.5-1">
      <input type="radio" name="f1.5" id="f1.5-1" value="1">
      YA
    </label>
    <label class="radio-inline" for="f1.5-2">
      <input type="radio" name="f1.5" id="f1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.5-3">
      <input type="radio" name="f1.5" id="f1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

	<div class="tab-pane" id="tab8">
	<h3 class="block">G: Amalan Terbaik  (Nyatakan amalan terbaik mengikut penemuan auditor)</h3>	
		<div class="form-group">
  <label class="col-md-4 control-label" for="g1.1">Amalan Terbaik Pertama</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="g1.1-1">
      <input type="radio" name="g1.1" id="g1.1-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="g1.1-2">
      <input type="radio" name="g1.1" id="g1.1-2" value="0">
      TIDAK
    </label> 
    
  </div>
</div>

			<div class="form-group">
  <label class="col-md-4 control-label" for="g1.2">Amalan Terbaik Kedua</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="g1.2-1">
      <input type="radio" name="g1.2" id="g1.2-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="g1.2-2">
      <input type="radio" name="g1.2" id="g1.2-2" value="0">
      TIDAK
    </label> 
  
  </div>
</div>
</div>

	<div class="tab-pane" id="tab9">
	<h3 class="block">Bahagian H : Impak KKP/5S</h3>

		<div class="form-group">
  <label class="col-md-4 control-label" for="h1.1">Kecederaan hilang upaya kekal/kecederaan yang mendapat cuti sakit melebihi 4 hari atau  maut dapat dielakkan</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="h1.1-1">
      <input type="radio" name="h1.1" id="h1.1-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="h1.1-2">
      <input type="radio" name="h1.1" id="h1.1-2" value="0">
      TIDAK
    </label> 
    
  </div>
</div>

	<div class="form-group">
  <label class="col-md-4 control-label" for="h1.2">Kemalangan kecil/kemalangan nyaris/kejadian berbahaya di tempat kerja dapat dielakkan</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="h1.2-1">
      <input type="radio" name="h1.2" id="h1.2-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="h1.2-2">
      <input type="radio" name="h1.2" id="h1.2-2" value="0">
      TIDAK
    </label> 
    
  </div>
</div>

	<div class="form-group">
  <label class="col-md-4 control-label" for="h1.3">Bahan terbuang (kertas, plastik, perabot lama – diukur dengan kilogram / jumlah RM berdasarkan hasil jualan)</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="h1.3-1">
      <input type="radio" name="h1.3" id="h1.3-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="h1.3-2">
      <input type="radio" name="h1.3" id="h1.3-2" value="0">
      TIDAK
    </label> 
	
	</div>
</div>
	
	<div class="form-group">
  <label class="col-md-4 control-label" for="h1.4">Ruang baharu yang dapat diwujudkan hasil daripada aktiviti 5S di  ruang asal (diukur berdasarkan penggunaan ruang baharu/ pengoptimuman ruang dalam unit kaki persegi)</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="h1.4-1">
      <input type="radio" name="h1.4" id="h1.4-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="h1.4-2">
      <input type="radio" name="h1.4" id="h1.4-2" value="0">
      TIDAK
    </label> 
	
	</div>
</div>
</div>
<?php if($CI->flexi_auth->get_user_group_id() == "5") {?>
<div class="tab-pane" id="tab10">
	<h3 class="block">Pengesahan Ketua Pegawai Insiden</h3>
	
						<div class="form-group">
  <label class="col-md-4 control-label" for="pengesahan">Pengesahan Ketua Pegawai Insiden</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="pengesahan-1">
      <input type="radio" name="pengesahan" id="pengesahan-1" value="1">
      Disahkan
    </label> 
    <label class="radio-inline" for="i1.4-2">
      <input type="radio" name="pengesahan" id="pengesahan-2" value="2">
      Tidak disahkan
    </label> 
	
	</div>
</div>
		
</div> 
<?php } ?>

	

	
							<input type="hidden" name="fa" value="submit">
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Kembali </a>
												<a href="javascript:;" class="btn blue button-next">
												Seterusnya<i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript: submit_form();" class="btn green button-submit">
												Hantar <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 

<script>

function submit_form()
{
	document.test_form.submit();
}
jQuery(document).ready(function() {  
   FormWizard.init();
});
</script>
 


