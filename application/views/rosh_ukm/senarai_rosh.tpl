<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title"><?php echo $CI->rd_menu->page_title()->page_title; ?> </h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                         <table id="manage_user_acc" class="display table">
                                            <thead>
												<tr>
													<th>Nama Pegawai ROSH</th>
													<th>UKMPer</th>
													<th>Jawatan</th>
													
												</tr>
											</thead>
                                            <tbody>
													<?php foreach ($result as $user) { 
													$id_rosh = $user['rosh_id'];?>
													<tr>
														<td>
															<?php echo $user['rosh_nama'];?>
														</td>
														<td>
															<?php echo $user['rosh_ukmper'];?>
														</td>
														<td>
															<?php echo $user['rosh_jawatan'];?>
														</td>
														
														<td>
															<?php echo $user[$this->flexi_auth->db_column('user_group', 'name')];?>
														</td>
														<td>
														
															<a href="#" onclick="javascript:delete_func('<?php echo $id_rosh; ?>');">Delete</a>
															</td>
													</tr>
												<?php } ?>
												</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<form name="delete" id="delete" action="" method="POST">
						<input type='hidden' name="action" value="delete">
						<input type='hidden' name="id_rosh" id="id_rosh" value="">
					</form>
					<script type='text/javascript'>
function delete_func(id_rosh){
	if (confirm("Delete this data ?")) {
		document.getElementById("id_rosh").value = id_rosh;
		document.delete.submit();
		//alert('Deleted');
	  } else {
		//alert('Not Deleted');
	  }
}
</script>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 
 


