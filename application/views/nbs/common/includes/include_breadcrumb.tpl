<?php $CI =& get_instance(); ?>
<h3 class="page-title">
			<?php echo $CI->rd_menu->page_title()->page_title; ?> 
            <?php if($CI->rd_menu->page_title()->page_title_small){ ?> <small><?php echo $CI->rd_menu->page_title()->page_title_small; ?></small> <?php } ?>
            
			</h3>
<div class="page-bar">
				<ul class="page-breadcrumb">
                
                
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
                    
                    <?php
                    $count_breadcrumb = count($CI->rd_menu->breadcrumb());
                    $last_breadcrumb = $count_breadcrumb-1;
                    foreach($CI->rd_menu->breadcrumb() as $key => $value){ ?>
                    <?php
                    if($value['link'] == 'ci_controller'){ $menulink = base_url().$value['class'].'/'.$value['method']; }
                    else{ $menulink = $value['link']; }
                    ?>
                        <?php if($last_breadcrumb != $key){ ?>
                        <li>
    						<a href="<?php echo $menulink; ?>"><?php echo $value['name']; ?></a>
                            
                            <i class="fa fa-angle-right"></i>
                            
    						
    					</li>
                        <?php }else{ ?>
                        <li>
    						<a style="text-decoration: none;" href="javascript: void();"><?php echo $value['name']; ?></a>
                            
    					</li>
                        <?php } ?>
                    <?php }
                    ?>
				</ul>
				<div class="page-toolbar">
                    <!--
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height grey-salt" data-placement="top" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp; <span class="thin uppercase visible-lg-inline-block"></span>&nbsp; <i class="fa fa-angle-down"></i>
					</div>
                    -->
				</div>
			</div>