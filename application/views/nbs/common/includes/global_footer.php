<?php
$CI =& get_instance();
?>
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo $CI->rd_meta->get_value('page-footer-inner'); ?>
	</div>
	<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>