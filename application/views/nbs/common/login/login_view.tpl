<?php
$CI =& get_instance();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
	<title><?php echo $CI->rd_meta->get_value('title'); ?></title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/>
	<meta name="keywords" content="demo, flexi auth, user authentication, codeigniter"/>
	<?php $this->load->view('nbs/common/login/login_head.tpl'); ?>
</head>
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="<?php echo base_url(); ?>" style="font-family: 'Lobster', cursive;">
	<img class="logo-image" src="<?php echo $CI->rd_meta->get_value('logo_login_url'); ?>" alt="" />
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
<?php if($message){ ?>
    <div class="alert alert-info alert-dismissable">
        <?php echo $message; ?> </a>
    </div>
<?php } ?>
	<!-- BEGIN LOGIN FORM -->
    <?php echo form_open(current_url(), 'class="login-form"');?>
		<h3 class="form-title">Log Masuk</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Masukkan UKMPer dan Kata laluan anda. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">UKMPer</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>


				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="UKMPer" value="<?php echo set_value('login_identity');?>" name="login_identity"/>

            </div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Kata Laluan</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Kata Laluan" name="login_password" />
			</div>
		</div>

        <div class="form-group">
			<?php

							# Below are 2 examples, the first shows how to implement 'reCaptcha' (By Google - http://www.google.com/recaptcha),
							# the second shows 'math_captcha' - a simple math question based captcha that is native to the flexi auth library.
							# This example is setup to use reCaptcha by default, if using math_captcha, ensure the 'auth' controller and 'demo_auth_model' are updated.

							# reCAPTCHA Example
							# To activate reCAPTCHA, ensure the 'if' statement immediately below is uncommented and then comment out the math captcha 'if' statement further below.
			 				# You will also need to enable the recaptcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
							#/*
							if (isset($captcha))
							{
								//echo "<li>\n";
								//echo $captcha;
								//echo "</li>\n";
							}
							#*/

							/* math_captcha Example
							# To activate math_captcha, ensure the 'if' statement immediately below is uncommented and then comment out the reCAPTCHA 'if' statement just above.
							# You will also need to enable the math_captcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
							if (isset($captcha))
							{
								echo "<li>\n";
								echo "<label for=\"captcha\">Captcha Question:</label>\n";
								echo $captcha.' = <input type="text" id="captcha" name="login_captcha" class="width_50"/>'."\n";
								echo "</li>\n";
							}
							#*/
						?>
		</div>




		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember_me" value="1"/> Ingati Saya </label>
            <input type="hidden" name="login_user" value="Submit" />
			<button type="submit" class="btn red pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>		<div class="forget-password">                    <h4>Lupa Kata Laluan?</h4>                    <p> Klik                     <a href="javascript:;" id="forget-password"> saya </a> untuk set semula kata laluan anda. </p>                </div>
        <!--
		<div class="login-options">
			<h4>Or login with</h4>
			<ul class="social-icons">
				<li>
					<a class="facebook" data-original-title="facebook" href="#">
					</a>
				</li>
				<li>
					<a class="twitter" data-original-title="Twitter" href="#">
					</a>
				</li>
				<li>
					<a class="googleplus" data-original-title="Goole Plus" href="#">
					</a>
				</li>
				<li>
					<a class="linkedin" data-original-title="Linkedin" href="#">
					</a>
				</li>
			</ul>
		</div>
        -->


	<?php echo form_close();?>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->								 <?php echo form_open(base_url("auth/forgotten_password"), 'class="forget-form"');?>                <h3>Forget Password ?</h3>                <p> Enter your e-mail address below to reset your password. </p>                <div class="form-group">                    <div class="input-icon">                        <i class="fa fa-envelope"></i>                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="forgot_password_identity" value="<?php echo set_value('forgot_password_identity');?>"/> </div>                </div>                <div class="form-actions">                    <button type="button" id="back-btn" class="btn grey-salsa btn-outline"> Back </button>					 <input type="hidden" name="send_forgotten_password" value="Submit" />					<button type="submit" class="btn red pull-right">						Submit <i class="m-icon-swapright m-icon-white"></i>					</button>                </div>            <?php echo form_close();?>
	<!-- END FORGOT PASSWORD FORM -->
	<!-- BEGIN REGISTRATION FORM -->

	<!-- END REGISTRATION FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 <?php echo $CI->rd_meta->get_value('page-footer-inner'); ?>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script type="text/javascript">$(function(){
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_UK/all.js', function(){
    FB.init({
        appId   : '<?php echo $CI->rd_meta->get_value('facebook_app_id'); ?>',
        xfbml   : true,
        version : 'v2.3'
    });

  });
});</script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-facebook.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/imagesloaded.pkgd.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('media/nbs'); ?>/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url('media/nbs'); ?>/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url('media/nbs'); ?>/admin/pages/scripts/login.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
  Login.init();
  $(".logo-image").error(function(e){
    console.log('asdasdads')
    //$(this).parent().html('<h4 class="logo-text"><?php echo $CI->far_meta->get_value('title'); ?></h4>');
  });

  $('.logo-image').imagesLoaded().fail( function() {
    $('.logo-image').parent().html('<h4 class="logo-text"><?php echo $CI->rd_meta->get_value('title'); ?></h4>');
  });
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
