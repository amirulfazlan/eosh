	<meta name="robots" content="index, follow"/>
	<meta name="designer" content="haseydesign - Rob Hussey : rob @ haseydesign .com"/> 
	<meta name="copyright" content="Copyright <?php echo date('Y');?> Rob Hussey, All Copyrights Reserved"/>
	<meta http-equiv="imagetoolbar" content="no"/>	
	
	<link rel="stylesheet" href="<?php echo base_url('media/nbs/includes/css/global.css');?>?v=1.0">
	<link rel="stylesheet" href="<?php echo base_url('media/nbs/includes/css/structure.css');?>?v=1.0">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo base_url('media/nbs'); ?>/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url('media/nbs'); ?>/global/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/global/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?php echo base_url('media/nbs'); ?>/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('media/nbs'); ?>/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url('media/nbs'); ?>/global/img/favicon.png" type="image/x-icon">
<link rel="icon" href="<?php echo base_url('media/nbs'); ?>/global/img/favicon.png" type="image/x-icon">