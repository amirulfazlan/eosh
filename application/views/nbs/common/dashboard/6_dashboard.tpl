<?php $CI =& get_instance(); 
$this->load->view('nbs/common/header/header.tpl'); ?> 
<?php
?>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('media/nbs'); ?>/admin/layout/css/loader.css"/>
<div id="page_loader-wrapper">
    <div id="page_loader"></div>
 
    <div class="page_loader-section section-left"></div>
    <div class="page_loader-section section-right"></div>
 
</div>
<div class="row">
<div class="col-md-12">
        <?php if (!empty($message)) { ?>
            <?php if(is_array($message)){ ?> 
            <?php foreach($message as $m_key => $m_val){ ?>
                <div class="note note-success"><p><?php echo $m_val; ?></p></div>
            <?php } ?>
            <?php }else{ ?>
            <div class="note note-success">
                <p><?php echo $message; ?></p>
            </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
   <!-- <div class="row">
    <div class="col-md-12">
        <div class="well" style="padding: 0px;">
            <div class="btn-group btn-group-justified" style="min-width: 310px; height: 200px; margin: 0 auto">
                <a class="btn btn-success" href="" target="_blank">
                    <span>Jumlah Penilaian</span>
                    <br />
                    <font size="14"><b>0</b></font>
                    <br><font size="2">Penilaian</font>
                </a>
                <a class="btn btn-danger" href="" target="_blank"><span>Jumlah Hari Tanpa Kemalangan</span><br><font size="14"><b>0</b></font><br><font size="2">Kemalangan</font></a>
                <a class="btn btn-warning" href="" target="_blank"><span>Jumlah PTJ</span><br><font size="14"><b>0</b></font><br><font size="2">PTJ</font></a>
            </div>
        </div>
    </div>   
	</div>-->
<div class="row">
    <div class="col-md-12">
        <div class="well" style="padding: 0px;">
					<div class="btn-group btn-group-justified" style="min-width: 190px; height: 120px; margin: 0 auto">
						<a class="btn btn-warning" href="" target="_blank"><font size="4"><?php echo date('d M Y'); ?> Tarikh Hari Ini</font></a>
						<a class="btn btn-success" href="" target="_blank"><font size="4">Antaramuka Pegawai Insiden</font></a>
					</div>
		</div>
    </div>
    
</div>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="../../eosh/media/nbs/img/eoshdepan1.jpg"< alt="Latihan Kebakaran">
    </div>

    <div class="item">
      <img src="../../eosh/media/nbs/img/eoshdepan2.jpg" alt="Hari Bebas Kenderaan">
    </div>

    <div class="item">
      <img src="../../eosh/media/nbs/img/eoshdepan3.jpg" alt="Alatan Semasa Audit">
    </div>

    <div class="item">
      <img src="../../eosh/media/nbs/img/eoshdepan4.jpg" alt="Alatan Semasa Audit">
    </div>
	
	<div class="item">
      <img src="../../eosh/media/nbs/img/eoshdepan5.jpg" alt="Virus Zika">
    </div>
	
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


    </div>
    
    <div class="col-md-4 col-sm-12">
									<div class="portlet box blue mypasti">
										<div class="portlet-title">
											<div class="caption">
												<i class="fa fa-gift"></i>Facebook
											</div>
										</div>
										<div class="portlet-body">
											<div class="tabbable portlet-tabs">
												<ul class="nav nav-tabs" style="top: -51px;">
													
													<li>
														<a href="#portlet_tab_facebook_photo" data-toggle="tab">
														Photo Gallery </a>
													</li>
                                                    <li class="active">
														<a href="#portlet_tab_facebook_news_feed" data-toggle="tab">
														News Feed </a>
													</li>
													
												</ul>
												<div class="tab-content">
													<div class="tab-pane" id="portlet_tab_facebook_photo">
														<div class="scroller" style="height: 410px;" data-always-visible="1" data-rail-visible="0" id="facebook_page_album">
								
                            							</div>
                            							<div class="scroller-footer">
                            								<div class="btn-arrow-link pull-right">
                            									<a target="_blank" href="https://www.facebook.com/<?php echo $CI->rd_meta->get_value('facebook_pageid'); ?>">View All</a>
                            									<i class="icon-arrow-right"></i>
                            								</div>
                            							</div>
													</div>
													<div class="tab-pane active" id="portlet_tab_facebook_news_feed">
														<div class="scroller facebook-wall standard" style="height: 410px;" data-always-visible="1" data-rail-visible="0" id="facebook_wall">
								
							                             </div>
                                                         <div class="scroller-footer">
                            								<div class="btn-arrow-link pull-right">
                            									<a target="_blank" href="https://www.facebook.com/<?php echo $CI->rd_meta->get_value('facebook_pageid'); ?>">View All News</a>
                            									<i class="icon-arrow-right"></i>
                            								</div>
                            							</div>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
    
    

</div>


<?php $this->load->view('nbs/common/footer/footer.tpl'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-facebook-wall/style.css" />
<script type="text/javascript" src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-facebook-wall/facebook_wall.plugin.js"></script>

<script type="text/javascript" src="<?php echo base_url('media/nbs'); ?>/global/plugins/Facebook-Album-Browser/src/jquery.fb.albumbrowser.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('media/nbs'); ?>/global/plugins/Facebook-Album-Browser/src/jquery.fb.albumbrowser.css" />
  
<script type="text/javascript">
$(function(){
    $("#recent_transaction").load('<?php echo base_url('media/nbs'); ?>wallet/dashboard_recent_transaction', false, function(){
        
    })
    
    $('#facebook_wall').facebook_wall({
            id: '<?php echo $CI->rd_meta->get_value('facebook_pageid'); ?>',
            access_token: '<?php echo $CI->rd_meta->get_value('facebook_access_token'); ?>',
            limit: 15,
            effect: 'none',
            message_length: '1000',
            show_guest_entries : false,
            avatar_size : 'small',
            locale: 'en_US'
        });
        
    $("#facebook_page_album").FacebookAlbumBrowser({
        account: "<?php echo $CI->rd_meta->get_value('facebook_pageid'); ?>",
        accessToken: "<?php echo $CI->rd_meta->get_value('facebook_access_token'); ?>",
        //skipAlbums: ["Profile Pictures", "Timeline Photos"],
        showAccountInfo: false,
        showImageCount: true,
        showImageText: true,
        lightbox: true,
                photosCheckbox: true,
                photoChecked: function(photo){
                    console.log("PHOTO CHECKED: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                    console.log("CHECKED PHOTOS COUNT: " + this.checkedPhotos.length);
                },
                photoUnchecked: function (photo) {
                    console.log("PHOTO UNCHECKED: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                    console.log("CHECKED PHOTOS COUNT: " + this.checkedPhotos.length);
                },
                albumSelected: function (photo) {
                    console.log("ALBUM CLICK: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                },
                photoSelected: function (photo) {
                    console.log("PHOTO CLICK: " + photo.id + " - " + photo.url + " - " + photo.thumb);
                }
            });
            
    setTimeout(function(){
		$('body').addClass('loaded');
		$('h1').css('color','#222222')
	}, 500);
    
});
</script>
			
		