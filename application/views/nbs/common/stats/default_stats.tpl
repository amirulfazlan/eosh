<?php 
$CI =& get_instance(); 
$user = $this->flexi_auth->get_user_by_identity_row_array();
$logged_id = $CI->flexi_auth->get_user_group_id();
?>
<div class="row">
    <div class="col-md-12">
    <div class="alert alert-block alert-warning fade in">
								<h4 class="alert-heading">Warning!</h4>
								<p>
									 This is default stats. Please create new dashboard view file. views/nbs/common/stats/<?php echo $logged_id ?>_stats.tpl
								</p>
								
							</div>
    </div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-comments"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php //echo $CI->far_data->count_total_winner(); ?>
							</div>
							<div class="desc">
								 Total Winner<sup>(s)</sup>
							</div>
						</div>
						<a class="more" href="<?php echo base_url(); ?>winner/list_winner">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
							
							</div>
							<div class="desc">
								 Total Collection
							</div>
						</div>
						<a class="more" href="#">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<i class="fa fa-facebook"></i>
						</div>
						<div class="details">
							<div class="number" id="dashboard_stats_total_fb_friend">
								 0
							</div>
							<div class="desc">
								 Total Facebook Friend
							</div>
						</div>
						<a class="more" href="#">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat purple-plum">
						<div class="visual">
							<i class="fa fa-money"></i>
						</div>
						<div class="details">
							<div class="number">
								
							</div>
							<div class="desc">
								 Total Wallet
							</div>
						</div>
						<a class="more" href="#">
						View more <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
			</div>