<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	 <div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			 <div class="panel-heading clearfix">
                <h4 class="panel-title">Update User Group</h4>
             </div>
			 <div class="panel-heading clearfix">
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/manage_user_groups">Manage User Groups</a>
			</div>
			 <div class="panel-body">
				<form class="form-horizontal"  action="" method="post">
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Group Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="update_group_name" value="<?php echo set_value('update_group_name', $group[$this->flexi_auth->db_column('user_group', 'name')]);?>">
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Group Description</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="update_group_description"><?php echo set_value('update_group_description', $group[$this->flexi_auth->db_column('user_group', 'description')]);?></textarea>
						</div>
					</div>
				   <div class="form-group">
						<?php $ugrp_admin = ($group[$this->flexi_auth->db_column('user_group', 'admin')] == 1) ;?>
						<label class="col-sm-2 control-label">Is Admin Group</label>
						<div class="col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="update_group_admin" value="1" <?php echo set_checkbox('update_group_admin', 1, $ugrp_admin);?>/>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">User Group Privileges</label>
						<div class="col-sm-10">
							<p class="form-control-static"><a href="<?php echo $base_url;?>auth_admin/update_group_privileges/<?php echo $group['ugrp_id']; ?>">Manage Privileges for this User Group</a></p>
						</div>
					</div>
					 <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success" name="update_user_group" value="Submit">Update Group</button>
						</div>
					</div>
				</form>
			 </div>
		</div>
	 
	 </div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 