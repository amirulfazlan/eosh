<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			<div class="panel-heading clearfix">
				<h4 class="panel-title">Manage User Groups</h4>
			</div>
			<div class="panel-heading clearfix">
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/insert_user_group">Insert New User Group</a>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
				<?php echo form_open(current_url());	?>
				<table class="table">
					<thead>
						<tr>
							<th>Group Name</th>
							<th>Description</th>
							<th>Is Admin Group</th>
							<th>User Group Privileges</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($user_groups as $group) { ?>
							<tr>
								<th style="font-weight:normal;">
									<a href="<?php echo $base_url;?>auth_admin/update_user_group/<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>">
										<?php echo $group[$this->flexi_auth->db_column('user_group', 'name')];?>
									</a>
								</th>
								<th style="font-weight:normal;"><?php echo $group[$this->flexi_auth->db_column('user_group', 'description')];?></th>
								<th style="font-weight:normal; text-align:center;"><?php echo ($group[$this->flexi_auth->db_column('user_group', 'admin')] == 1) ? "Yes" : "No";?></td>
								<th style="font-weight:normal; text-align:center; ">
									<a href="<?php echo $base_url.'auth_admin/update_group_privileges/'.$group[$this->flexi_auth->db_column('user_group', 'id')];?>">Manage</a>
								</th>
								<th style="font-weight:normal; text-align:center; ">
								<?php if ($this->flexi_auth->is_privileged('Delete User Groups')) { ?>
									<input type="checkbox" name="delete_group[<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>]" value="1"/>
								<?php } else { ?>
									<input type="checkbox" disabled="disabled"/>
									<small>Not Privileged</small>
									<input type="hidden" name="delete_group[<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>]" value="0"/>
								<?php } ?>
								</th>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<?php $disable = (! $this->flexi_auth->is_privileged('Update User Groups') && ! $this->flexi_auth->is_privileged('Delete User Groups')) ? 'disabled="disabled"' : NULL;?>
				<button type="submit" class="btn btn-danger" name="update_user_privilege" value="Delete Checked User Groups" <?php echo $disable; ?>>Delete Checked User Groups</button>
				<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 