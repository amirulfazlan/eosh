<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			<div class="panel-heading clearfix">
				<h4 class="panel-title">Update User Privileges of <?php echo $user['upro_first_name'].' '.$user['upro_last_name']; ?>, Member of Group <?php echo $user['ugrp_name']; ?></h4>
			</div>
			<div class="panel-heading clearfix">
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/manage_user_accounts">Manage User Accounts</a>
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/update_user_account/<?php echo $user['upro_uacc_fk']; ?>">Update Users Account</a>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
				<?php echo form_open(current_url());	?>
				<table class="table">
					<thead>
						<tr>
							<th>Privilege Name</th>
							<th>Description</th>
							<th>User Has Individual Privilege</th>
							<th>Has Privilege From User Group</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($privileges as $privilege) { ?>
							<tr>
								<th style="font-weight:normal;">
									<input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')]; ?>][id]" value="<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')]; ?>"/>
									<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'name')];?>
								</th>
								<th style="font-weight:normal;"><?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'description')];?></td>
								<th style="font-weight:normal; text-align:center;">
									<?php 
										// Define form input values.
										$current_status = (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $user_privileges)) ? 1 : 0; 
										$new_status = (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $user_privileges)) ? 'checked="checked"' : NULL;
									?>
									<input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][current_status]" value="<?php echo $current_status ?>"/>
									<input type="hidden" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][new_status]" value="0"/>
									<input type="checkbox" name="update[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>][new_status]" value="1" <?php echo $new_status ?>/>
								</th>
                                <th style="font-weight:normal; text-align:center;">
									<?php echo (in_array($privilege[$this->flexi_auth->db_column('user_privileges', 'id')], $group_privileges) ? 'Yes' : 'No'); ?>
                                </th>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<button type="submit" class="btn btn-danger" name="update_user_privilege" value="Update User Privileges">Update</button>
				<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 