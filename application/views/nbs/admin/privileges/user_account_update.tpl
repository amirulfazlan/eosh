<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	 <div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			 <div class="panel-heading clearfix">
                <h4 class="panel-title">Update Account of <?php echo $user['upro_first_name'].' '.$user['upro_last_name']; ?></h4>
             </div>
			 <div class="panel-body">
				<form class="form-horizontal"  action="" method="post">
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">First Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="update_first_name" value="<?php echo set_value('update_first_name',$user['upro_first_name']);?>">
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Last Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="update_last_name" value="<?php echo set_value('update_last_name',$user['upro_last_name']);?>">
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Phone Number</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="update_phone_number" value="<?php echo set_value('update_phone_number',$user['upro_phone']);?>">
						</div>
					</div>
				   <div class="form-group">
						<?php $newsletter = ($user['upro_newsletter'] == 1) ;?>
						<label class="col-sm-2 control-label">Newsletter</label>
						<div class="col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="newsletter" name="update_newsletter" value="1" <?php echo set_checkbox('update_newsletter','1',$newsletter); ?>/>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						 <label class="col-sm-2 control-label">Email address</label>
						 <div class="col-sm-10">
							<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" name="update_email_address" value="<?php echo set_value('update_email_address',$user[$this->flexi_auth->db_column('user_acc', 'email')]);?>">
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="update_username" value="<?php echo set_value('update_username',$user[$this->flexi_auth->db_column('user_acc', 'username')]);?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Group</label>
						<div class="col-sm-10">
							<select class="form-control m-b-sm" name="update_group">
								<?php foreach($groups as $group) { ?>
									<?php $user_group = ($group[$this->flexi_auth->db_column('user_group', 'id')] == $user[$this->flexi_auth->db_column('user_acc', 'group_id')]) ? TRUE : FALSE;?>
									<option value="<?php echo $group[$this->flexi_auth->db_column('user_group', 'id')];?>" <?php echo set_select('update_group', $group[$this->flexi_auth->db_column('user_group', 'id')], $user_group);?>>
										<?php echo $group[$this->flexi_auth->db_column('user_group', 'name')];?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Privileges</label>
						<div class="col-sm-10">
							<p class="form-control-static"><a href="<?php echo $base_url.'auth_admin/update_user_privileges/'.$user[$this->flexi_auth->db_column('user_acc', 'id')];?>" class="tooltip_trigger"
									title="Manage a users access privileges.">Manage User Privileges</a></p>
						</div>
					</div>
					 <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success" name="update_users_account" value="Submit">Update Account</button>
						</div>
					</div>
				</form>
			 </div>
		</div>
	 
	 </div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 