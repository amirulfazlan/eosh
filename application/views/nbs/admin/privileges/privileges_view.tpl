<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			<div class="panel-heading clearfix">
				<h4 class="panel-title">Manage Privileges</h4>
			</div>
			<div class="panel-heading clearfix">
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/insert_privilege">Insert New Privilege</a>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
				<?php echo form_open(current_url());	?>
				<table class="table">
					<thead>
						<tr>
							<th>Privilege Name</th>
							<th>Description</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($privileges as $privilege) { ?>
							<tr>
								<td>
									<a href="<?php echo $base_url;?>auth_admin/update_privilege/<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>">
										<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'name')];?>
									</a>
								</td>
								<td><?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'description')];?></td>
								<td class="align_ctr">
								<?php if ($this->flexi_auth->is_privileged('Delete Users')) { ?>
									<input type="checkbox" name="delete_privilege[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>]" value="1"/>
								<?php } else { ?>
									<input type="checkbox" disabled="disabled"/>
									<small>Not Privileged</small>
									<input type="hidden" name="delete_privilege[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>]" value="0"/>
								<?php } ?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<?php $disable = (! $this->flexi_auth->is_privileged('Update Privileges') && ! $this->flexi_auth->is_privileged('Delete Privileges')) ? 'disabled="disabled"' : NULL;?>
				<button type="submit" class="btn btn-danger"  name="submit" value="Delete Checked Privileges" <?php echo $disable; ?>>Delete Checked Privileges</button>
				<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 