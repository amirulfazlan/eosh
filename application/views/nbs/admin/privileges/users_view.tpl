<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title"><?php echo $page_title;?></h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                         <table id="list_user_stat" class="display table">
                                            <thead>
												<tr>
													<th>Email</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>User Group</th>
													<?php if (isset($status) && $status == 'failed_login_users') { ?>
														<th>Failed Attempts</th>
													<?php } ?>
													<th>Status</th>
												</tr>
											</thead>
                                            <?php if (! empty($users)) { ?>
											<tbody>
											<?php foreach ($users as $user) { ?>
												<tr>
													<td>
														<a href="<?php echo $base_url;?>auth_admin/update_user_account/<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>">
															<?php echo $user[$this->flexi_auth->db_column('user_acc', 'email')];?>
														</a>
													</td>
													<td>
														<?php echo $user['upro_first_name'];?>
													</td>
													<td>
														<?php echo $user['upro_last_name'];?>
													</td>
													<td>
														<?php echo $user[$this->flexi_auth->db_column('user_group', 'name')];?>
													</td>
												<?php if (isset($status) && $status == 'failed_login_users') { ?>
													<td>
														<?php echo $user[$this->flexi_auth->db_column('user_acc', 'failed_logins')];?>
													</td>
												<?php } ?>
													<td>
														<?php echo ($user[$this->flexi_auth->db_column('user_acc', 'active')] == 1) ? 'Active' : 'Inactive';?>
													</td>
												</tr>
											<?php } ?>
											</tbody>
										<?php } else { ?>
											<tbody>
												<tr>
													<td colspan="<?php echo (isset($status) && $status == 'failed_login_users') ? '6' : '5'; ?>" class="highlight_red">
														No users are available.
													</td>
												</tr>
											</tbody>
										<?php } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 
 


