<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title"><?php echo $CI->rd_menu->page_title()->page_title; ?> </h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                         <table id="manage_user_acc" class="display table">
                                            <thead>
												<tr>
													<th>Email</th>
													<th>First Name</th>
													<th>Last Name</th>
													<th>User Group</th>
													<th>User Privileges</th>
													<th>Account Suspended</th>
													<th>Delete</th>
												</tr>
											</thead>
                                            <tbody>
													<?php foreach ($users as $user) { ?>
													<tr>
														<td>
															<a href="<?php echo $base_url.'auth_admin/update_user_account/'.$user[$this->flexi_auth->db_column('user_acc', 'id')];?>">
																<?php echo $user[$this->flexi_auth->db_column('user_acc', 'email')];?>
															</a>
														</td>
														<td>
															<?php echo $user['upro_first_name'];?>
														</td>
														<td>
															<?php echo $user['upro_last_name'];?>
														</td>
														<td>
															<?php echo $user[$this->flexi_auth->db_column('user_group', 'name')];?>
														</td>
														<td>
															<a href="<?php echo $base_url.'auth_admin/update_user_privileges/'.$user[$this->flexi_auth->db_column('user_acc', 'id')];?>">Manage</a>
														</td>
														<td>
															<input type="hidden" name="current_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="<?php echo $user[$this->flexi_auth->db_column('user_acc', 'suspend')];?>"/>
															<!-- A hidden 'suspend_status[]' input is included to detect unchecked checkboxes on submit -->
															<input type="hidden" name="suspend_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="0"/>
														
														<?php if ($this->flexi_auth->is_privileged('Update Users')) { ?>
															<input type="checkbox" name="suspend_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="1" <?php echo ($user[$this->flexi_auth->db_column('user_acc', 'suspend')] == 1) ? 'checked="checked"' : "";?>/>
														<?php } else { ?>
															<input type="checkbox" disabled="disabled"/>
															<small>Not Privileged</small>
															<input type="hidden" name="suspend_status[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="0"/>
														<?php } ?>
														</td>
														<td>
														<?php if ($this->flexi_auth->is_privileged('Delete Users')) { ?>
															<input type="checkbox" name="delete_user[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="1"/>
														<?php } else { ?>
															<input type="checkbox" disabled="disabled"/>
															<small>Not Privileged</small>
															<input type="hidden" name="delete_user[<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>]" value="0"/>
														<?php } ?>
														</td>
													</tr>
												<?php } ?>
												</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 
 


