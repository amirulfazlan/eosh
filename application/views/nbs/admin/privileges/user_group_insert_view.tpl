<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	 <div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			 <div class="panel-heading clearfix">
                <h4 class="panel-title">Insert User Group</h4>
             </div>
			 <div class="panel-heading clearfix">
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/manage_user_groups">Manage User Groups</a>
			</div>
			 <div class="panel-body">
				<form class="form-horizontal"  action="" method="post">
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Group Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="insert_group_name" value="<?php echo set_value('insert_group_name');?>">
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Group Description</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="insert_group_description"><?php echo set_value('insert_group_description');?></textarea>
						</div>
					</div>
				   <div class="form-group">
						<label class="col-sm-2 control-label">Is Admin Group</label>
						<div class="col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="insert_group_admin" value="1" <?php echo set_checkbox('insert_group_admin',1);?>/>
								</label>
							</div>
						</div>
					</div>
					 <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success" name="insert_user_group" value="Submit">Insert Group</button>
						</div>
					</div>
				</form>
			 </div>
		</div>
	 
	 </div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 