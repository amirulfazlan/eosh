<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	 <div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			 <div class="panel-heading clearfix">
                <h4 class="panel-title">Update Privilege</h4>
             </div>
			 <div class="panel-heading clearfix">
				<a class="btn btn-success" href="<?php echo $base_url;?>auth_admin/manage_privileges">Manage Privileges</a>
			</div>
			 <div class="panel-body">
				<form class="form-horizontal"  action="" method="post">
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Privilege Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="input-Default" name="update_privilege_name" value="<?php echo set_value('update_privilege_name', $privilege[$this->flexi_auth->db_column('user_privileges', 'name')]);?>">
						</div>
					</div>
					<div class="form-group">
						<label for="input-Default" class="col-sm-2 control-label">Privilege Description</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="update_privilege_description"><?php echo set_value('update_privilege_description', $privilege[$this->flexi_auth->db_column('user_privileges', 'description')]);?></textarea>
						</div>
					</div>
					 <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success" name="update_privilege" value="Submit">Update Privilege</button>
						</div>
					</div>
				</form>
			 </div>
		</div>
	 
	 </div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 