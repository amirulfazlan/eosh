<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<?php if (! empty($message)) { ?>
			<div class="alert alert-info alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $message; ?>
			</div>
			<?php } ?>
			<div class="panel-heading clearfix">
				<h4 class="panel-title">User Accounts Not Activated in 31 Days</h4>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
				<?php echo form_open(current_url());	?>
				<table class="table">
					<thead>
						<tr>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>User Group</th>
							<th>Status</th>
						</tr>
					</thead>
					<?php if (!empty($users)) { ?>
					<tbody>
						<?php foreach ($users as $user) { ?>
							<tr>
								<td>
									<a href="<?php echo $base_url;?>auth_admin/update_user_account/<?php echo $user[$this->flexi_auth->db_column('user_acc', 'id')];?>">
										<?php echo $user[$this->flexi_auth->db_column('user_acc', 'email')];?>
									</a>
								</td>
								<td>
									<?php echo $user['upro_first_name'];?>
								</td>
								<td>
									<?php echo $user['upro_last_name'];?>
								</td>
								<td class="align_ctr">
									<?php echo $user[$this->flexi_auth->db_column('user_group', 'name')];?>
								</td>
								<td class="align_ctr">
									<?php echo ($user[$this->flexi_auth->db_column('user_acc', 'active')] == 1) ? 'Active' : 'Inactive';?>
								</td>
							</tr>
						<?php } ?>
						</tbody>
						<button type="submit" class="btn btn-danger" name="delete_unactivated"  value="Delete Listed Users">Delete Listed Users</button>
						<?php } else { ?>
						<tbody>
							<tr>
								<td colspan="5" class="highlight_red">
									No users are available.
								</td>
							</tr>
						</tbody>
					<?php } ?>
				</table>
				<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 