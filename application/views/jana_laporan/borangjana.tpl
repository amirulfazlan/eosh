<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> PENILAIAN UMUM <span class="step-title">
								Step 1 of 4 </span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="display_report" class="form-horizontal" name="test_form" id="submit_form" method="POST" target="POPUPW"
    onsubmit="window.open('about:blank','POPUPW', 'width=800,height=700');">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Borang Utama </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												A </span>
												<span class="desc">
												<i class="fa fa-check"></i> Butiran Laporan </span>
												</a>
											</li>
											
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												Terdapat Ralat dalam pemarkahan, sila semak semula.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Masukkan Butiran Laporan Yang Ingin Dijana</h3>
												<div class="form-group">
													<label class="control-label col-md-3">PTJ <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<select name="ptj_group" class="form-control">
															<option value="">- SILA PILIH -</option>
															
															<?php foreach($ptj_list as $a => $b){ 
																echo "<option value='$b[id_ptj]'>$b[nama_ptj]</option/>";
															} ?>
														</select>
														<span class="help-block">
														Pilih PTJ Untuk Dijana Laporan </span>
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="control-label col-md-3" for="bilanganaudit">Jenis Penilaian</label>
														<div class="col-md-4">
															<label class="radio-inline" for="jenispenilaian-1">
																<input type="radio" name="jenispenilaian" id="jenispenilaian-1" value="1">
																Penilaian Umum
															</label>
															<label class="radio-inline" for="jenispenilaian-2">
																<input type="radio" name="jenispenilaian" id="jenispenilaian-2" value="2">
																Penilaian Khusus
															</label>
															
														</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3" for="bilanganaudit">Jenis Laporan</label>
														<div class="col-md-4">
															<label class="radio-inline" for="jenislaporan-1">
																<input type="radio" name="jenislaporan" id="jenislaporan-1" value="1">
																Laporan Ringkas
															</label>
															<label class="radio-inline" for="jenislaporan-2">
																<input type="radio" name="jenislaporan" id="jenislaporan-2" value="2">
																Laporan Tak Ringkas
															</label>
															
														</div>
												</div>
												
												<div class="form-group">
													<label class="control-label col-md-3">Bilangan Audit <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<select name="bil_audit" class="form-control">
															<option value="">- SILA PILIH -</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
														</select>
														<span class="help-block">
														Pilih Bilangan Audit </span>
													</div>
												</div>
												
												</div>
												
												<button type="submit" class="btn green button-submit">Hantar</button>
										</form>
										
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 

<script>


jQuery(document).ready(function() {  
  // FormWizard.init();
});
</script>
 


