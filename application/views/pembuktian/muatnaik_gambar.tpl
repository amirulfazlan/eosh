<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/css/jquery.fileupload.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css">


			<div class="row">
				<div class="col-md-12">
					<blockquote>
						<p style="font-size:16px">
							 Sila muatnaik gambar - gambar pembuktian penambahbaikan di sini.
						</p>
					</blockquote>
					<br>
					<form id="fileuploads" action="" method="POST" enctype="multipart/form-data">
						<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
						<div class="row fileupload-buttonbar">
							<div class="col-lg-7">
								<!-- The fileinput-button span is used to style the file input field as button -->
								<input type="file" name="photo">
								<br>
								
								<button type="submit" class="btn blue start">
								<i class="fa fa-upload"></i>
								<span>
								Muat Naik </span>
								</button>
								<button type="reset" class="btn warning cancel">
								<i class="fa fa-ban-circle"></i>
								<span>
								Batal Muatnaik </span>
								</button>
								<button type="button" class="btn red delete">
								<i class="fa fa-trash"></i>
								<span>
								Buang </span>
								</button>
								<input type="checkbox" class="toggle">
								<!-- The global file processing state -->
								<span class="fileupload-process">
								</span>
							</div>
							<!-- The global progress information -->
							<div class="col-lg-5 fileupload-progress fade">
								<!-- The global progress bar -->
								<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
									<div class="progress-bar progress-bar-success" style="width:0%;">
									</div>
								</div>
								<!-- The extended global progress information -->
								<div class="progress-extended">
									 &nbsp;
								</div>
							</div>
						</div>
						<!-- The table listing the files available for upload/download -->
						<table role="presentation" class="table table-striped clearfix">
						<tbody class="files">
						</tbody>
						</table>
					</form>
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Perhatian</h3>
						</div>
						<div class="panel-body">
							<ul>
								<li>
									 Maksimum saiz fail yang boleh dibuat naik adalah<strong>5 MB</strong> .
								</li>
								<li>
									 Hanya fail gambar(<strong>JPG, GIF, PNG</strong>) dibenarkan untuk dimuatnaik.
								</li>
								<li>
									 Fail yang dimuatnaik perlulah disertakan dengan UKMPer Ketua Pegawai Insiden.
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger label label-danger"></strong>
        </td>
        <td>
            <p class="size">Memproses...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn blue start" disabled>
                    <i class="fa fa-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn red cancel">
                    <i class="fa fa-ban"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-download fade">
                <td>
                    <span class="preview">
                        {% if (file.thumbnailUrl) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                        {% } %}
                    </span>
                </td>
                <td>
                    <p class="name">
                        {% if (file.url) { %}
                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                        {% } else { %}
                            <span>{%=file.name%}</span>
                        {% } %}
                    </p>
                    {% if (file.error) { %}
                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                    {% } %}
                </td>
                <td>
                    <span class="size">{%=o.formatFileSize(file.size)%}</span>
                </td>
                <td>
                    {% if (file.deleteUrl) { %}
                        <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                            <i class="fa fa-trash-o"></i>
                            <span>Delete</span>
                        </button>
                        <input type="checkbox" name="delete" value="1" class="toggle">
                    {% } else { %}
                        <button class="btn yellow cancel btn-sm">
                            <i class="fa fa-ban"></i>
                            <span>Cancel</span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}
    </script>			
			
			
			
			
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 

<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<script src="<?php echo base_url('media/nbs'); ?>/admin/pages/scripts/form-fileupload.js"></script>


<script>
jQuery(document).ready(function() {  
   FormWizard.init();
   FormFileUpload.init();
});
</script>
 


