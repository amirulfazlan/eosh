<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title"><?php echo $CI->rd_menu->page_title()->page_title; ?> </h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                         <table id="manage_user_acc" class="display table">
                                            <thead>
												<tr>
													<th>Nama PTJ</th>
													<th>Alamat PTJ</th>
													<th>Jenis PTJ</th>
													<th>Ketua Pegawai Insiden</th>
													
												</tr>
											</thead>
                                            <tbody>
													<?php foreach ($result as $user) {
														$id_ptj = $user['id_ptj'];
													?>
													<tr>
														<td>
															<?php echo $user['nama_ptj'];?>
														</td>
														<td>
															<?php echo $user['alamat_ptj'];?>
														</td>
														<td>
															<?php echo $user['jenis_ptj'];?>
														</td>
														
														
														<td>
															<?php echo $this->demo_auth_admin_model->get_ptj_kpi_nama($id_ptj);?>
														</td>
														<td>
															<a href="<?php echo base_url().'auth_admin/update_ptj/'.$id_ptj;?>">Edit</a>
															<a href="#" onclick="javascript:delete_func('<?php echo $id_ptj; ?>');">Delete</a>
														</td>
													</tr>
												<?php } ?>
												</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<form name="delete" id="delete" action="" method="POST">
						<input type='hidden' name="action" value="delete">
						<input type='hidden' name="id_ptj" id="id_ptj" value="">
					</form>
<script type='text/javascript'>
function delete_func(id_ptj){
	if (confirm("Delete this data ?")) {
		document.getElementById("id_ptj").value = id_ptj;
		document.delete.submit();
		//alert('Deleted');
	  } else {
		//alert('Not Deleted');
	  }
}
</script>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 
 


