<?php $this->load->view('nbs/common/header/header.tpl'); 

$post = $this->input->post();
$CI =& get_instance();

 ?>

<div class="row">    
	<div class="col-md-12">		        
		<?php if (!empty($message)) { ?>            
		<?php if(is_array($message)){ ?>             
		<?php foreach($message as $m_key => $m_val){ ?>                
		<div class="note note-success">
			<p><?php echo $m_val; ?></p>
		</div>            
		<?php } ?>            
		<?php }else{ ?>            
		<div class="note note-success">                
			<p><?php echo $message; ?></p>            
		</div>            
		<?php } ?>        
		<?php } ?>    
	</div>        
	
	<div class="col-md-12">        
		<?php if (!empty($msg_error)) { ?>            
			<?php if(is_array($msg_error)){ ?>             
				<?php foreach($msg_error as $m_key => $m_val){ ?>                
				<div class="note note-danger"><p><?php echo $m_val; ?></p></div>            
				<?php } ?>            
				<?php }else{ ?>            
				<div class="note note-danger">                
					<p><?php echo $msg_error; ?></p>            
				</div>            
				<?php } ?>       
				<?php } ?>    
	</div>        
	
	<div class="col-md-12">        
		<div class="portlet box mypasti">									
			<div class="portlet-title">										
				<div class="caption">											
					<i class="fa fa-gift"></i>Kemaskini Butiran Pusat Tanggungjawab									
				</div>																			
			</div>									
			
			<div class="portlet-body form">                                    									
				
				<!-- BEGIN FORM-->										
				<form method="post" action="<?php echo current_url(); ?>" class="form-horizontal">											
					<div class="form-body">                
                       <div class="row">                                                    
						<div class="col-md-6">														
						 <div class="form-group">															
						  <label class="control-label col-md-3">NAMA PTJ</label>															
						   <div class="col-md-9">                                                                
								<input type="text" class="form-control" name="namaptj" value="<?php echo $result[0]['nama_ptj'];?>" />		
						   </div>                                                            														
						 </div>													
						</div>     
						                          
					
					                                               

					<div class="row">   
						

						<div class="col-md-6">

							<div class="form-group">

								<label class="control-label col-md-3">ALAMAT PTJ</label>

								<div class="col-md-9">
									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-check"></i></span>
									<input type="text" class="form-control" name="alamatptj" value="<?php echo $result[0]['alamat_ptj'];?>" />
									</div>
								</div>

							</div>

						</div>
                          
					</div>    
					
					
					<div class="row"> 

						<div class="col-md-6">														
							<div class="form-group">    
								<label class="control-label col-md-3">Jenis PTJ</label> 
									<div class="col-md-9">
									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-check"></i></span>
									<input type="text" class="form-control" name="jenisptj" value="<?php echo $result[0]['jenis_ptj'];?>" />
									</div>
								</div>                    									
							</div>																
						</div>  
                          
					</div> 
                      					
				                                                                                
					<input type="hidden" name="id_ptj" value="<?php echo $result[0]['id_ptj'];?>">
					<input type="hidden" name="fa" value="submit">
					
					<div class="form-actions">                                                
						<div class="btn-set pull-right">                                                                                                      
								<button type="submit" class="btn blue" id="btn_submit" name="register_new_moderator" value="submit">KEMASKINI</button>        
						</div>																							
					</div>	
				 </div>
				</form>										
				
				<!-- END FORM-->									
				
			</div>    
		</div>    
	</div>
	
	<?php $this->load->view('nbs/common/footer/footer.tpl'); ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">