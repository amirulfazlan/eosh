<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> PENILAIAN UMUM <span class="step-title">
								Step 1 of 4 </span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="" class="form-horizontal" name="test_form" id="submit_form" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Borang Utama </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												A </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian A : KEPERLUAN UMUM </span>
												</a>
											</li>
											<li>
												<a href="#tab3" data-toggle="tab" class="step active">
												<span class="number">
												B </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian B : KESELAMATAN UMUM </span>
												</a>
											</li>
											<li>
												<a href="#tab4" data-toggle="tab" class="step">
												<span class="number">
												C </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian C : KESELAMATAN ELEKTRIK/KEMUDAHAN ELEKTRIK </span>
												</a>
											</li>
											<li>
												<a href="#tab5" data-toggle="tab" class="step">
												<span class="number">
												D </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian D : KESELAMATAN KEBAKARAN </span>
												</a>
											</li>
											<li>
												<a href="#tab6" data-toggle="tab" class="step">
												<span class="number">
												E </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian E : BENGKEL KERJA </span>
												</a>
											</li>
											<li>
												<a href="#tab7" data-toggle="tab" class="step">
												<span class="number">
												F </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian F : TEMPAT UMUM </span>
												</a>
											</li>
											<li>
												<a href="#tab8" data-toggle="tab" class="step">
												<span class="number">
												G </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian G : KAWASAN PERSEKITARAN </span>
												</a>
											</li>
											<li>
												<a href="#tab9" data-toggle="tab" class="step">
												<span class="number">
												H </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian H : AMALAN TERBAIK </span>
												</a>
											</li>
											<li>
												<a href="#tab10" data-toggle="tab" class="step">
												<span class="number">
												I </span>
												<span class="desc">
												<i class="fa fa-check"></i> Bahagian I : IMPAK KKP/5S</span>
												</a>
											</li>
											<?php if($CI->flexi_auth->get_user_group_id() == "5") {?>											<li>
												<a href="#tab11" data-toggle="tab" class="step">
												<span class="number">
												J </span>
												<span class="desc">
												<i class="fa fa-check"></i> Pengesahan Ketua Pegawai Insiden</span>
												</a>
											</li>
											<?php }?>
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												Terdapat Ralat dalam pemarkahan, sila semak semula.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Masukkan Butiran Penilaian</h3>
												<div class="form-group">
													<label class="control-label col-md-3">PTJ <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<select name="ptj_group" class="form-control">
															<option value="">- SILA PILIH -</option>
															
															<?php foreach($ptj_list as $a => $b){ 
																echo "<option value='$b[id_ptj]'>$b[nama_ptj]</option/>";
															} ?>
														</select>
														<span class="help-block">
														Pilih PTJ yang ingin dinilai </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3" for="bilaudit">Bilangan Audit</label>
														<div class="col-md-4">
															<label class="radio-inline" for="bilaudit-1">
																<input type="radio" name="bilaudit" id="bilaudit-1" value="1">
																1
															</label>
															<label class="radio-inline" for="bilaudit-2">
																<input type="radio" name="bilaudit" id="bilaudit-2" value="2">
																2
															</label>
															<label class="radio-inline" for="bilaudit-3">
																<input type="radio" name="bilaudit" id="bilaudit-3" value="3">
																3
															</label>
															<label class="radio-inline" for="bilaudit-4">
																<input type="radio" name="bilaudit" id="bilaudit-4" value="4">
																4
															</label>
														</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3" for="skopaudit">Skop Audit</label>
														<div class="col-md-8">
															<label class="radio-inline" for="skopaudit-1">
																<input type="radio" name="skopaudit" id="skopaudit-1" value="Keselamatan Dan Kesihatan Tempat Kerja">
																Keselamatan Dan Kesihatan Tempat Kerja
															</label>
															<label class="radio-inline" for="skopaudit-2">
																<input type="radio" name="skopaudit" id="skopaudit-2" value="5S">
																Persekitaran Berkualiti 5S
															</label>
															<span class="help-block">
														Pilih Skop Audit </span>
														</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3">Program/Pusat Pengajian <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="progpusat"/>
														<span class="help-block">
														Isikan Nama Program/Pusat Pengajian </span>
													</div>
												</div>
											
											
											<div class="form-group">
													<label class="control-label col-md-3">Bilik/Aras/Bangunan <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="bilikaras"/>
														<span class="help-block">
														Isikan Nama Bilik/Aras/Bangunan </span>
													</div>
											</div>
											
											<div class="form-group">
													<label class="control-label col-md-3">ZON 5S <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="zon5s"/>
														<span class="help-block">
														Isikan Zon 5S </span>
													</div>
											</div>
											
											<div class="form-group">
													<label class="control-label col-md-3">Lokasi <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<input type="text" class="form-control" name="lokasi"/>
														<span class="help-block">
														Isikan Lokasi </span>
													</div>
											</div>
											</div>
											
											<div class="tab-pane" id="tab2">
												<h3 class="block">A1 : Sudut Keselamatan Dan Kesihatan Tempat Kerja</h3>
												
												<div class="form-group">
													<label class="col-md-6 control-label" for="a1.1">Mempamerkan Dasar KKP yang ditandatangani oleh Naib Canselor dan terkini</label>
													  <div class="col-md-4">
														<label class="radio-inline" for="a1.1-0">
														  <input type="radio" name="a1.1" id="a1.1-0" value="1">
														  YA
														</label>
														<label class="radio-inline" for="a1.1-1">
														  <input type="radio" name="a1.1" id="a1.1-1" value="0">
														  TIDAK
														</label>
														<label class="radio-inline" for="a1.1-2">
														  <input type="radio" name="a1.1" id="a1.1-2" value="0">
														  TIDAK BERKAITAN
														</label>
													  </div>
												</div>
												
												
												
												
												<div class="form-group">
  <label class="col-md-6 control-label" for="a1.2">Mempamerkan carta organisasi JKKP</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.2-0">
      <input type="radio" name="a1.2" id="a1.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.2-1">
      <input type="radio" name="a1.2" id="a1.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.2-2">
      <input type="radio" name="a1.2" id="a1.2-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.3">Mempamerkan Pelan Laluan Kecemasan / Lokasi Alat Pemadam Api </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.3-0">
      <input type="radio" name="a1.3" id="a1.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.3-1">
      <input type="radio" name="a1.3" id="a1.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.3-2">
      <input type="radio" name="a1.3" id="a1.3-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.6">Mempamerkan No Telefon Kecemasan Terkini</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.6-0">
      <input type="radio" name="a1.6" id="a1.6-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.6-1">
      <input type="radio" name="a1.6" id="a1.6-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.6-2">
      <input type="radio" name="a1.6" id="a1.6-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.5">Mempamerkan Maklumat Pegawai Insiden</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.5-0">
      <input type="radio" name="a1.5" id="a1.5-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.5-1">
      <input type="radio" name="a1.5" id="a1.5-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.5-2">
      <input type="radio" name="a1.5" id="a1.5-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.4">Mempamerkan keputusan audit KKP yang terdahulu.</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.4-0">
      <input type="radio" name="a1.4" id="a1.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.4-1">
      <input type="radio" name="a1.4" id="a1.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.4-2">
      <input type="radio" name="a1.4" id="a1.4-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.7">Mempamerkan Pelan Tindakan Kecemasan/ Prosedur Pengungsian Bangunan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.7-0">
      <input type="radio" name="a1.7" id="a1.7-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.7-1">
      <input type="radio" name="a1.7" id="a1.7-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.7-2">
      <input type="radio" name="a1.7" id="a1.7-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.8">Lokasi  sudut KKP mudah dilihat </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.8-0">
      <input type="radio" name="a1.8" id="a1.8-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.8-1">
      <input type="radio" name="a1.8" id="a1.8-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.8-2">
      <input type="radio" name="a1.8" id="a1.8-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a1.9">Bahan/ maklumat di sudut KKP terkini dan sesuai  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a1.9-0">
      <input type="radio" name="a1.9" id="a1.9-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a1.9-1">
      <input type="radio" name="a1.9" id="a1.9-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a1.9-2">
      <input type="radio" name="a1.9" id="a1.9-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A2: Sudut 5S</h2>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a2.1">Mempamerkan Dasar 5S yang ditandatangan oleh Wakil Pengurusan SPKP dan terkini  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.1-0">
      <input type="radio" name="a2.1" id="a2.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.1-1">
      <input type="radio" name="a2.1" id="a2.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.1-2">
      <input type="radio" name="a2.1" id="a2.1-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a2.2">Mempamerkan Carta Organisasi SPB 5S</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.2-0">
      <input type="radio" name="a2.2" id="a2.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.2-1">
      <input type="radio" name="a2.2" id="a2.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.2-2">
      <input type="radio" name="a2.2" id="a2.2-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a2.3">Mempamerkan Objektif SPB 5S </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.3-0">
      <input type="radio" name="a2.3" id="a2.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.3-1">
      <input type="radio" name="a2.3" id="a2.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.3-2">
      <input type="radio" name="a2.3" id="a2.3-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a2.4">Mempamerkan Carta Perbatuan SPB 5S </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.4-0">
      <input type="radio" name="a2.4" id="a2.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.4-1">
      <input type="radio" name="a2.4" id="a2.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.4-2">
      <input type="radio" name="a2.4" id="a2.4-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple radioes (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="a2.5">Mempamerkan Pelan Lantai Zon 5S </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.5-0">
      <input type="radio" name="a2.5" id="a2.5-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.5-1">
      <input type="radio" name="a2.5" id="a2.5-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.5-2">
      <input type="radio" name="a2.5" id="a2.5-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a2.6">Mempamerkan Ringkasan Aktiviti 5S Zon (gambar, info terkini dan lain-lain)  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.6-0">
      <input type="radio" name="a2.6" id="a2.6-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.6-1">
      <input type="radio" name="a2.6" id="a2.6-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.6-2">
      <input type="radio" name="a2.6" id="a2.6-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a2.7">Mempamerkan Keputusan audit 5S yang terdahulu   </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.7-0">
      <input type="radio" name="a2.7" id="a2.7-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.7-1">
      <input type="radio" name="a2.7" id="a2.7-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.7-2">
      <input type="radio" name="a2.7" id="a2.7-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a2.8">Lokasi sudut 5S mudah dilihat </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.8-0">
      <input type="radio" name="a2.8" id="a2.8-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.8-1">
      <input type="radio" name="a2.8" id="a2.8-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.8-2">
      <input type="radio" name="a2.8" id="a2.8-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a2.9"> Bahan / maklumat di sudut 5S terkini dan sesuai  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a2.9-0">
      <input type="radio" name="a2.9" id="a2.9-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a2.9-1">
      <input type="radio" name="a2.9" id="a2.9-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a2.9-2">
      <input type="radio" name="a2.9" id="a2.9-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>A3: Keperluan Asas Ruang/Bilik</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a3.1"> Maklumat Pegawai Bertanggungjawab / kakitangan dipamerkan pada pintu masuk ruang / bilik  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a3.1-0">
      <input type="radio" name="a3.1" id="a3.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a3.1-1">
      <input type="radio" name="a3.1" id="a3.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a3.1-2">
      <input type="radio" name="a3.1" id="a3.1-2" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a3.2"> Mempamerkan pelan suis di ruang/bilik   </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a3.2-0">
      <input type="radio" name="a3.2" id="a3.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a3.2-1">
      <input type="radio" name="a3.2" id="a3.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a3.2-2">
      <input type="radio" name="a3.2" id="a3.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a3.3"> Mempamerkan Borang KEW.PA-7 (Senarai Aset Alih)    </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a3.3-0">
      <input type="radio" name="a3.3" id="a3.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a3.3-1">
      <input type="radio" name="a3.3" id="a3.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a3.3-2">
      <input type="radio" name="a3.2" id="a3.3-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a3.4"> Mempamerkan Etika Penggunaan ruang/bilik     </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a3.4-0">
      <input type="radio" name="a3.4" id="a3.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a3.4-1">
      <input type="radio" name="a3.4" id="a3.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a3.4-2">
      <input type="radio" name="a3.4" id="a3.4-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<h2>A4: Pernyataan Pergerakan Pegawai</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a4.1"> Mempamerkan maklumat pergerakan kakitangan yang jelas di papan Pergerakan Kakitangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.1-0">
      <input type="radio" name="a4.1" id="a4.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.1-1">
      <input type="radio" name="a4.1" id="a4.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.1-2">
      <input type="radio" name="a4.1" id="a4.1-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a4.2"> Maklumat pergerakan kakitangan terkini "(latihan, urusan rasmi,cuti, ada, dan sebagainya)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a4.2-0">
      <input type="radio" name="a4.2" id="a4.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a4.2-1">
      <input type="radio" name="a4.2" id="a4.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a4.2-2">
      <input type="radio" name="a4.2" id="a4.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<h2>A5: Pengurusan Fail</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a5.1"> Kabinet fail dilabel dan diindeks </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.1-0">
      <input type="radio" name="a5.1" id="a5.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.1-1">
      <input type="radio" name="a5.1" id="a5.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.1-2">
      <input type="radio" name="a5.1" id="a5.1-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a5.2"> Fail disusun dengan teratur mengikut nombor siri / indeks yang ditetapkan di dalam kabinet fail, mudah diperoleh/disimpan dan bersistem </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.2-0">
      <input type="radio" name="a5.2" id="a5.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.2-1">
      <input type="radio" name="a5.2" id="a5.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.2-2">
      <input type="radio" name="a5.2" id="a5.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a5.3"> Fail berbentuk ”ring” dilabel mengikut format yang telah diseragamkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.3-0">
      <input type="radio" name="a5.3" id="a5.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.3-1">
      <input type="radio" name="a5.3" id="a5.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.3-2">
      <input type="radio" name="a5.3" id="a5.3-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a5.4"> Fail mudah diperoleh/disimpan  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.4-0">
      <input type="radio" name="a5.4" id="a5.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.4-1">
      <input type="radio" name="a5.4" id="a5.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.4-2">
      <input type="radio" name="a5.4" id="a5.4-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a5.5"> Pencarian/pencapaian fail  dalam tempoh kurang 30 saat  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a5.5-0">
      <input type="radio" name="a5.5" id="a5.5-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a5.5-1">
      <input type="radio" name="a5.5" id="a5.5-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a5.5-2">
      <input type="radio" name="a5.5" id="a5.5-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>


<h2>A6: Penyimpanan Kunci </h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a6.1">Berada di tempat yang selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.1-0">
      <input type="radio" name="a6.1" id="a6.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.1-1">
      <input type="radio" name="a6.1" id="a6.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.1-2">
      <input type="radio" name="a6.1" id="a6.1-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a6.2"> Kunci tempat penyimpanan disimpan oleh kakitangan bertanggungjawab  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.2-0">
      <input type="radio" name="a6.2" id="a6.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.2-1">
      <input type="radio" name="a6.2" id="a6.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.2-2">
      <input type="radio" name="a6.2" id="a6.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a6.3"> Kunci disusun, dilabel dan diindeks  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.3-0">
      <input type="radio" name="a6.3" id="a6.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.3-1">
      <input type="radio" name="a6.3" id="a6.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.3-2">
      <input type="radio" name="a6.3" id="a6.3-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a6.4"> Indeks kunci diletakkan di tempat yang selamat dan berasingan dari tempat penyimpanan kunci  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a6.4-0">
      <input type="radio" name="a6.4" id="a6.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a6.4-1">
      <input type="radio" name="a6.4" id="a6.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a6.4-2">
      <input type="radio" name="a6.4" id="a6.4-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<h2>A7: Meja (Ruang Kerja/Bilik Kakitangan</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a7.1"> Keadaan di atas meja bersih, kemas dan teratur  </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.1-0">
      <input type="radio" name="a7.1" id="a7.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.1-1">
      <input type="radio" name="a7.1" id="a7.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.1-2">
      <input type="radio" name="a7.1" id="a7.1-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a7.2"> Tiada bahan/dokumen/peralatan di bawah meja </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.2-0">
      <input type="radio" name="a7.2" id="a7.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.2-1">
      <input type="radio" name="a7.2" id="a7.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.2-2">
      <input type="radio" name="a7.2" id="a7.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a7.3"> Laci meja dilabel mengikut item seperti “ALAT TULIS”, “DOKUMEN” dan “PERIBADI” dan kemas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a7.3-0">
      <input type="radio" name="a7.3" id="a7.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a7.3-1">
      <input type="radio" name="a7.3" id="a7.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a7.3-2">
      <input type="radio" name="a7.3" id="a7.3-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<h2>A8: Tanda Arah/Petunjuk</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a8.1"> Tanda arah/ petunjuk disediakan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a8.1-0">
      <input type="radio" name="a8.1" id="a8.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a8.1-1">
      <input type="radio" name="a8.1" id="a8.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a8.1-2">
      <input type="radio" name="a8.1" id="a8.1-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a8.2"> Tanda arah/ petunjuk mencukupi </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a8.2-0">
      <input type="radio" name="a8.2" id="a8.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a8.2-1">
      <input type="radio" name="a8.2" id="a8.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a8.2-2">
      <input type="radio" name="a8.2" id="a8.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a8.3"> Tanda arah/ petunjuk jelas </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a8.3-0">
      <input type="radio" name="a8.3" id="a8.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a8.3-1">
      <input type="radio" name="a8.3" id="a8.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a8.3-2">
      <input type="radio" name="a8.3" id="a8.3-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a8.4"> Dipasang di tempat yang strategik </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a8.4-0">
      <input type="radio" name="a8.4" id="a8.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a8.4-1">
      <input type="radio" name="a8.4" id="a8.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a8.4-2">
      <input type="radio" name="a8.4" id="a8.4-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<h2>A9: Stor/Tempat Penyimpanan </h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="a9.1"> Stor dilabel dan kemas </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a9.1-0">
      <input type="radio" name="a9.1" id="a9.1-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a9.1-1">
      <input type="radio" name="a9.1" id="a9.1-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a9.1-2">
      <input type="radio" name="a9.1" id="a9.1-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a9.2">Semua rak peralatan/alat tulis /mesin/ tempat penyimpanan  dilabel</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a9.2-0">
      <input type="radio" name="a9.2" id="a9.2-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a9.2-1">
      <input type="radio" name="a9.2" id="a9.2-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a9.2-2">
      <input type="radio" name="a9.2" id="a9.2-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a9.3">Mempunyai troli khas untuk mengangkat bahan/barang </label>
  <div class="col-md-4">
    <label class="radio-inline" for="a9.3-0">
      <input type="radio" name="a9.3" id="a9.3-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a9.3-1">
      <input type="radio" name="a9.3" id="a9.3-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a9.3-2">
      <input type="radio" name="a9.3" id="a9.3-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a9.4">Bahan/barang disusun mengikut keutamaan penggunaan dan pengeluaran secara Masuk Dahulu, Keluar Dahulu "(First In,First Out) "</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a9.4-0">
      <input type="radio" name="a9.4" id="a9.4-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a9.4-1">
      <input type="radio" name="a9.4" id="a9.4-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a9.4-2">
      <input type="radio" name="a9.4" id="a9.4-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="a9.5">Bahan/barang rosak diasingkan dengan bahan/barang yang masih boleh digunakan "</label>
  <div class="col-md-4">
    <label class="radio-inline" for="a9.5-0">
      <input type="radio" name="a9.5" id="a9.5-0" value="1">
      YA
    </label>
    <label class="radio-inline" for="a9.5-1">
      <input type="radio" name="a9.5" id="a9.5-1" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="a9.5-2">
      <input type="radio" name="a9.5" id="a9.5-2" value="0">
      TIDAK BERKAITAN
    </label>
	 
  </div>
</div>
</div>

<div class="tab-pane" id="tab3">
<h3 class="block">B1 : Peti Pertolongan Cemas</h3>
<div class="form-group">
  <label class="col-md-6 control-label" for="b1.1">Mempunyai peti pertolongan cemas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.1-1">
      <input type="radio" name="b1.1" id="b1.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b1.1-2">
      <input type="radio" name="b1.1" id="b1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.1-3">
      <input type="radio" name="b1.1" id="b1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="b1.2">Kedudukan mudah dicapai</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.2-1">
      <input type="radio" name="b1.2" id="b1.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b1.2-2">
      <input type="radio" name="b1.2" id="b1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.2-3">
      <input type="radio" name="b1.2" id="b1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="b1.3">Kandungan peti pertolongan cemas diselenggara secara berkala oleh kakitangan yang bertanggungjawab</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.3-1">
      <input type="radio" name="b1.3" id="b1.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b1.3-2">
      <input type="radio" name="b1.3" id="b1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.3-3">
      <input type="radio" name="b1.3" id="b1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="b1.4">Tidak menyimpan ubat makan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b1.4-1">
      <input type="radio" name="b1.4" id="b1.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b1.4-2">
      <input type="radio" name="b1.4" id="b1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b1.4-3">
      <input type="radio" name="b1.4" id="b1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B2: Peralatan Dan Barangan Pejabat</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="b2.1">Berkeadaan baik dan bersih</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b2.1-1">
      <input type="radio" name="b2.1" id="b2.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b2.1-2">
      <input type="radio" name="b2.1" id="b2.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b2.1-3">
      <input type="radio" name="b2.1" id="b2.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b2.2">Disimpan dengan selamat dan rapi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b2.2-1">
      <input type="radio" name="b2.2" id="b2.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b2.2-2">
      <input type="radio" name="b2.2" id="b2.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b2.2-3">
      <input type="radio" name="b2.2" id="b2.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b2.3">Mudah akses kepada tempat simpanan tinggi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b2.3-1">
      <input type="radio" name="b2.3" id="b2.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b2.3-2">
      <input type="radio" name="b2.3" id="b2.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b2.3-3">
      <input type="radio" name="b2.3" id="b2.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b2.4">Peralatan tajam disimpan dengan betul dan selamat </label>
  <div class="col-md-4">
    <label class="radio-inline" for="b2.4-1">
      <input type="radio" name="b2.4" id="b2.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b2.4-2">
      <input type="radio" name="b2.4" id="b2.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b2.4-3">
      <input type="radio" name="b2.4" id="b2.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B3 : Pintu Rintangan Api/Pintu Kecemasan</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="b3.1">Pintu kecemasan dikunci dan mudah dibuka dari dalam</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b3.1-1">
      <input type="radio" name="b3.1" id="b3.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b3.1-2">
      <input type="radio" name="b3.1" id="b3.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b3.1-3">
      <input type="radio" name="b3.1" id="b3.1-3" value="0">
      TIDAK BERKAITAN
    </label>
</div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b3.2">Pintu kecemasan bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b3.2-1">
      <input type="radio" name="b3.2" id="b3.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b3.2-2">
      <input type="radio" name="b3.2" id="b3.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b3.2-3">
      <input type="radio" name="b3.2" id="b3.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B4: Laluan Utama/Kecemasan</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="b4.1">Mempunyai label tanda arah keluar</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b4.1-1">
      <input type="radio" name="b4.1" id="b4.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b4.1-2">
      <input type="radio" name="b4.1" id="b4.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b4.11-3">
      <input type="radio" name="b4.1" id="b4.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b4.2">Lampu kecemasan yang berfungsi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b4.2-1">
      <input type="radio" name="b4.2" id="b4.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b4.2-2">
      <input type="radio" name="b4.2" id="b4.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b4.2-3">
      <input type="radio" name="b4.2" id="b4.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b4.3">Laluan dan tangga keluar bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b4.3-1">
      <input type="radio" name="b4.3" id="b4.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b4.3-2">
      <input type="radio" name="b4.3" id="b4.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b4.3-3">
      <input type="radio" name="b4.3" id="b4.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b4.4">Pelan lantai ke arah tempat berkumpul semasa kecemasan dipamerkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b4.4-1">
      <input type="radio" name="b4.4" id="b4.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b4.4-2">
      <input type="radio" name="b4.4" id="b4.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b4.4-3">
      <input type="radio" name="b4.4" id="b4.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B5: Pintu Keluar</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="b5.1">Mempunyai lampu tanda “KELUAR” yang dapat dilihat dengan jelas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b5.1-1">
      <input type="radio" name="b5.1" id="b5.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b5.1-2">
      <input type="radio" name="b5.1" id="b5.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b5.1-3">
      <input type="radio" name="b5.1" id="b5.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b5.2">Lampu tanda “KELUAR” berfungsi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b5.2-1">
      <input type="radio" name="b5.2" id="b5.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b5.2-2">
      <input type="radio" name="b5.2" id="b5.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b5.2-3">
      <input type="radio" name="b5.2" id="b5.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B6: Lantai/Dinding/Siling </h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="b6.1">Bersih / kemas (Tidak bocor / berkulat / berlubang)</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.1-1">
      <input type="radio" name="b6.1" id="b6.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b6.1-2">
      <input type="radio" name="b6.1" id="b6.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.1-3">
      <input type="radio" name="b6.1" id="b6.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-6 control-label" for="b6.2">Lantai tidak licin</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b6.2-1">
      <input type="radio" name="b6.2" id="b6.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b6.2-2">
      <input type="radio" name="b6.2" id="b6.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b6.2-3">
      <input type="radio" name="b6.2" id="b6.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>B7: Bahan Mudah Terbakar/Berbahaya</h2>

<div class="form-group">
  <label class="col-md-6 control-label" for="b7.1">Tidak disimpan di dalam pejabat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="b7.1-1">
      <input type="radio" name="b7.1" id="b7.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="b7.1-2">
      <input type="radio" name="b7.1" id="b7.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="b7.1-3">
      <input type="radio" name="b7.1" id="b7.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

<div class="tab-pane" id="tab4">
	<h3 class="block">C1 : Keselamatan Elektrik/Kemudahan Elektrik</h3>
<div class="form-group">
  <label class="col-md-6 control-label" for="c1.1">Peralatan dan kemudahan elektrik berfungsi dengan baik dan selamat</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="c1.1-1">
      <input type="radio" name="c1.1" id="c1.1-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="c1.1-2">
      <input type="radio" name="c1.1" id="c1.1-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="c1.1-3">
      <input type="radio" name="c1.1" id="c1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="c1.2">Pendawaian tambahan dilakukan oleh kakitangan yang bertauliah</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="c1.2-1">
      <input type="radio" name="c1.2" id="c1.2-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="c1.2-2">
      <input type="radio" name="c1.2" id="c1.2-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="c1.2-3">
      <input type="radio" name="c1.2" id="c1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="c1.3">Wayar/kabel (ICT dan elektrik) dalam keadaan kemas</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="c1.3-1">
      <input type="radio" name="c1.3"" id="c1.3-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="c1.3-2">
      <input type="radio" name="c1.3"" id="c1.3-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="c1.3-3">
      <input type="radio" name="c1.3"" id="c1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="c1.4">Bilik elektrik / AHU tidak disalahguna </label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="c1.4-1">
      <input type="radio" name="c1.4" id="c1.4-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="c1.4-2">
      <input type="radio" name="c1.4" id="c1.4-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="c1.4-3">
      <input type="radio" name="c1.4" id="c1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="c1.5">Peralatan elektrik ditutup setelah digunakan dan menyediakan label peringatan </label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="c1.5-1">
      <input type="radio" name="c1.5" id="c1.5-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="c1.5-2">
      <input type="radio" name="c1.5" id="c1.5-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="c1.5-3">
      <input type="radio" name="c1.5" id="c1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="c1.6">Peralatan yang rosak dilabel dan dilaporkan  </label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="c1.6-1">
      <input type="radio" name="c1.6" id="c1.6-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="c1.6-2">
      <input type="radio" name="c1.6" id="c1.6-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="c1.6-3">
      <input type="radio" name="c1.6" id="c1.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

<div class="tab-pane" id="tab5">
	<h3 class="block">D1 : Keselamatan Kebakaran</h3>
<div class="form-group">
  <label class="col-md-6 control-label" for="d1.1">APA dilabel dan diletak pada tempat yang sesuai (mudah dicapai dan bebas daripada halangan)</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.1-1">
      <input type="radio" name="d1.1" id="d1.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="d1.1-2">
      <input type="radio" name="d1.1" id="d1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.1-3">
      <input type="radio" name="d1.1" id="d1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="d1.2">APA masih sah digunakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.2-1">
      <input type="radio" name="d1.2" id="d1.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="d1.2-2">
      <input type="radio" name="d1.2" id="d1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.2-3">
      <input type="radio" name="d1.2" id="d1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="d1.3">Kad Rekod Pemeriksaan Berkala APA digantungkan pada alat/kotak alat pemadam api</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.3-1">
      <input type="radio" name="d1.3" id="d1.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="d1.3-2">
      <input type="radio" name="d1.3" id="d1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.3-3">
      <input type="radio" name="d1.3" id="d1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="d1.4">Gelung Hos berada dalam keadaan baik dan bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.4-1">
      <input type="radio" name="d1.4" id="d1.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="d1.4-2">
      <input type="radio" name="d1.4" id="d1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.4-3">
      <input type="radio" name="d1.4" id="d1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="d1.5">Alat Pecah Kaca berada dalam keadaan baik dan bebas dari halangan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.5-1">
      <input type="radio" name="d1.5" id="d1.5-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="d1.5-2">
      <input type="radio" name="d1.5" id="d1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.5-3">
      <input type="radio" name="d1.5" id="d1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-6 control-label" for="d1.6">Terdapat alat pengesan kebakaran (Contoh: Sprinkler/ Alat pengesan haba/ Alat pengesan asap)</label>
  <div class="col-md-4">
    <label class="radio-inline" for="d1.6-1">
      <input type="radio" name="d1.6" id="d1.6-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="d1.6-2">
      <input type="radio" name="d1.6" id="d1.6-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="d1.6-3">
      <input type="radio" name="d1.6" id="d1.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>




										</div>
										<div class="tab-pane" id="tab6">
	<h3 class="block">E1 : Bengkel Kerja</h3>
<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="e1.1">Semua peralatan yang didaftarkan sebagai aset perlu dilabel dengan Stiker Aset </label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.1-1">
      <input type="radio" name="e1.1" id="e1.1-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.1-2">
      <input type="radio" name="e1.1" id="e1.1-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.1-3">
      <input type="radio" name="e1.1" id="e1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="e1.2">Penyelenggaraan peralatan mengikut jadual yang disediakan</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.2-1">
      <input type="radio" name="e1.2" id="e1.2-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.2-2">
      <input type="radio" name="e1.2" id="e1.2-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.2-3">
      <input type="radio" name="e1.2" id="e1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="e1.3">Semua alat mempunyai  manual /prosedur penggunaan alat</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.3-1">
      <input type="radio" name="e1.3" id="e1.3-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.3-2">
      <input type="radio" name="e1.3" id="e1.3-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.3-3">
      <input type="radio" name="e1.3" id="e1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="e1.4">Bahan siap dan dalam penyelenggaraan diuruskan dengan baik</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.4-1">
      <input type="radio" name="e1.4" id="e1.4-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.4-2">
      <input type="radio" name="e1.4" id="e1.4-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.4-3">
      <input type="radio" name="e1.4" id="e1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="e1.5">Menyediakan tempat penyimpanan peralatan dan dilabel</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.5-1">
      <input type="radio" name="e1.5" id="e1.5-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.5-2">
      <input type="radio" name="e1.5" id="e1.5-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.5-3">
      <input type="radio" name="e1.5" id="e1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.6">Semua peralatan dibersihkan selepas diguna</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.6-1">
      <input type="radio" name="e1.6" id="e1.6-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.6-2">
      <input type="radio" name="e1.6" id="e1.6-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.6-3">
      <input type="radio" name="e1.6" id="e1.6-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="e1.7">Menyediakan tempat penyimpanan sisa</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="e1.7-1">
      <input type="radio" name="e1.7" id="e1.7-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="e1.7-2">
      <input type="radio" name="e1.7" id="e1.7-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="e1.7-3">
      <input type="radio" name="e1.7" id="e1.7-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>
	
	
	<div class="tab-pane" id="tab7">
	<h3 class="block">F1: Surau</h3>
	
<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f1.1">Surau dilabel</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.1-1">
      <input type="radio" name="f1.1" id="f1.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f1.1-2">
      <input type="radio" name="f1.1" id="f1.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.1-3">
      <input type="radio" name="f1.1" id="f1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f1.2">Ruang solat dan tempat wuduk dalam keadaan bersih</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.2-1">
      <input type="radio" name="f1.2" id="f1.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f1.2-2">
      <input type="radio" name="f1.2" id="f1.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.2-3">
      <input type="radio" name="f1.2" id="f1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f1.3">Arah kiblat ditandakan dengan jelas</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.3-1">
      <input type="radio" name="f1.3" id="f1.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f1.3-2">
      <input type="radio" name="f1.3" id="f1.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.3-3">
      <input type="radio" name="f1.3" id="f1.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f1.4">Sejadah dan kain solat bersih, kemas dan tersusun</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.4-1">
      <input type="radio" name="f1.4" id="f1.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f1.4-2">
      <input type="radio" name="f1.4" id="f1.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.4-3">
      <input type="radio" name="f1.4" id="f1.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f1.5">Mempunyai ciri-ciri keselamatan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f1.5-1">
      <input type="radio" name="f1.5" id="f1.5-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f1.5-2">
      <input type="radio" name="f1.5" id="f1.5-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f1.5-3">
      <input type="radio" name="f1.5" id="f1.5-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>F2: Gimnasium</h2>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f2.1">Setiap peralatan dipamerkan nama alatan dan prosedur penggunaan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="f2.1-1">
      <input type="radio" name="f2.1" id="f2.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f2.1-2">
      <input type="radio" name="f2.1" id="f2.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f2.1-3">
      <input type="radio" name="f2.1" id="f2.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f2.2">Ruang gimnasium bersih </label>
  <div class="col-md-4">
    <label class="radio-inline" for="f2.2-1">
      <input type="radio" name="f2.2" id="f2.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f2.2-2">
      <input type="radio" name="f2.2" id="f2.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f2.2-3">
      <input type="radio" name="f2.2" id="f2.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f2.3">Peralatan gimnasium disusun dengan teratur </label>
  <div class="col-md-4">
    <label class="radio-inline" for="f2.3-1">
      <input type="radio" name="f2.3" id="f2.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f2.3-2">
      <input type="radio" name="f2.3" id="f2.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f2.3-3">
      <input type="radio" name="f2.3" id="f2.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>F3: Papan Kenyataan Utama PTJ</h2>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f3.1">Maklumat dikemaskini</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f3.1-1">
      <input type="radio" name="f3.1" id="f3.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f3.1-2">
      <input type="radio" name="f3.1" id="f3.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f3.1-3">
      <input type="radio" name="f3.1" id="f3.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f3.2">Maklumat disusun dengan teratur </label>
  <div class="col-md-4">
    <label class="radio-inline" for="f3.2-1">
      <input type="radio" name="f3.2" id="f3.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f3.2-2">
      <input type="radio" name="f3.2" id="f3.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f3.2-3">
      <input type="radio" name="f3.2" id="f3.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>F4: Lif</h2>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f4.1">Lif berfungsi dengan baik </label>
  <div class="col-md-4">
    <label class="radio-inline" for="f4.1-1">
      <input type="radio" name="f4.1" id="f4.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f4.1-2">
      <input type="radio" name="f4.1" id="f4.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f4.1-3">
      <input type="radio" name="f4.1" id="f4.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f4.2">Mempamerkan tindakan kecemasan jika terperangkap di dalam lif</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f4.2-1">
      <input type="radio" name="f4.2" id="f4.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f4.2-2">
      <input type="radio" name="f4.2" id="f4.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f4.2-3">
      <input type="radio" name="f4.2" id="f4.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f4.3">Sijil Keselamatan Mesin Angkat yang masih sah dipamerkan </label>
  <div class="col-md-4">
    <label class="radio-inline" for="f4.3-1">
      <input type="radio" name="f4.3" id="f4.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f4.3-2">
      <input type="radio" name="f4.3" id="f4.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f4.3-3">
      <input type="radio" name="f4.3" id="f4.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f4.4">Sistem kecemasan lif berfungsi dengan baik "(loceng, intercom, kipas dan lampu)"</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f4.4-1">
      <input type="radio" name="f4.4" id="f4.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f4.4-2">
      <input type="radio" name="f4.4" id="f4.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f4.4-3">
      <input type="radio" name="f4.4" id="f4.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>F5: Tandas</h2>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f5.1">Tandas dilabel mengikut jantina dan jenis</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f5.1-1">
      <input type="radio" name="f5.1" id="f5.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f5.1-2">
      <input type="radio" name="f5.1" id="f5.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f5.1-3">
      <input type="radio" name="f5.1" id="f5.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f5.2">Tiada bau yang kurang menyenangkan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f5.2-1">
      <input type="radio" name="f5.2" id="f5.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f5.2-2">
      <input type="radio" name="f5.2" id="f5.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f5.2-3">
      <input type="radio" name="f5.2" id="f5.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f5.3">Kemudahan dan prasarana yang disediakan berkeadaan baik dan boleh digunakan dengan selamat</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f5.3-1">
      <input type="radio" name="f5.3" id="f5.3-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f5.3-2">
      <input type="radio" name="f5.3" id="f5.3-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f5.3-3">
      <input type="radio" name="f5.3" id="f5.3-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f5.4">Mempunyai jadual penyelenggaraan /pembersihan dan dipatuhi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f5.4-1">
      <input type="radio" name="f5.4" id="f5.4-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f5.4-2">
      <input type="radio" name="f5.4" id="f5.4-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f5.4-3">
      <input type="radio" name="f5.4" id="f5.4-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<h2>F6:Kemudahan Untuk OKU</h2>
<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f6.1">Tandas, tangga dan tempat letak kenderaan OKU disediakan</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f6.1-1">
      <input type="radio" name="f6.1" id="f6.1-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f6.1-2">
      <input type="radio" name="f6.1" id="f6.1-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f6.1-3">
      <input type="radio" name="f6.1" id="f6.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="f6.2">Petunjuk untuk kemudahan OKU disediakan dan mencukupi</label>
  <div class="col-md-4">
    <label class="radio-inline" for="f6.2-1">
      <input type="radio" name="f6.2" id="f6.2-1" value="1" >
      YA
    </label>
    <label class="radio-inline" for="f6.2-2">
      <input type="radio" name="f6.2" id="f6.2-2" value="0">
      TIDAK
    </label>
    <label class="radio-inline" for="f6.2-3">
      <input type="radio" name="f6.2" id="f6.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>

	<div class="tab-pane" id="tab8">
	<h3 class="block">Bahagian G : Kawasan Persekitaran</h3>
	<div class="form-group">
  <label class="col-md-4 control-label" for="g1.1">Pokok hiasan dijaga dengan baik</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="g1.1-1">
      <input type="radio" name="g1.1" id="g1.1-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="g1.1-2">
      <input type="radio" name="g1.1" id="g1.1-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="g1.1-3">
      <input type="radio" name="g1.1" id="g1.1-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>

<!-- Multiple Radios (inline) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="g1.2">Tong sampah mencukupi dan bertutup</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="g1.2-1">
      <input type="radio" name="g1.2" id="g1.2-1" value="1" >
      YA
    </label> 
    <label class="radio-inline" for="g1.2-2">
      <input type="radio" name="g1.2" id="g1.1-2" value="0">
      TIDAK
    </label> 
    <label class="radio-inline" for="g1.2-3">
      <input type="radio" name="g1.2" id="g1.2-3" value="0">
      TIDAK BERKAITAN
    </label>
  </div>
</div>
</div>
		
	<div class="tab-pane" id="tab9">
	<h3 class="block">H: Amalan Terbaik  (Nyatakan amalan terbaik mengikut penemuan auditor)</h3>	
		<div class="form-group">
  <label class="col-md-4 control-label" for="h1.1">Amalan Terbaik Pertama</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="h1.1-1">
      <input type="radio" name="h1.1" id="h1.1-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="h1.1-2">
      <input type="radio" name="h1.1" id="h1.1-2" value="0">
      TIDAK
    </label> 
    
  </div>
</div>

			<div class="form-group">
  <label class="col-md-4 control-label" for="h1.2">Amalan Terbaik Kedua</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="h1.2-1">
      <input type="radio" name="h1.2" id="h1.2-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="h1.2-2">
      <input type="radio" name="h1.2" id="h1.2-2" value="0">
      TIDAK
    </label> 
  
  </div>
</div>
</div>

	<div class="tab-pane" id="tab10">
	<h3 class="block">Bahagian I : Impak KKP/5S</h3>

		<div class="form-group">
  <label class="col-md-4 control-label" for="i1.1">Kecederaan hilang upaya kekal/kecederaan yang mendapat cuti sakit melebihi 4 hari atau  maut dapat dielakkan</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="i1.1-1">
      <input type="radio" name="i1.1" id="i1.1-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="i1.1-2">
      <input type="radio" name="i1.1" id="i1.1-2" value="0">
      TIDAK
    </label> 
    
  </div>
</div>

	<div class="form-group">
  <label class="col-md-4 control-label" for="i1.2">Kemalangan kecil/kemalangan nyaris/kejadian berbahaya di tempat kerja dapat dielakkan</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="i1.2-1">
      <input type="radio" name="i1.2" id="i1.2-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="i1.2-2">
      <input type="radio" name="i1.2" id="i1.2-2" value="0">
      TIDAK
    </label> 
    
  </div>
</div>

	<div class="form-group">
  <label class="col-md-4 control-label" for="i1.3">Bahan terbuang (kertas, plastik, perabot lama – diukur dengan kilogram / jumlah RM berdasarkan hasil jualan)</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="i1.3-1">
      <input type="radio" name="i1.3" id="i1.3-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="i1.3-2">
      <input type="radio" name="i1.3" id="i1.3-2" value="0">
      TIDAK
    </label> 
	
	</div>
</div>
	
	<div class="form-group">
  <label class="col-md-4 control-label" for="i1.4">Ruang baharu yang dapat diwujudkan hasil daripada aktiviti 5S di  ruang asal (diukur berdasarkan penggunaan ruang baharu/ pengoptimuman ruang dalam unit kaki persegi)</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="i1.4-1">
      <input type="radio" name="i1.4" id="i1.4-1" value="1">
      YA
    </label> 
    <label class="radio-inline" for="i1.4-2">
      <input type="radio" name="i1.4" id="i1.4-2" value="0">
      TIDAK
    </label> 
	
	</div>
</div>
</div>

<?php if($CI->flexi_auth->get_user_group_id() == "5") {?>
<div class="tab-pane" id="tab11">
	<h3 class="block">Pengesahan Ketua Pegawai Insiden</h3>
	
						<div class="form-group">
  <label class="col-md-4 control-label" for="pengesahan">Pengesahan Ketua Pegawai Insiden</label>
  <div class="col-md-4"> 
    <label class="radio-inline" for="pengesahan-1">
      <input type="radio" name="pengesahan" id="pengesahan-1" value="1">
      Disahkan
    </label> 
    <label class="radio-inline" for="i1.4-2">
      <input type="radio" name="pengesahan" id="pengesahan-2" value="2">
      Tidak disahkan
    </label> 
	
	</div>
</div>
		
</div> 
<?php } ?>




	

	
							<input type="hidden" name="fa" value="submit">
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Kembali </a>
												<a href="javascript:;" class="btn blue button-next">
												Seterusnya<i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript: submit_form();" class="btn green button-submit">
												Hantar <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 

<script>

function submit_form()
{
	document.test_form.submit();
}
jQuery(document).ready(function() {  
   FormWizard.init();
});
</script>
 


