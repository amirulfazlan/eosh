<?php $CI =& get_instance(); ?>
<?php $this->load->view('nbs/common/header/header.tpl'); ?> 
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box blue" id="form_wizard_1">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> PENILAIAN UMUM <span class="step-title">
								Step 1 of 4 </span>
							</div>
							<div class="tools hidden-xs">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="" class="form-horizontal" name="test_form" id="submit_form" method="POST">
								<div class="form-wizard">
									<div class="form-body">
										<ul class="nav nav-pills nav-justified steps">
											<li>
												<a href="#tab1" data-toggle="tab" class="step">
												<span class="number">
												1 </span>
												<span class="desc">
												<i class="fa fa-check"></i> Borang Utama </span>
												</a>
											</li>
											<li>
												<a href="#tab2" data-toggle="tab" class="step">
												<span class="number">
												A </span>
												<span class="desc">
												<i class="fa fa-check"></i> Butiran Laporan </span>
												</a>
											</li>
											
										</ul>
										<div id="bar" class="progress progress-striped" role="progressbar">
											<div class="progress-bar progress-bar-success">
											</div>
										</div>
										<div class="tab-content">
											<div class="alert alert-danger display-none">
												<button class="close" data-dismiss="alert"></button>
												Terdapat Ralat dalam pemarkahan, sila semak semula.
											</div>
											<div class="alert alert-success display-none">
												<button class="close" data-dismiss="alert"></button>
												Your form validation is successful!
											</div>
											<div class="tab-pane active" id="tab1">
												<h3 class="block">Masukkan Butiran Laporan Yang Ingin Dijana</h3>
												<div class="form-group">
													<label class="control-label col-md-3">PTJ <span class="required">
													* </span>
													</label>
													<div class="col-md-4">
														<select name="ptj_group" class="form-control">
															<option value="">- SILA PILIH -</option>
															<option value="1">FAKULTI TEKNOLOGI DAN SAINS MAKLUMAT</option>
															<option value="2">FAKULTI SAINS SOSIAL DAN KEMANUSIAAN</option>
															<option value="3">FAKULTI KEJURUTERAAN DAN ALAM BINA</option>
														</select>
														<span class="help-block">
														Pilih PTJ Untuk Dijana Laporan </span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3" for="jenispenilaian">Jenis Penilaian</label>
														<div class="col-md-4">
															<label class="radio-inline" for="jenispenilaian-1">
																<input type="radio" name="jenispenilaian" id="jenispenilaian-1" value="Penilaian Umum">
																Penilaian Umum
															</label>
															<label class="radio-inline" for="jenispenilaian-2">
																<input type="radio" name="jenispenilaian" id="jenispenilaian-2" value="2">
																Penilaian Khusus
															</label>
															
														</div>
												</div>
												
												</div>
											
											<div class="tab-pane" id="tab2">
												<h3 class="block">Laporan</h3>
												
												<table border="1" cellspacing="0" cellpadding="0" width="1170">
    <tbody>
        <tr>
            <td width="224" valign="top">
                <p>
                    PTJ:
                </p>
            </td>
            <td width="295" valign="top">
            </td>
            <td width="650" colspan="2" valign="top">
                <p>
                    <img width="19" height="18" src="file:///C:/Users/AMIRUL~1/AppData/Local/Temp/msohtmlclip1/01/clip_image001.png"/>
                    <img width="19" height="18" src="file:///C:/Users/AMIRUL~1/AppData/Local/Temp/msohtmlclip1/01/clip_image001.png"/>
                    Skop Audit Keselamatan dan Kesihatan Tempat Kerja
                </p>
                <p>
                    (Tandakan v pada kotak berkenaan) Persekitaran Berkualiti 5S
                </p>
            </td>
        </tr>
        <tr>
            <td width="224" valign="top">
                <p>
                    Program / Pusat Pengajian:
                </p>
            </td>
            <td width="295" valign="top">
            </td>
            <td width="343" valign="top">
                <p>
                    Lokasi:
                </p>
                <p>
                    (Pejabat / Dewan Kuliah / Stor / Bengkel / Kolej
                </p>
            </td>
            <td width="307" valign="top">
            </td>
        </tr>
        <tr>
            <td width="224" valign="top">
                <p>
                    Bilik / Aras / Bangunan:
                </p>
            </td>
            <td width="295" valign="top">
            </td>
            <td width="343" valign="top">
                <p>
                    Zon (5S) :
                </p>
            </td>
            <td width="307" valign="top">
            </td>
        </tr>
        <tr>
            <td width="224" rowspan="4" valign="top">
                <p>
                    Tarikh Pemeriksaan:
                </p>
            </td>
            <td width="295" valign="top">
                <p>
                    Audit Bil 1 :
                </p>
            </td>
            <td width="343" rowspan="4" valign="top">
                <p>
                    Masa Pemeriksaan:
                </p>
            </td>
            <td width="307" valign="top">
                <p>
                    Audit Bil 1 :
                </p>
            </td>
        </tr>
        <tr>
            <td width="295" valign="top">
                <p>
                    Audit Bil 2 :
                </p>
            </td>
            <td width="307" valign="top">
                <p>
                    Audit Bil 2 :
                </p>
            </td>
        </tr>
        <tr>
            <td width="295" valign="top">
                <p>
                    Audit Bil 3 :
                </p>
            </td>
            <td width="307" valign="top">
                <p>
                    Audit Bil 3 :
                </p>
            </td>
        </tr>
        <tr>
            <td width="295" valign="top">
                <p>
                    Audit Bil 4 :
                </p>
            </td>
            <td width="307" valign="top">
                <p>
                    Audit Bil 4 :
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    <strong>ISIKAN MARKAH PADA KOTAK BERKENAAN:</strong>
</p>
<p>
    <strong> MARKAH: YA (1) TIDAK (0) TIDAK BERKAITAN (X)</strong>
</p>
<p>
    <strong> Catatan<em>: </em>Tulisan condong <em>(italic</em>) adalah kriteria yang berkaitan dengan Keselamatan dan Kesihatan Tempat Kerja</strong>
</p>
<table border="1" cellspacing="0" cellpadding="0" width="1170">
    <thead>
        <tr>
            <td width="65" rowspan="2">
                <p align="center">
                    <strong>Bil.</strong>
                </p>
            </td>
            <td width="502" colspan="2" rowspan="2">
                <p align="center">
                    <strong>Perkara / Kriteria Audit</strong>
                </p>
            </td>
            <td width="603" colspan="4">
                <p align="center">
                    <strong>Markah</strong>
                </p>
                <p align="center">
                    <strong>(Ya (1), Tidak (0), Tidak Berkaitan (X)</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="158" valign="top">
                <p align="center">
                    <strong>Audit Bil. 1</strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p align="center">
                    <strong>Audit Bil. 2</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p align="center">
                    <strong>Audit Bil. 3</strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p align="center">
                    <strong>Audit Bil. 4<em></em></strong>
                </p>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="65" valign="top">
                <p align="center">
                    <strong>A</strong>
                </p>
            </td>
            <td width="1105" colspan="6" valign="top">
                <p>
                    <strong>KEPERLUAN UMUM </strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="center">
                    <strong>A.1</strong>
                </p>
            </td>
            <td width="1105" colspan="6" valign="top">
                <p>
                    <strong>Sudut <em>Keselamatan dan Kesihatan Tempat Kerja</em></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    1.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan Dasar KKP yang </em>
                    <em>ditandatangan oleh Naib Canselor dan terkini</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    2.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan </em>
                    <em>Carta Organisasi JKKP </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    3.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan</em>
                    <em> Pelan Laluan Kecemasan / Lokasi Alat Pemadam Api </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    4.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan Keputusan audit KKP yang terdahulu </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    5.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan Maklumat Pegawai Insiden</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    6.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan No. telefon kecemasan terkini</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    7.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan </em>
                    <em>Pelan Tindakan Kecemasan/ Prosedur Pengungsian Bangunan</em>
                    <em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    8.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>
                        Lokasi sudut KKP mudah
                        <s>
                        </s>
                        dilihat
                    </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    9.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Bahan/ maklumat di sudut KKP terkini dan sesuai </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>A.2</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Sudut 5S </strong>
                    <em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    10.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Dasar 5S yang ditandatangan oleh Wakil Pengurusan SPKP dan terkini
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    11.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Carta Organisasi SPB 5S
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    12.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Objektif SPB 5S <em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    13.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Carta Perbatuan SPB 5S
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    14.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Pelan Lantai Zon 5S
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    15.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Ringkasan Aktiviti 5S Zon (gambar, info terkini dan lain-lain)
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    16.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Keputusan audit 5S yang terdahulu
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    17.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Lokasi sudut 5S mudah
                    <s>
                    </s>
                    dilihat<em> </em><em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    18.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Bahan / maklumat di sudut 5S terkini dan sesuai<em> </em><em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="center">
                    <strong>A.3</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Keperluan Asas Ruang/Bilik</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    19.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Maklumat Pegawai Bertanggungjawab / kakitangan dipamerkan pada pintu masuk ruang / bilik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    20.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan pelan suis di ruang/bilik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    21.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Borang KEW.PA-7 (Senarai Aset Alih)
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    22.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan Etika Penggunaan ruang/bilik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="center">
                    <strong>A.4</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Pernyataan Pergerakan Kakitangan</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    23.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempamerkan maklumat pergerakan kakitangan yang jelas di papan Pergerakan Kakitangan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    24.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Maklumat pergerakan kakitangan terkini (latihan, urusan rasmi,cuti, ada, dan sebagainya)
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>A.5</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Pengurusan Fail</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    25.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Kabinet fail dilabel dan diindeks
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    26.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Fail disusun dengan teratur mengikut nombor siri / indeks yang ditetapkan di dalam kabinet fail, mudah diperoleh/disimpan dan bersistem
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    27.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Fail berbentuk 'ring'<em> </em>dilabel mengikut format yang telah diseragamkan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    28.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Fail mudah diperoleh/disimpan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    29.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Pencarian/pencapaian fail dalam tempoh kurang 30 saat
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>A.6</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Penyimpanan Kunci</strong>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    30.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Berada di tempat yang selamat
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    31.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Kunci tempat penyimpanan disimpan oleh kakitangan bertanggungjawab
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    32.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Kunci disusun, dilabel dan diindeks
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    33.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Indeks kunci diletakkan di tempat yang selamat dan berasingan dari tempat penyimpanan kunci
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>A.7</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Meja (Ruang Kerja/ Bilik Kakitangan)</strong>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    34.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Keadaan di atas meja bersih, kemas dan teratur
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    35.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Tiada bahan/dokumen/peralatan di bawah meja
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    36.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Laci meja dilabel mengikut item seperti ALAT TULIS, DOKUMEN dan PERIBADI dan kemas
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>A.8</strong>
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    <strong>Tanda Arah/ / Petunjuk</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    37.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Tanda arah/ petunjuk disediakan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    38.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Tanda arah/ petunjuk mencukupi
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    39.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Tanda arah/ petunjuk jelas
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    40.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Dipasang di tempat yang strategik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>A.9</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Stor /Tempat penyimpanan</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    41.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Stor dilabel dan kemas
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    42.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Semua rak peralatan/alat tulis /mesin/ tempat penyimpanan dilabel</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    43.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempunyai troli khas untuk mengangkat bahan/barang
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    44.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Bahan/barang disusun mengikut keutamaan penggunaan dan pengeluaran secara Masuk Dahulu, Keluar Dahulu (<em>First In,First Out</em>)
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    45.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Bahan/barang rosak diasingkan dengan bahan/barang yang masih boleh digunakan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>KESELAMATAN UMUM</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.1</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Peti Pertolongan Cemas </em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    46.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempunyai peti pertolongan cemas</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    47.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Kedudukan mudah dicapai</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    48.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Kandungan peti pertolongan cemas diselenggara secara berkala oleh kakitangan yang bertanggungjawab</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    49.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Tidak menyimpan ubat makan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.2</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Peralatan dan Barangan pejabat</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    50.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Berkeadaan baik dan bersih</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    51.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Disimpan dengan selamat dan rapi</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    52.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mudah akses kepada tempat simpanan tinggi</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    53.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Peralatan tajam disimpan dengan betul dan selamat</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.3</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Pintu Rintangan Api / Pintu Kecemasan</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    54.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Pintu kecemasan dikunci dan mudah dibuka dari dalam</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    55.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Pintu kecemasan </em>
                    <em>bebas dari halangan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.4</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Laluan Utama / Kecemasan</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    56.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempunyai label tanda arah keluar</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    57.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Lampu kecemasan yang berfungsi</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    58.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Laluan dan tangga keluar bebas dari halangan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    59.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Pelan lantai ke arah tempat berkumpul semasa kecemasan dipamerkan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.5</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Pintu Keluar</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    60.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempunyai lampu tanda KELUAR yang dapat dilihat dengan jelas</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    61.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Lampu tanda KELUAR berfungsi</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.6</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Lantai / Dinding / Siling</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    62.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Bersih / kemas (Tidak bocor / berkulat / berlubang)</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    63.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Lantai tidak licin</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>B.7</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Bahan Mudah Terbakar/Berbahaya</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    64.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Tidak disimpan di dalam pejabat</em>
                    <em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>C</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>KESELAMATAN ELEKTRIK/ KEMUDAHAN ELEKTRIK</em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    65.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Peralatan dan kemudahan elektrik berfungsi dengan baik dan selamat</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    66.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Pendawaian tambahan dilakukan oleh kakitangan yang bertauliah</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    67.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Wayar/kabel (ICT dan elektrik) dalam keadaan kemas</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    68.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Bilik elektrik / AHU tidak disalahguna </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    69.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Peralatan elektrik ditutup setelah digunakan dan menyediakan label peringatan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    70.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Peralatan yang rosak dilabel dan dilaporkan </em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>D</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>KESELAMATAN KEBAKARAN</em></strong>
                    <em></em>
                </p>
            </td>
            <td width="158" valign="top">
            </td>
            <td width="142" valign="top">
            </td>
            <td width="158" valign="top">
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>D.1</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong><em>Alat Pemadam Api (APA) / Alat Pencegah Kebakaran</em></strong>
                    <strong><em></em></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    71.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>APA dilabel dan diletak pada tempat yang sesuai (mudah dicapai dan bebas daripada halangan)</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    72.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>APA masih sah digunakan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    73.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Kad Rekod Pemeriksaan Berkala APA digantungkan pada alat/kotak alat pemadam api</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    74.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Gelung Hos berada dalam keadaan baik dan bebas dari halangan</em>
                    <em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    75.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Alat Pecah Kaca berada dalam keadaan baik dan bebas dari halangan</em>
                    <em></em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    76.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Terdapat alat pengesan kebakaran </em>
                    <em>(Contoh:</em>
                    <em> Sprinkler/ Alat pengesan haba/ Alat pengesan asap)*</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>E</strong>
                </p>
            </td>
            <td width="1105" colspan="6" valign="top">
                <p>
                    <strong>BENGKEL KERJA </strong>
                </p>
                <p>
                    <strong>
*KOMPUTER / PENJILIDAN / PERCETAKAN / KENDERAAN / MEKANIKAL / KACA / LANDSKAP / LAIN-LAIN (NYATAKAN) ______________________________                        <em>*potong yang mana tidak berkaitan</em>
                    </strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    77.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Semua peralatan yang didaftarkan sebagai aset perlu dilabel dengan Stiker Aset
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    78.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Penyelenggaraan peralatan mengikut jadual yang disediakan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    79.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Semua alat mempunyai manual /prosedur penggunaan alat
                    <s>
                    </s>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    80.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Bahan siap dan dalam penyelenggaraan diuruskan dengan baik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    81.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Menyediakan tempat penyimpanan peralatan dan dilabel
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    82.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Semua peralatan dibersihkan selepas diguna
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    83.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Menyediakan tempat penyimpanan sisa
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>F</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>TEMPAT UMUM</strong>
                </p>
            </td>
            <td width="158" valign="top">
            </td>
            <td width="142" valign="top">
            </td>
            <td width="158" valign="top">
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>F.1</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Surau</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    84.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Surau dilabel
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    85.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Ruang solat dan tempat wuduk dalam keadaan bersih
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    86.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Arah kiblat ditandakan dengan jelas
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    87.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Sejadah dan kain solat bersih, kemas dan tersusun
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    88.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Mempunyai ciri-ciri keselamatan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>F.2</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Gimnasium</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    89.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Setiap peralatan dipamerkan nama alatan dan prosedur penggunaan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    90.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Ruang gimnasium bersih
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    91.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Peralatan gimnasium disusun dengan teratur
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>F.3</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Papan Kenyataan Utama PTJ</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    92.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Maklumat dikemaskini
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    93.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Maklumat disusun dengan teratur
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>F.4</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Lif</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    94.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Lif berfungsi dengan baik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    95.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Mempamerkan tindakan kecemasan jika terperangkap di dalam lif</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    96.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Sijil Keselamatan Mesin Angkat yang masih sah dipamerkan</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    97.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <em>Sistem kecemasan lif berfungsi dengan baik (loceng, intercom, kipas dan lampu)</em>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="center">
                    <strong>F.5</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Tandas</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    98.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Tandas dilabel mengikut jantina dan jenis
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    99.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Tiada bau yang kurang menyenangkan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    100.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Kemudahan dan prasarana yang disediakan berkeadaan baik dan boleh digunakan dengan selamat
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    101.
                </p>
            </td>
            <td width="502" colspan="2">
                <p>
                    Mempunyai jadual penyelenggaraan /pembersihan dan dipatuhi
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>F.6</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>Kemudahan untuk OKU</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    102.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Tandas, tangga dan tempat letak kenderaan OKU disediakan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    103.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Pentunjuk untuk kemudahan OKU disediakan dan mencukupi
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>G</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>KAWASAN PERSEKITARAN</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    104.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Pokok hiasan dijaga dengan baik
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    105.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Tong sampah mencukupi dan bertutup
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>H</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>AMALAN TERBAIK</strong>
                </p>
                <p>
                    <strong> (Nyatakan amalan terbaik mengikut penemuan auditor)</strong>
                </p>
                <p>
                    <strong>Markah bagi kriteria nombor 106 dan 107 ialah 1 atau 0 sahaja</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    106.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Amalan Terbaik Pertama Audit Bil.1
                </p>
                <p>
                    
                </p>
                <p>
                    Amalan Terbaik Pertama Audit Bil.2
                </p>
                <p>
                    
                <p>
                    Amalan Terbaik Pertama Audit Bil.3
                </p>
                <p>
                    
                <p>
                    Amalan Terbaik Pertama Audit Bil.4
                </p>
                <p>
                    
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    107.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Amalan Terbaik Kedua Audit Bil.1
                </p>
                <p>
                    ..
                </p>
                <p>
                    Amalan Terbaik Kedua Audit Bil.2
                </p>
                <p>
                    .
                </p>
                <p>
                    Amalan Terbaik Kedua Audit Bil.3
                </p>
                <p>
                    .
                </p>
                <p>
                    Amalan Terbaik Kedua Audit Bil.4
                </p>
                <p>
                    .
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p>
                    <strong>I</strong>
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    <strong>IMPAK KKP/ 5S </strong>
                </p>
                <p>
                    <strong>Markah bagi kriteria nombor 108 - 111 ialah 1 atau 0 sahaja</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    108.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Kecederaan hilang upaya kekal/kecederaan yang mendapat cuti sakit melebihi 4 hari atau maut dapat dielakkan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    109.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Kemalangan kecil/kemalangan nyaris/kejadian berbahaya di tempat kerja dapat dielakkan
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    110.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Bahan terbuang (kertas, plastik, perabot lama  diukur dengan kilogram / jumlah RM berdasarkan hasil jualan)<strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> KG</strong>
                </p>
                <p>
                    <strong>atau</strong>
                </p>
                <p>
                    <strong>RM=</strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> KG</strong>
                </p>
                <p>
                    <strong>atau</strong>
                </p>
                <p>
                    <strong>RM=</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> KG</strong>
                </p>
                <p>
                    <strong>atau</strong>
                </p>
                <p>
                    <strong>RM=</strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> KG</strong>
                </p>
                <p>
                    <strong>atau</strong>
                </p>
                <p>
                    <strong>RM=</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="65" valign="top">
                <p align="right">
                    111.
                </p>
            </td>
            <td width="502" colspan="2" valign="top">
                <p>
                    Ruang baharu yang dapat diwujudkan hasil daripada aktiviti 5S di ruang asal (diukur berdasarkan penggunaan ruang baharu/ pengoptimuman
                    ruang dalam unit kaki persegi)
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> Kaki persegi</strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> Kaki persegi</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> Kaki persegi</strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong>YA/ TIDAK</strong>
                </p>
                <p>
                    <strong>Jika YA, nyatakan;</strong>
                </p>
                <p>
                    <strong></strong>
                </p>
                <p>
                    <strong> Kaki persegi</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="283" colspan="2" rowspan="3" valign="top">
                <p align="right">
                    <strong>JUMLAH</strong>
                </p>
                <p align="right">
                    <strong></strong>
                </p>
            </td>
            <td width="283" valign="top">
                <p align="right">
                    <strong>Ya </strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong> </strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
            </td>
            <td width="145" valign="top">
            </td>
        </tr>
        <tr>
            <td width="283" valign="top">
                <p align="right">
                    <strong>Tidak</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="283" valign="top">
                <p align="right">
                    <strong>Tidak Berkaitan</strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="142" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="158" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
            <td width="145" valign="top">
                <p>
                    <strong></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="567" colspan="3" valign="top">
                <p align="right">
                    <strong>Markah (%)</strong>
                </p>
                <p align="right">
                    <em>(Jumlah Ya) / (Jumlah Ya + Jumlah Tidak) x 100%</em>
                    <strong></strong>
                </p>
            </td>
            <td width="603" colspan="4" valign="top">
            </td>
        </tr>
        <tr>
            <td width="567" colspan="3" valign="top">
                <p align="right">
                    <strong> </strong>
                </p>
                <p align="right">
                    <strong>SKALA PEMARKAHAN:</strong>
                    <strong></strong>
                </p>
            </td>
            <td width="603" colspan="4" valign="top">
                <p>
                    Cemerlang (90-100%)
                </p>
                <p>
                    Baik (70 -89 %)
                </p>
                <p>
                    Sederhana (51-69%)
                </p>
                <p>
                    Lemah (31-50%)
                </p>
                <p>
                    Sangat Lemah (=30%)
                </p>
            </td>
        </tr>
        <tr height="0">
            <td width="65">
            </td>
            <td width="219">
            </td>
            <td width="283">
            </td>
            <td width="158">
            </td>
            <td width="142">
            </td>
            <td width="158">
            </td>
            <td width="145">
            </td>
        </tr>
    </tbody>
</table>
<table border="1" cellspacing="0" cellpadding="0" width="1168">
    <tbody>
        <tr>
            <td width="201" valign="top">
                <p>
                    <strong>Disediakan: </strong>
                </p>
            </td>
            <td width="172" valign="top">
                <p align="center">
                    <strong>Audit 1</strong>
                </p>
            </td>
            <td width="250" valign="top">
                <p align="center">
                    <strong>Audit 2</strong>
                </p>
            </td>
            <td width="313" valign="top">
                <p align="center">
                    <strong>Audit 3</strong>
                </p>
            </td>
            <td width="233" valign="top">
                <p align="center">
                    <strong>Audit 4</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Tandatangan:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Nama:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Jawatan:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Tarikh:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Jenis Audit
                </p>
                <p>
                    (Kendiri/ Silang PTJ)
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    <strong>Disahkan :</strong>
                </p>
            </td>
            <td width="172" valign="top">
                <p align="center">
                    <strong>Audit 1</strong>
                </p>
            </td>
            <td width="250" valign="top">
                <p align="center">
                    <strong>Audit 2</strong>
                </p>
            </td>
            <td width="313" valign="top">
                <p align="center">
                    <strong>Audit 3</strong>
                </p>
            </td>
            <td width="233" valign="top">
                <p align="center">
                    <strong>Audit 4</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Tandatangan:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Nama:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Jawatan:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
        <tr>
            <td width="201" valign="top">
                <p>
                    Tarikh:
                </p>
            </td>
            <td width="172" valign="top">
            </td>
            <td width="250" valign="top">
            </td>
            <td width="313" valign="top">
            </td>
            <td width="233" valign="top">
            </td>
        </tr>
    </tbody>
</table>
<p>
    Lengkapkan Laporan Penemuan Audit termasuk lampiran bergambar/cadangan bagi setiap penemuan Tidak (markah kriteria=0)
</p>
												
												
												
												
												<div>


	

	
							<input type="hidden" name="fa" value="submit">
									</div>
									<div class="form-actions">
										<div class="row">
											<div class="col-md-offset-3 col-md-9">
												<a href="javascript:;" class="btn default button-previous">
												<i class="m-icon-swapleft"></i> Kembali </a>
												<a href="javascript:;" class="btn blue button-next">
												Seterusnya<i class="m-icon-swapright m-icon-white"></i>
												</a>
												<a href="javascript: submit_form();" class="btn green button-submit">
												Hantar <i class="m-icon-swapright m-icon-white"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
<?php $this->load->view('nbs/common/footer/footer.tpl'); ?> 

<script>

function submit_form()
{
	document.test_form.submit();
}
jQuery(document).ready(function() {  
   FormWizard.init();
});
</script>
 


