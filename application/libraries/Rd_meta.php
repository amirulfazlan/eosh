<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rd_meta {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    public function get_value($meta){
        $query = $this->CI->db->query('SELECT value FROM rd_meta WHERE meta="'.$meta.'" LIMIT 1');
        return $query->row()->value;
    }
    
    public function update_value($meta, $value){
        
        //check is meta available
        $query2 = $this->CI->db->query('SELECT * FROM rd_meta WHERE meta="'.$meta.'"');
        $numrow = $query2->num_rows();
        
        //if not available, create meta
        if($numrow == 0){
            $dataInsert = array(
               'meta' => $meta
            );
            $this->CI->db->insert('rd_meta', $dataInsert); 
        }else{
            $data = array(
                'value' => $value
            );
    
            $this->CI->db->where('meta', $meta);
            $this->CI->db->update('rd_meta', $data);
        }
        
         
    }
	
    public function meta_list(){
        $query = $this->CI->db->query('SELECT * FROM rd_meta');
        $rows = $query->result_array();
        return $rows;
    }
    public function count_total_meta(){
        $query = $this->CI->db->query('SELECT COUNT(meta) as count_total FROM rd_meta');
        $rowdata = $query->row();
        return $rowdata->count_total;
    }
    public function meta_list_paging($limit, $start){
        $query = $this->CI->db->query("SELECT * FROM rd_meta LIMIT ". $start .", ".$limit);
        $rows = $query->result_array();
        return $rows;
    }
    
    public function get_array($meta){
        $val = $this->get_value($meta);
        return explode(',',$val);
    }		
	
	
	
}

?>