<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eosh_kpi_register extends CI_Controller {
 
    function __construct() 
    {
        parent::__construct();
 		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;

		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	

		// Check user is logged in as an admin.
		// For security, admin users should always sign in via Password rather than 'Remember me'.
		

		
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
	}

 
 	/**
 	 * ROSH Register KPI 
 	 */
    function eosh_register_pi()
    {	

	
		$this->load->model('demo_auth_admin_model');
		$this->data['ptj_list'] = $this->demo_auth_admin_model->selectptj();
		// Check user has privileges to view user accounts, else display a message to notify the user they do not have valid privileges.
	/*	if (! $this->flexi_auth->is_privileged('Register Staff'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have privileges to register staff.</p>');
			redirect('auth');
		}*/
		
		if ($this->input->post('register'))
		{	

			$this->load->model('eosh/pegawai_insiden/register/kpi_privileges', 'kpi_privileges');
			$this->kpi_privileges->register_account();
		}

		// Get any status message that may have been set.
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		

		$this->load->view('pegawai_insiden/register/register_staff.tpl', $this->data);
	}
	

}

/* End of file Eosh_kpi_register.php */
/* Location: ./application/controllers/kpi/register/Eosh_kpi_register.php */