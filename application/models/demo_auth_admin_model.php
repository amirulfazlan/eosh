<?php

$nama_ptj_desc[1] = "Fakulti Teknologi Dan Sains Maklumat";
$nama_ptj_desc[2] = "Fakulti Sains Sosial Dan Kemanusiaan";
$nama_ptj_desc[3] = "Fakulti Kejuruteraan Dan Alam Bina";
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Demo_auth_admin_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
		
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// User Accounts
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

 	/**
	 * get_user_accounts
	 * Gets a paginated list of users that can be filtered via the user search form, filtering by the users email and first and last names.
	 */
	function get_user_accounts()
	{
		// Select user data to be displayed.
		$sql_select = array(
			$this->flexi_auth->db_column('user_acc', 'id'),
			$this->flexi_auth->db_column('user_acc', 'email'),
			$this->flexi_auth->db_column('user_acc', 'suspend'),
			$this->flexi_auth->db_column('user_group', 'name'),
			'upro_first_name',
			'upro_last_name',
		);
		$this->flexi_auth->sql_select($sql_select);

		// For this example, prevent any 'Master Admin' users (User group id of 3) being listed to non 'Master Admin' users.
		if (! $this->flexi_auth->in_group('Master Admin'))
		{
			$sql_where[$this->flexi_auth->db_column('user_group', 'id').' !='] = 3;
			$this->flexi_auth->sql_where($sql_where);
		}	

		// Get url for any search query or pagination position.
		$uri = $this->uri->uri_to_assoc(3);

		// Set pagination limit, get current position and get total users.
		$limit = 10;
		$offset = (isset($uri['page'])) ? $uri['page'] : FALSE;		
		
		// Set SQL WHERE condition depending on whether a user search was submitted.
		if (array_key_exists('search', $uri))
		{
			// Set pagination url to include search query.
			$pagination_url = 'auth_admin/manage_user_accounts/search/'.$uri['search'].'/';
			$config['uri_segment'] = 6; // Changing to 6 will select the 6th segment, example 'controller/function/search/query/page/10'.

			// Convert uri '-' back to ' ' spacing.
			$search_query = str_replace('-',' ',$uri['search']);
								
			// Get users and total row count for pagination.
			// Custom SQL SELECT, WHERE and LIMIT statements have been set above using the sql_select(), sql_where(), sql_limit() functions.
			// Using these functions means we only have to set them once for them to be used in future function calls.
			$total_users = $this->flexi_auth->search_users_query($search_query)->num_rows();			
			
			$this->flexi_auth->sql_limit($limit, $offset);
			$this->data['users'] = $this->flexi_auth->search_users_array($search_query);
		}
		else
		{
			// Set some defaults.
			$pagination_url = 'auth_admin/manage_user_accounts/';
			$search_query = FALSE;
			$config['uri_segment'] = 4; // Changing to 4 will select the 4th segment, example 'controller/function/page/10'.
			
			// Get users and total row count for pagination.
			// Custom SQL SELECT and WHERE statements have been set above using the sql_select() and sql_where() functions.
			// Using these functions means we only have to set them once for them to be used in future function calls.
			$total_users = $this->flexi_auth->get_users_query()->num_rows();

			$this->flexi_auth->sql_limit($limit, $offset);
			$this->data['users'] = $this->flexi_auth->get_users_array();
		}
		
		// Create user record pagination.
		$this->load->library('pagination');	
		$config['base_url'] = base_url().$pagination_url.'page/';
		$config['total_rows'] = $total_users;
		$config['per_page'] = $limit; 
		$this->pagination->initialize($config); 
		
		// Make search query and pagination data available to view.
		$this->data['search_query'] = $search_query; // Populates search input field in view.
		$this->data['pagination']['links'] = $this->pagination->create_links();
		$this->data['pagination']['total_users'] = $total_users;
	}

 	/**
	 * update_user_accounts
	 * The function loops through all POST data checking the 'Suspend' and 'Delete' checkboxes that have been checked, and updates/deletes the user accounts accordingly.
	 */
	function update_user_accounts()
    {
		// If user has privileges, delete users.
		if ($this->flexi_auth->is_privileged('Delete Users')) 
		{
			if ($delete_users = $this->input->post('delete_user'))
			{
				foreach($delete_users as $user_id => $delete)
				{
					// Note: As the 'delete_user' input is a checkbox, it will only be present in the $_POST data if it has been checked,
					// therefore we don't need to check the submitted value.
					$this->flexi_auth->delete_user($user_id);
				}
			}
		}
			
		// Update User Suspension Status.
		// Suspending a user prevents them from logging into their account.
		if ($user_status = $this->input->post('suspend_status'))
		{
			// Get current statuses to check if submitted status has changed.
			$current_status = $this->input->post('current_status');
			
			foreach($user_status as $user_id => $status)
			{
				if ($current_status[$user_id] != $status)
				{
					if ($status == 1)
					{
						$this->flexi_auth->update_user($user_id, array($this->flexi_auth->db_column('user_acc', 'suspend') => 1));
					}
					else
					{
						$this->flexi_auth->update_user($user_id, array($this->flexi_auth->db_column('user_acc', 'suspend') => 0));
					}
				}
			}
		}
			
		// Save any public or admin status or error messages to CI's flash session data.
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		
		// Redirect user.
		redirect('auth_admin/manage_user_accounts');			
	}

 	/**
	 * update_user_account
	 * Updates the account and profile data of a specific user.
	 * Note: The user profile table ('demo_user_profiles') is used in this demo as an example of relating additional user data to the auth libraries account tables. 
	 */
	function update_user_account($user_id)
	{
		$this->load->library('form_validation');

		// Set validation rules.
		$validation_rules = array(
			array('field' => 'update_first_name', 'label' => 'First Name', 'rules' => 'required'),
			array('field' => 'update_last_name', 'label' => 'Last Name', 'rules' => 'required'),
			array('field' => 'update_phone_number', 'label' => 'Phone Number', 'rules' => 'required'),
			array('field' => 'update_newsletter', 'label' => 'Newsletter', 'rules' => 'integer'),
			array('field' => 'update_email_address', 'label' => 'Email Address', 'rules' => 'required|valid_email|identity_available['.$user_id.']'),
			array('field' => 'update_username', 'label' => 'Username', 'rules' => 'min_length[4]|identity_available['.$user_id.']'),
			array('field' => 'update_group', 'label' => 'User Group', 'rules' => 'required|integer')
		);

		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			// 'Update User Account' form data is valid.
			// IMPORTANT NOTE: As we are updating multiple tables (The main user account and user profile tables), it is very important to pass the
			// primary key column and value in the $profile_data for any custom user tables being updated, otherwise, the function will not
			// be able to identify the correct custom data row.
			// In this example, the primary key column and value is 'upro_id' => $user_id.
			$profile_data = array(
				'upro_id' => $user_id,
				'upro_first_name' => $this->input->post('update_first_name'),
				'upro_last_name' => $this->input->post('update_last_name'),
				'upro_phone' => $this->input->post('update_phone_number'),
				'upro_newsletter' => $this->input->post('update_newsletter'),
				$this->flexi_auth->db_column('user_acc', 'email') => $this->input->post('update_email_address'),
				$this->flexi_auth->db_column('user_acc', 'username') => $this->input->post('update_username'),
				$this->flexi_auth->db_column('user_acc', 'group_id') => $this->input->post('update_group')
			);			

			// If we were only updating profile data (i.e. no email, username or group included), we could use the 'update_custom_user_data()' function instead.
			$this->flexi_auth->update_user($user_id, $profile_data);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			
			// Redirect user.
			redirect('auth_admin/update_user_account/'.$user_id);			
		}
		
		return FALSE;
	}

 	/**
	 * delete_users
	 * Delete all user accounts that have not been activated X days since they were registered.
	 */
	function delete_users($inactive_days)
	{
		// Deleted accounts that have never been activated.
		$this->flexi_auth->delete_unactivated_users($inactive_days);

		// Save any public or admin status or error messages to CI's flash session data.
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		
		// Redirect user.
		redirect('auth_admin/manage_user_accounts');			
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// User Groups
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

  	/**
	 * manage_user_groups
	 * The function loops through all POST data checking the 'Delete' checkboxes that have been checked, and deletes the associated user groups.
	 */
   function manage_user_groups()
    {
		// Delete groups.
		if ($delete_groups = $this->input->post('delete_group'))
		{
			foreach($delete_groups as $group_id => $delete)
			{
				// Note: As the 'delete_group' input is a checkbox, it will only be present in the $_POST data if it has been checked,
				// therefore we don't need to check the submitted value.
				$this->flexi_auth->delete_group($group_id);
			}
		}

		// Save any public or admin status or error messages to CI's flash session data.
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		
		// Redirect user.
		redirect('auth_admin/manage_user_groups');			
	}
	
  	/**
	 * insert_user_group
	 * Inserts a new user group.
	 */
	function insert_user_group()
	{
		$this->load->library('form_validation');

		// Set validation rules.
		$validation_rules = array(
			array('field' => 'insert_group_name', 'label' => 'Group Name', 'rules' => 'required'),
			array('field' => 'insert_group_admin', 'label' => 'Admin Status', 'rules' => 'integer')
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			// Get user group data from input.
			$group_name = $this->input->post('insert_group_name');
			$group_desc = $this->input->post('insert_group_description');
			$group_admin = ($this->input->post('insert_group_admin')) ? 1 : 0;

			$this->flexi_auth->insert_group($group_name, $group_desc, $group_admin);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			
			// Redirect user.
			redirect('auth_admin/manage_user_groups');			
		}
	}
	
  	/**
	 * update_user_group
	 * Updates a specific user group.
	 */
	function update_user_group($group_id)
	{
		$this->load->library('form_validation');

		// Set validation rules.
		$validation_rules = array(
			array('field' => 'update_group_name', 'label' => 'Group Name', 'rules' => 'required'),
			array('field' => 'update_group_admin', 'label' => 'Admin Status', 'rules' => 'integer')
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			// Get user group data from input.
			$data = array(
				$this->flexi_auth->db_column('user_group', 'name') => $this->input->post('update_group_name'),
				$this->flexi_auth->db_column('user_group', 'description') => $this->input->post('update_group_description'),
				$this->flexi_auth->db_column('user_group', 'admin') => $this->input->post('update_group_admin')
			);			

			$this->flexi_auth->update_group($group_id, $data);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			
			// Redirect user.
			redirect('auth_admin/update_user_group/'.$group_id);			
		}
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Privileges
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

  	/**
	 * manage_privileges
	 * The function loops through all POST data checking the 'Delete' checkboxes that have been checked, and deletes the associated privileges.
	 */
    function manage_privileges()
    {
		// Delete privileges.
		if ($delete_privileges = $this->input->post('delete_privilege'))
		{
			foreach($delete_privileges as $privilege_id => $delete)
			{
				// Note: As the 'delete_privilege' input is a checkbox, it will only be present in the $_POST data if it has been checked,
				// therefore we don't need to check the submitted value.
				$this->flexi_auth->delete_privilege($privilege_id);
			}
		}

		// Save any public or admin status or error messages to CI's flash session data.
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		
		// Redirect user.
		redirect('auth_admin/manage_privileges');			
	}

  	/**
	 * insert_privilege
	 * Inserts a new privilege.
	 */
	function insert_privilege()
	{
		$this->load->library('form_validation');

		// Set validation rules.
		$validation_rules = array(
			array('field' => 'insert_privilege_name', 'label' => 'Privilege Name', 'rules' => 'required')
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			// Get privilege data from input.
			$privilege_name = $this->input->post('insert_privilege_name');
			$privilege_desc = $this->input->post('insert_privilege_description');

			$this->flexi_auth->insert_privilege($privilege_name, $privilege_desc);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			
			// Redirect user.
			redirect('auth_admin/manage_privileges');			
		}
	}
	
  	/**
	 * update_privilege
	 * Updates a specific privilege.
	 */
	function update_privilege($privilege_id)
	{
		$this->load->library('form_validation');

		// Set validation rules.
		$validation_rules = array(
			array('field' => 'update_privilege_name', 'label' => 'Privilege Name', 'rules' => 'required')
		);
		
		$this->form_validation->set_rules($validation_rules);
		
		if ($this->form_validation->run())
		{
			// Get privilege data from input.
			$data = array(
				$this->flexi_auth->db_column('user_privileges', 'name') => $this->input->post('update_privilege_name'),
				$this->flexi_auth->db_column('user_privileges', 'description') => $this->input->post('update_privilege_description')
			);			

			$this->flexi_auth->update_privilege($privilege_id, $data);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			
			// Redirect user.
			redirect('auth_admin/manage_privileges');			
		}
	}
	
   	/**
	 * update_user_privileges
	 * Updates the privileges for a specific user.
	 */
	function update_user_privileges($user_id)
    {
		// Update privileges.
		foreach($this->input->post('update') as $row)
		{
			if ($row['current_status'] != $row['new_status'])
			{
				// Insert new user privilege.
				if ($row['new_status'] == 1)
				{
					$this->flexi_auth->insert_privilege_user($user_id, $row['id']);	
				}
				// Delete existing user privilege.
				else
				{
					$sql_where = array(
						$this->flexi_auth->db_column('user_privilege_users', 'user_id') => $user_id,
						$this->flexi_auth->db_column('user_privilege_users', 'privilege_id') => $row['id']
					);
					
					$this->flexi_auth->delete_privilege_user($sql_where);
				}
			}
		}

		// Save any public or admin status or error messages to CI's flash session data.
		//$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		$this->session->set_flashdata('message', "User privileges has been successfully updated.");
		
		// Redirect user.
		redirect('auth_admin/update_user_privileges/'.$user_id);			
	}

   	/**
	 * update_group_privileges
	 * Updates the privileges for a specific user group.
	 */
	function update_group_privileges($group_id)
    {
		// Update privileges.
		foreach($this->input->post('update') as $row)
		{
			if ($row['current_status'] != $row['new_status'])
			{
				// Insert new user privilege.
				if ($row['new_status'] == 1)
				{
					$this->flexi_auth->insert_user_group_privilege($group_id, $row['id']);	
				}
				// Delete existing user privilege.
				else
				{
					$sql_where = array(
						$this->flexi_auth->db_column('user_privilege_groups', 'group_id') => $group_id,
						$this->flexi_auth->db_column('user_privilege_groups', 'privilege_id') => $row['id']
					);
					
					$this->flexi_auth->delete_user_group_privilege($sql_where);
				}
			}
		}

		// Save any public or admin status or error messages to CI's flash session data.
		$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
		
		// Redirect user.
		redirect('auth_admin/update_group_privileges/'.$group_id);			
    }
	
	
	function insert_penilaian_umum($form_data)
	{
		$ptj_group = $form_data['ptj_group'];
		$bil_audit = $form_data['bilaudit'];
		$skop_audit = $form_data['skopaudit'];
		$program_pusat = $form_data['progpusat'];
		$bilik_aras = $form_data['bilikaras'];
		$zon = $form_data['zon5s'];
		$lokasi = $form_data['lokasi'];
		
		$a1_1 = $form_data['a1_1'];
		$a1_2 = $form_data['a1_2'];
		$a1_3 = $form_data['a1_3'];
		$a1_4 = $form_data['a1_4'];
		$a1_5 = $form_data['a1_5'];
		$a1_6 = $form_data['a1_6'];
		$a1_7 = $form_data['a1_7'];
		$a1_8 = $form_data['a1_8'];
		$a1_9 = $form_data['a1_9'];
		
		$a2_1 = $form_data['a2_1'];
		$a2_2 = $form_data['a2_2'];
		$a2_3 = $form_data['a2_3'];
		$a2_4 = $form_data['a2_4'];
		$a2_5 = $form_data['a2_5'];
		$a2_6 = $form_data['a2_6'];
		$a2_7 = $form_data['a2_7'];
		$a2_8 = $form_data['a2_8'];
		$a2_9 = $form_data['a2_9'];
		
		$a3_1 = $form_data['a3_1'];
		$a3_2 = $form_data['a3_2'];
		$a3_3 = $form_data['a3_3'];
		$a3_4 = $form_data['a3_4'];
		
		$a4_1 = $form_data['a4_1'];
		$a4_2 = $form_data['a4_2'];
		
	
		$a5_1 = $form_data['a5_1'];
		$a5_2 = $form_data['a5_2'];
		$a5_3 = $form_data['a5_3'];
		$a5_4 = $form_data['a5_4'];
		$a5_5 = $form_data['a5_5'];
		
		$a6_1 = $form_data['a6_1'];
		$a6_2 = $form_data['a6_2'];
		$a6_3 = $form_data['a6_3'];
		$a6_4 = $form_data['a6_4'];
		
		$a7_1 = $form_data['a7_1'];
		$a7_2 = $form_data['a7_2'];
		$a7_3 = $form_data['a7_3'];
		
		$a8_1 = $form_data['a8_1'];
		$a8_2 = $form_data['a8_2'];
		$a8_3 = $form_data['a8_3'];
		$a8_4 = $form_data['a8_4'];
		
		$a9_1 = $form_data['a9_1'];
		$a9_2 = $form_data['a9_2'];
		$a9_3 = $form_data['a9_3'];
		$a9_4 = $form_data['a9_4'];
		$a9_5 = $form_data['a9_5'];
		
		
		
		$b1_1 = $form_data['b1_1'];
		$b1_2 = $form_data['b1_2'];
		$b1_3 = $form_data['b1_3'];
		$b1_4 = $form_data['b1_4'];
		
		$b2_1 = $form_data['b2_1'];
		$b2_2 = $form_data['b2_2'];
		$b2_3 = $form_data['b2_3'];
		$b2_4 = $form_data['b2_4'];
		
		$b3_1 = $form_data['b3_1'];
		$b3_2 = $form_data['b3_2'];
		
		$b4_1 = $form_data['b4_1'];
		$b4_2 = $form_data['b4_2'];
		$b4_3 = $form_data['b4_3'];
		$b4_4 = $form_data['b4_4'];
		
		$b5_1 = $form_data['b5_1'];
		$b5_2 = $form_data['b5_2'];
		
		$b6_1 = $form_data['b6_1'];
		$b6_2 = $form_data['b6_2'];
		
		$b7_1 = $form_data['b7_1'];
		
		$c1_1 = $form_data['c1_1'];
		$c1_2 = $form_data['c1_2'];
		$c1_3 = $form_data['c1_3'];
		$c1_4 = $form_data['c1_4'];
		$c1_5 = $form_data['c1_5'];
		$c1_6 = $form_data['c1_6'];
		
		$d1_1 = $form_data['d1_1'];
		$d1_2 = $form_data['d1_2'];
		$d1_3 = $form_data['d1_3'];
		$d1_4 = $form_data['d1_4'];
		$d1_5 = $form_data['d1_5'];
		$d1_6 = $form_data['d1_6'];
		
		$e1_1 = $form_data['e1_1'];
		$e1_2 = $form_data['e1_2'];
		$e1_3 = $form_data['e1_3'];
		$e1_4 = $form_data['e1_4'];
		$e1_5 = $form_data['e1_5'];
		$e1_6 = $form_data['e1_6'];
		$e1_7 = $form_data['e1_7'];
		
		$f1_1 = $form_data['f1_1'];
		$f1_2 = $form_data['f1_2'];
		$f1_3 = $form_data['f1_3'];
		$f1_4 = $form_data['f1_4'];
		$f1_5 = $form_data['f1_5'];
		
		$f2_1 = $form_data['f2_1'];
		$f2_2 = $form_data['f2_2'];
		$f2_3 = $form_data['f2_3'];
		
		$f3_1 = $form_data['f3_1'];
		$f3_2 = $form_data['f3_2'];
		
		$f4_1 = $form_data['f4_1'];
		$f4_2 = $form_data['f4_2'];
		$f4_3 = $form_data['f4_3'];
		$f4_4 = $form_data['f4_4'];
		
		$f5_1 = $form_data['f5_1'];
		$f5_2 = $form_data['f5_2'];
		$f5_3 = $form_data['f5_3'];
		$f5_4 = $form_data['f5_4'];
		
		$f6_1 = $form_data['f6_1'];
		$f6_2 = $form_data['f6_2'];
		
		$g1_1 = $form_data['g1_1'];
		$g1_2 = $form_data['g1_2'];
		
		$h1_1 = $form_data['h1_1'];
		$h1_2 = $form_data['h1_2'];
		
		
		$i1_1 = $form_data['i1_1'];
		$i1_2 = $form_data['i1_2'];
		$i1_3 = $form_data['i1_3'];
		$i1_4 = $form_data['i1_4'];
		
		$pengesahan_kpi = $form_data['pengesahan'];
		
		$section_a_total = $a1_1 + $a1_2 + $a1_3 + $a1_4 + $a1_5 + $a1_6 + $a1_7 + $a1_8 + $a1_9 + $a2_1 + $a2_2 + $a2_3 + $a2_4 + $a2_5 + $a2_6 + $a2_7 + $a2_8 + $a2_9
		+ $a3_1 + $a3_2 + $a3_3 + $a3_4 + $a4_1 + $a4_2 + $a5_1 + $a5_2 + $a5_3 + $a5_4 + $a5_5 + $a6_1 +$a6_2 + $a6_3 + $a6_4 + $a7_1 + $a7_2 + $a7_3 + $a8_1 + $a8_2 + $a8_3 + $a8_4
		+ $a9_1 +  $a9_2 + $a9_3 ;
		
		$section_b_total = $b1_1 + $b1_2 + $b1_3 + $b1_4 + $b2_1 + $b2_2 + $b2_3 + $b2_4 + $b3_1 + $b3_2 + $b4_1 + $b4_2 + $b4_3 + $b4_4 + $b5_1 + $b5_2 + $b6_1 + $b6_2 + $b7_1  ;
		
		$section_c_total =  $c1_1 + $c1_2 + $c1_3 + $c1_4 + $c1_5 + $c1_6 ;
		
		$section_d_total =  $d1_1 + $d1_2 + $d1_3 + $d1_4 + $d1_5 + $d1_6 ;
		
		$section_e_total =  $e1_1 + $e1_2 + $e1_3 + $e1_4 + $e1_5 + $e1_6 + $e1_7 ;
		
		$section_f_total = $f1_1 + $f1_2 + $f1_3 + $f1_4 + $f1_5 + $f2_1 + $f2_2 + $f2_3 + $f3_1 + $f3_2 + $f4_1 + $f4_2 + $f4_3 + $f4_4  + $f5_1 + $f5_2 + $f5_3 + $f5_4 + $f6_1 + $f6_2  ;  
		
		$section_g_total = $g1_1 + $g1_2;
		
		$section_h_total = $h1_1 + $h1_2;
		
		$section_i_total = $i1_1 + $i1_2 + $i1_3 + $i1_4;
		
		$totalquestion = 109;
		
		$marking_total = $section_a_total + $section_b_total + $section_c_total + $section_d_total + $section_e_total + $section_f_total + $section_g_total + $section_h_total + $section_i_total ;
		
		$dummy_score = ($marking_total * 100);
		
		$percentage_score = ($dummy_score / $totalquestion);
		
		if ($percentage_score >= 90) { 
    		
			$gred = "Cemerlang";
		}
		elseif ($percentage_score >= 70) {
    		$gred = "Baik";
		} 
		elseif ($percentage_score >= 51) {
    		$gred = "Sederhana";
		}
		elseif ($percentage_score >= 31) {
    		$gred = "Lemah";
		}
		else {
    		$gred = "Sangat Lemah";
		}
		
		$query = $this->db->query("INSERT INTO penilaian_umum (id_ptj, bil_audit, skop_audit, program_pusat, bilik_aras, lokasi, zon, a1_1, a1_2, a1_3, a1_4, a1_5, a1_6, a1_7, a1_8, a1_9, a2_1, a2_2, a2_3, a2_4, a2_5, a2_6, a2_7, a2_8, a2_9, a3_1, a3_2, a3_3, a3_4, a4_1, a4_2, a5_1, a5_2, a5_3, a5_4, a5_5, a6_1, a6_2, a6_3, a6_4, a7_1, a7_2, a7_3, a8_1, a8_2, a8_3, a8_4, a9_1, a9_2, a9_3, a9_4, a9_5, b1_1, b1_2, b1_3, b1_4, b2_1, b2_2, b2_3, b2_4, b3_1, b3_2, b4_1, b4_2, b4_3, b4_4, b5_1, b5_2, b6_1, b6_2, b7_1, c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, d1_1, d1_2, d1_3, d1_4, d1_5, d1_6, e1_1, e1_2, e1_3, e1_4, e1_5, e1_6, e1_7, f1_1, f1_2, f1_3, f1_4, f1_5, f2_1, f2_2, f2_3, f3_1, f3_2, f4_1, f4_2, f4_3, f4_4, f5_1, f5_2, f5_3, f5_4, f6_1, f6_2, g1_1, g1_2, h1_1, h1_2, i1_1, i1_2, i1_3, i1_4, jumlah_markah_a, jumlah_markah_b, jumlah_markah_c, jumlah_markah_d, jumlah_markah_e, jumlah_markah_f, jumlah_markah_g, jumlah_markah_h, jumlah_markah_i, jumlah_markah_keseluruhan, peratus_markah, gred,pengesahan_kpi)
		VALUES ('$ptj_group' ,'$bil_audit','$skop_audit','$program_pusat','$bilik_aras','$lokasi','$zon','$a1_1','$a1_2','$a1_3','$a1_4','$a1_5','$a1_6','$a1_7','$a1_8','$a1_9','$a2_1','$a2_2','$a2_3','$a2_4','$a2_5','$a2_6','$a2_7','$a2_8','$a2_9','$a3_1','$a3_2','$a3_3','$a3_4','$a4_1','$a4_2','$a5_1','$a5_2','$a5_3','$a5_4','$a5_5','$a6_1','$a6_2','$a6_3','$a6_4','$a7_1','$a7_2','$a7_3','$a8_1','$a8_2','$a8_3','$a8_4','$a9_1','$a9_2','$a9_3','$a9_4','$a9_5','$b1_1','$b1_2','$b1_3', '$b1_4', '$b2_1','$b2_2','$b2_3','$b2_4','$b3_1','$b3_2','$b4_1','$b4_2','$b4_3','$b4_4','$b5_1','$b5_2','$b6_1','$b6_2','$b7_1','$c1_1','$c1_2','$c1_3','$c1_4','$c1_5','$c1_6','$d1_1','$d1_2','$d1_3','$d1_4','$d1_5','$d1_6','$e1_1','$e1_2','$e1_3','$e1_4','$e1_5','$e1_6','$e1_7','$f1_1','$f1_2','$f1_3','$f1_4','$f1_5','$f2_1','$f2_2','$f2_3','$f3_1','$f3_2','$f4_1','$f4_2','$f4_3','$f4_4','$f5_1','$f5_2','$f5_3','$f5_4', '$f6_1', '$f6_2', '$g1_1','$g1_2','$h1_1','$h1_2','$i1_1','$i1_2','$i1_3','$i1_4','$section_a_total', '$section_b_total', '$section_c_total', '$section_d_total', '$section_e_total', '$section_f_total', '$section_g_total', '$section_h_total', '$section_i_total', '$marking_total', '$percentage_score','$gred','$pengesahan_kpi')")  ;
		
	}
	
	function insert_penilaian_khusus($form_data)
	{
		$ptj_group = $form_data['ptj_group'];
		$bil_audit = $form_data['bilaudit'];
		$skop_audit = $form_data['skopaudit'];
		$program_pusat = $form_data['progpusat'];
		$bilik_aras = $form_data['bilikaras'];
		$zon = $form_data['zon5s'];
		$lokasi = $form_data['lokasi'];
		
		$a1_1 = $form_data['a1_1'];
		$a1_2 = $form_data['a1_2'];
		$a1_3 = $form_data['a1_3'];
		$a1_4 = $form_data['a1_4'];
		$a1_5 = $form_data['a1_5'];
		$a1_6 = $form_data['a1_6'];
		$a1_7 = $form_data['a1_7'];
		$a1_8 = $form_data['a1_8'];
		$a1_9 = $form_data['a1_9'];
		
		$a2_1 = $form_data['a2_1'];
		$a2_2 = $form_data['a2_2'];
		$a2_3 = $form_data['a2_3'];
		$a2_4 = $form_data['a2_4'];
		
		$a3_1 = $form_data['a3_1'];
		$a3_2 = $form_data['a3_2'];
		
		
		$a4_1 = $form_data['a4_1'];
		$a4_2 = $form_data['a4_2'];
		$a4_3 = $form_data['a4_3'];
		$a4_4 = $form_data['a4_4'];
		$a4_5 = $form_data['a4_5'];
	
		$a5_1 = $form_data['a5_1'];
		$a5_2 = $form_data['a5_2'];
		$a5_3 = $form_data['a5_3'];
		$a5_4 = $form_data['a5_4'];
		
		
		$a6_1 = $form_data['a6_1'];
		$a6_2 = $form_data['a6_2'];
		$a6_3 = $form_data['a6_3'];
		
		
		$a7_1 = $form_data['a7_1'];
		$a7_2 = $form_data['a7_2'];
		$a7_3 = $form_data['a7_3'];
		$a7_4 = $form_data['a7_4'];
		
		$b1_1 = $form_data['b1_1'];
		$b1_2 = $form_data['b1_2'];
		$b1_3 = $form_data['b1_3'];
		$b1_4 = $form_data['b1_4'];
		
		$b2_1 = $form_data['b2_1'];
		$b2_2 = $form_data['b2_2'];
		
		$b3_1 = $form_data['b3_1'];
		$b3_2 = $form_data['b3_2'];
		
		$b4_1 = $form_data['b4_1'];
		$b4_2 = $form_data['b4_2'];
		
		$b5_1 = $form_data['b5_1'];
		$b5_2 = $form_data['b5_2'];
		$b5_3 = $form_data['b5_3'];
		$b5_4 = $form_data['b5_4'];
		
		$b6_1 = $form_data['b6_1'];
		$b6_2 = $form_data['b6_2'];
		$b6_3 = $form_data['b6_3'];
		$b6_4 = $form_data['b6_4'];
		$b6_5 = $form_data['b6_5'];
		$b6_6 = $form_data['b6_6'];
		$b6_7 = $form_data['b6_7'];
		
		$b7_1 = $form_data['b7_1'];
		
		$c1_1 = $form_data['c1_1'];
		$c1_2 = $form_data['c1_2'];
		$c1_3 = $form_data['c1_3'];
		$c1_4 = $form_data['c1_4'];
		$c1_5 = $form_data['c1_5'];
		$c1_6 = $form_data['c1_6'];
		$c1_7 = $form_data['c1_7'];
		
		$d1_1 = $form_data['d1_1'];
		$d1_2 = $form_data['d1_2'];
		$d1_3 = $form_data['d1_3'];
		$d1_4 = $form_data['d1_4'];
		$d1_5 = $form_data['d1_5'];
		$d1_6 = $form_data['d1_6'];
		$d1_7 = $form_data['d1_7'];
		$d1_8 = $form_data['d1_8'];
		$d1_9 = $form_data['d1_9'];
		$d1_10 = $form_data['d1_10'];
		
		$e1_1 = $form_data['e1_1'];
		$e1_2 = $form_data['e1_2'];
		$e1_3 = $form_data['e1_3'];
		$e1_4 = $form_data['e1_4'];
		$e1_5 = $form_data['e1_5'];
		$e1_6 = $form_data['e1_6'];
		$e1_7 = $form_data['e1_7'];
		
		$e2_1 = $form_data['e2_1'];
		$e2_2 = $form_data['e2_2'];
		$e2_3 = $form_data['e2_3'];
		$e2_4 = $form_data['e2_4'];
		$e2_5 = $form_data['e2_5'];
		$e2_6 = $form_data['e2_6'];
		$e2_7 = $form_data['e2_7'];
		$e2_8 = $form_data['e2_8'];
		$e2_9 = $form_data['e2_9'];
		$e2_10 = $form_data['e2_10'];
		$e2_11 = $form_data['e2_11'];
		$e2_12 = $form_data['e2_12'];
		$e2_13 = $form_data['e2_13'];
		$e2_14 = $form_data['e2_14'];
		$e2_15 = $form_data['e2_15'];
		$e2_16 = $form_data['e2_16'];
		$e2_17 = $form_data['e2_17'];
		$e2_18 = $form_data['e2_18'];
		$e2_19 = $form_data['e2_19'];
		$e2_20 = $form_data['e2_20'];
		
		$e3_1 = $form_data['e3_1'];
		$e3_2 = $form_data['e3_2'];
		$e3_3 = $form_data['e3_3'];
		$e3_4 = $form_data['e3_4'];
		$e3_5 = $form_data['e3_5'];
		$e3_6 = $form_data['e3_6'];
		$e3_7 = $form_data['e3_7'];
		
		$e4_1 = $form_data['e4_1'];
		$e4_2 = $form_data['e4_2'];
		$e4_3 = $form_data['e4_3'];
		$e4_4 = $form_data['e4_4'];
		$e4_5 = $form_data['e4_5'];
		$e4_6 = $form_data['e4_6'];
		
		$e5_1 = $form_data['e5_1'];
		$e5_2 = $form_data['e5_2'];
		$e5_3 = $form_data['e5_3'];
		$e5_4 = $form_data['e5_4'];
		$e5_5 = $form_data['e5_5'];
		$e5_6 = $form_data['e5_6'];
		
		$e6_1 = $form_data['e6_1'];
		$e6_2 = $form_data['e6_2'];
		$e6_3 = $form_data['e6_3'];
		$e6_4 = $form_data['e6_4'];
		$e6_5 = $form_data['e6_5'];
		$e6_6 = $form_data['e6_6'];
		$e6_7 = $form_data['e6_7'];
		$e6_8 = $form_data['e6_8'];
		
		$e7_1 = $form_data['e7_1'];
		$e7_2 = $form_data['e7_2'];
		$e7_3 = $form_data['e7_3'];
		
		$e8_1 = $form_data['e8_1'];
		$e8_2 = $form_data['e8_2'];
		$e8_3 = $form_data['e8_3'];
		$e8_4 = $form_data['e8_4'];
		$e8_5 = $form_data['e8_5'];
		$e8_6 = $form_data['e8_6'];
		$e8_7 = $form_data['e8_7'];
		
		$e9_1 = $form_data['e9_1'];
		$e9_2 = $form_data['e9_2'];
		$e9_3 = $form_data['e9_3'];
		$e9_4 = $form_data['e9_4'];
		
		$f1_1 = $form_data['f1_1'];
		$f1_2 = $form_data['f1_2'];
		$f1_3 = $form_data['f1_3'];
		$f1_4 = $form_data['f1_4'];
		$f1_5 = $form_data['f1_5'];
		
		$g1_1 = $form_data['g1_1'];
		$g1_2 = $form_data['g1_2'];
		
		$h1_1 = $form_data['h1_1'];
		$h1_2 = $form_data['h1_2'];
		$h1_3 = $form_data['h1_3'];
		$h1_4 = $form_data['h1_4'];
		
		$pengesahan_kpi = $form_data['pengesahan'];
		
		$section_a_total = $a1_1 + $a1_2 + $a1_3 + $a1_4 + $a1_5 + $a1_6 + $a1_7 + $a1_8 + $a1_9 + $a2_1 + $a2_2 + $a2_3 + $a2_4 + $a3_1 + $a3_2 + $a4_1 + $a4_2 + $a4_3 + $a4_4 + $a4_5 + $a5_1 + $a5_2 + $a5_3 + $a5_4 + $a6_1 +$a6_2 + $a6_3 + $a7_1 + $a7_2 + $a7_3 + $a7_4 ;
		
		$section_b_total = $b1_1 + $b1_2 + $b1_3 + $b1_4 + $b2_1 + $b2_2 + $b3_1 + $b3_2 + $b4_1 + $b4_2 + $b5_1 + $b5_2 + $b5_3 + $b5_4 + $b6_1 + $b6_2 + $b6_3 + $b6_4 + $b6_5 + $b6_6 + $b6_7 + $b7_1  ;
		
		$section_c_total =  $c1_1 + $c1_2 + $c1_3 + $c1_4 + $c1_5 + $c1_6 + $c1_7 ;
		
		$section_d_total =  $d1_1 + $d1_2 + $d1_3 + $d1_4 + $d1_5 + $d1_6 + $d1_7 + $d1_8 + $d1_9 + $d1_10 ;
		
		$section_e_total =  $e1_1 + $e1_2 + $e1_3 + $e1_4 + $e1_5 + $e1_6 + $e1_7 + $e2_1 + $e2_2 + $e2_3 + $e2_4 + $e2_5 + $e2_6 + $e2_7 + $e2_8 + $e2_9 + $e2_10 + $e2_11 + $e2_12 + $e2_13 + $e2_14 + $e2_15 + $e2_16 + $e2_17 + $e2_18 + $e2_19 + $e2_20 + $e3_1 + $e3_2 + $e3_3 + $e3_4 + $e3_5 + $e3_6 + $e3_7 + $e4_1 + $e4_2 + $e4_3 + $e4_4 + $e4_5 + $e4_6 + $e5_1 + $e5_2 + $e5_3 + $e5_4 + $e5_5 + $e5_6 + $e6_1 + $e6_2 + $e6_3 + $e6_4 + $e6_5 + $e6_6 + $e6_7 + $e6_8 + $e7_1 + $e7_2 + $e7_3 + $e8_1 + $e8_2 + $e8_3 + $e8_4 + $e8_5 + $e8_6 + $e8_7 + $e9_1 + $e9_2 + $e9_3 + $e9_4 ; 
		
		$section_f_total = $f1_1 + $f1_2 + $f1_3 + $f1_4 + $f1_5 ;  
		
		$section_g_total = $g1_1 + $g1_2;
		
		$section_h_total = $h1_1 + $h1_2 + $h1_3 + $h1_4;
		
		$totalquestion = 149;
		
		$marking_total = $section_a_total + $section_b_total + $section_c_total + $section_d_total + $section_e_total + $section_f_total + $section_g_total + $section_h_total ;
		
		$dummy_score = ($marking_total * 100);
		
		$percentage_score = ($dummy_score / $totalquestion);

		 

		if ($percentage_score >= 90) { 
    		
			$gred = "Cemerlang";
		}
		elseif ($percentage_score >= 70) {
    		$gred = "Baik";
		} 
		elseif ($percentage_score >= 51) {
    		$gred = "Sederhana";
		}
		elseif ($percentage_score >= 31) {
    		$gred = "Lemah";
		}
		else {
    		$gred = "Sangat Lemah";
		}

		
		$query = $this->db->query("INSERT INTO penilaian_khusus (id_ptj, bil_audit, skop_audit, program_pusat, bilik_aras, lokasi, zon, a1_1, a1_2, a1_3, a1_4, a1_5, a1_6, a1_7, a1_8, a1_9, a2_1, a2_2, a2_3, a2_4, a3_1, a3_2, a4_1, a4_2, a4_3, a4_4, a4_5,
		a5_1, a5_2, a5_3, a5_4, a6_1, a6_2, a6_3, a7_1, a7_2, a7_3, a7_4, b1_1, b1_2, b1_3, b1_4, b2_1, b2_2, b3_1, b3_2, b4_1, b4_2, b5_1, b5_2, b5_3, b5_4, b6_1, b6_2, b6_3, b6_4, b6_5, b6_6, b6_7, b7_1,
		c1_1, c1_2, c1_3, c1_4, c1_5, c1_6, c1_7, d1_1, d1_2, d1_3, d1_4, d1_5, d1_6, d1_7, d1_8, d1_9, d1_10,
		e1_1, e1_2, e1_3, e1_4, e1_5, e1_6, e1_7,  e2_1, e2_2, e2_3, e2_4, e2_5, e2_6, e2_7, e2_8, e2_9, e2_10, e2_11, e2_12, e2_13, e2_14, e2_15, e2_16, e2_17, e2_18, e2_19, e2_20,
		e3_1, e3_2, e3_3, e3_4, e3_5, e3_6, e3_7, e4_1, e4_2, e4_3, e4_4, e4_5, e4_6, e5_1, e5_2, e5_3, e5_4, e5_5, e5_6, e6_1, e6_2, e6_3, e6_4, e6_5, e6_6, e6_7, e6_8, e7_1, e7_2, e7_3,
		e8_1, e8_2, e8_3, e8_4, e8_5, e8_6, e8_7, e9_1, e9_2, e9_3, e9_4, f1_1, f1_2, f1_3, f1_4, f1_5, g1_1, g1_2, h1_1, h1_2, h1_3, h1_4, jumlah_markah_a, jumlah_markah_b, jumlah_markah_c, jumlah_markah_d, jumlah_markah_e, jumlah_markah_f, jumlah_markah_g, jumlah_markah_h, jumlah_markah_keseluruhan, peratus_markah, gred , pengesahan_kpi)
		VALUES ('$ptj_group' ,'$bil_audit','$skop_audit','$program_pusat','$bilik_aras','$lokasi','$zon','$a1_1','$a1_2','$a1_3','$a1_4','$a1_5','$a1_6','$a1_7','$a1_8','$a1_9',
		'$a2_1','$a2_2','$a2_3','$a2_4','$a3_1','$a3_2','$a4_1','$a4_2','$a4_3','$a4_4','$a4_5','$a5_1','$a5_2','$a5_3','$a5_4','$a6_1','$a6_2','$a6_3',
		'$a7_1','$a7_2','$a7_3','$a7_4','$b1_1','$b1_2','$b1_3', '$b1_4', '$b2_1','$b2_2','$b3_1','$b3_2','$b4_1','$b4_2','$b5_1','$b5_2','$b5_3','$b5_4',
		'$b6_1','$b6_2','$b6_3','$b6_4','$b6_5','$b6_6','$b6_7','$b7_1','$c1_1','$c1_2','$c1_3','$c1_4','$c1_5','$c1_6','$c1_7','$d1_1','$d1_2','$d1_3','$d1_4','$d1_5','$d1_6','$d1_7','$d1_8','$d1_9','$d1_10',
		'$e1_1','$e1_2','$e1_3','$e1_4','$e1_5','$e1_6','$e1_7', '$e2_1','$e2_2','$e2_3','$e2_4','$e2_5','$e2_6','$e2_7','$e2_8','$e2_9','$e2_10',
		'$e2_11','$e2_12','$e2_13','$e2_14','$e2_15','$e2_16','$e2_17','$e2_18','$e2_19','$e2_20','$e3_1','$e3_2','$e3_3','$e3_4','$e3_5','$e3_6','$e3_7',
		'$e4_1','$e4_2','$e4_3','$e4_4','$e4_5','$e4_6','$e5_1','$e5_2','$e5_3','$e5_4','$e5_5','$e5_6',
		'$e6_1','$e6_2','$e6_3','$e6_4','$e6_5','$e6_6','$e6_7','$e6_8','$e7_1','$e7_2','$e7_3','$e8_1','$e8_2','$e8_3','$e8_4','$e8_5','$e8_6','$e8_7','$e9_1','$e9_2','$e9_3','$e9_4',
		'$f1_1','$f1_2','$f1_3','$f1_4','$f1_5', '$g1_1','$g1_2','$h1_1','$h1_2','$h1_3','$h1_4', '$section_a_total','$section_b_total','$section_c_total','$section_d_total','$section_e_total','$section_f_total','$section_g_total','$section_h_total','$marking_total','$percentage_score', '$gred','$pengesahan_kpi')")  ;
		
	}
	
	function insert_photo_data($data){
		
		$dirname  		= $data['path_folder'];
		$filename 		= $data['filename'];
		$new_file_name  = $data['new_file_name'];
		$query = $this->db->query("INSERT INTO pemantauan (id_ptj, kpi_id,nama_asal_gambar_pembuktian, nama_unik_gambar_pembuktian, path_folder, catatan_pembuktian)
									VALUE('','','$filename','$new_file_name','$dirname','')");
	}
	
	function select_photo(){
		$query = $this->db->query("SELECT * FROM pemantauan");
		$row = $query->result_array();
		return $row;
	}
	
	function selectptj(){
		$query = $this->db->query("SELECT * FROM ptj");
		$row = $query->result_array();
		return $row;
	}
	
	function insert_ptj($form_data){
		
		$namaptj = $form_data['namaptj'];
		$alamatptj = $form_data['alamatptj'];
		$jenisptj = $form_data['jenisptj'];
		$query = $this->db->query("INSERT INTO ptj(nama_ptj,alamat_ptj,jenis_ptj) VALUE('$namaptj','$alamatptj','$jenisptj')");
	}
	
	function display_report($form_data){
		
		$jenispenilaian = $form_data['jenispenilaian'];
		$bil_audit = $form_data['bil_audit'];
		$ptj_group = $form_data['ptj_group'];
		
		switch($jenispenilaian){
			case "1":
			$table_name = "penilaian_umum";
			break;
			
			case "2":
			$table_name = "penilaian_khusus";
			break;
		}
		
		$query = $this->db->query("SELECT * FROM $table_name WHERE bil_audit = '$bil_audit' AND id_ptj = '$ptj_group'");
		$row = $query->result_array();
		return $row;
	}
	
	function display_ptj_group_list(){
		$query = $this->db->query("SELECT * FROM ptj");
		$row = $query->result_array();
		return $row;
	}
	
	
	function display_kpi(){
		$query = $this->db->query("SELECT * FROM eosh_kpi");
		$row = $query->result_array();
		return $row;
	}
	
	function display_pi(){
		$query = $this->db->query("SELECT * FROM eosh_pi");
		$row = $query->result_array();
		return $row;
	}
	
	function display_ptj(){
		$query = $this->db->query("SELECT * FROM ptj");
		$row = $query->result_array();
		return $row;
	}
	
	function display_rosh(){
		$query = $this->db->query("SELECT * FROM eosh_rosh");
		$row = $query->result_array();
		return $row;
	}
	
	function delete_ptj($id_ptj){
		$query = $this->db->query("DELETE FROM ptj WHERE id_ptj = '$id_ptj'");
		//$row = $query->result_array();
		//return $row;
	}
	
	function get_ptj_by_id($id_ptj){
		$query = $this->db->query("SELECT * FROM ptj WHERE id_ptj = '$id_ptj'");
		$row = $query->result_array();
		return $row;
	}
	
	function get_ptj_kpi_nama($kpi_id){
		$query = $this->db->query("SELECT kpi_nama FROM eosh_kpi WHERE id_ptj = '$kpi_id'");
		$row = $query->row('kpi_nama');
		return $row;
	}
	
	function update_ptj($form_data){
		$nama_ptj = $form_data['namaptj'];
		$alamat_ptj = $form_data['alamatptj'];
		$jenis_ptj = $form_data['jenisptj'];
		$id_ptj = $form_data['id_ptj'];
		
		$query = $this->db->query("UPDATE ptj SET nama_ptj = '$nama_ptj', alamat_ptj = '$alamat_ptj', jenis_ptj = '$jenis_ptj' WHERE id_ptj = '$id_ptj'");
		//$row = $query->result_array();
		//return $row;
	}
	
	function get_kpi_by_id($id_kpi){
		$query = $this->db->query("SELECT * FROM eosh_kpi WHERE kpi_id = '$id_kpi'");
		$row = $query->result_array();
		return $row;
	}
	
	function delete_kpi($id_kpi){
		$query = $this->db->query("DELETE FROM eosh_kpi WHERE kpi_id = '$id_kpi'");
		//$row = $query->result_array();
		//return $row;
	}
	
	function delete_pi($id_pi){
		$query = $this->db->query("DELETE FROM eosh_pi WHERE pi_id = '$id_pi'");
		//$row = $query->result_array();
		//return $row;
	}
	
	function delete_rosh($id_rosh){
		$query = $this->db->query("DELETE FROM eosh_rosh WHERE rosh_id = '$id_rosh'");
		//$row = $query->result_array();
		//return $row;
	}
	
	function update_kpi($form_data){
		$nama_awal_kpi = $form_data['register[first_name]'];
		$nama_akhir_kpi = $form_data['register[last_name]'];
		$jawatan_kpi = $form_data['register[position]'];
		$emel_kpi = $form_data['register[email]'];
		$phonepre_kpi = $form_data['register[hp_prefix]'];
		$phonenumber_kpi = $form_data['register[hp_number]'];
		$ukmper_kpi = $form_data['register[ukmper]'];
		$bilik_kpi = $form_data['register[bilik]'];
		$statuslantikan_kpi = $form_data['register[status_lantikan]'];
		
		
		$profile_data = array(
				'upro_first_name' 	=> $nama_awal_kpi,
				'upro_last_name' 	=> $nama_akhir_kpi,
				'upro_phone' 		=> $hp_number = '6'.$phonepre_kpi.$phonenumber_kpi,
				'upro_position'		=> $jawatan_kpi,
				'kpi_nama'			=> $register['first_name']." ".$register['last_name'],
				'kpi_ukmper'		=> $ukmper_kpi,
				'kpi_jawatan'		=> $jawatan_kpi,
				'kpi_bilik'			=> $bilik_kpi,
				'kpi_emel'			=> $emel_kpi,
				'kpi_status_lantikan'=>$statuslantikan_kpi,
				'kpi_tel'			=> $hp_number = '6'.$phonepre_kpi.$phonenumber_kpi,
			);
		
		$this->flexi_auth->update_user($kpi_id, $profile_data);
		
		
	}
	
}

/* End of file demo_auth_admin_model.php */
/* Location: ./application/models/demo_auth_admin_model.php */