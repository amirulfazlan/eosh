<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
    private $table_meta = 'johor_meta';
	 
    function __construct() {
        /* Call the Model constructor */
        parent::__construct();
    }
	
	
	
	//Get App Setting Info
	
	function app_info()
	{
		$query = $this->db->query("SELECT * FROM ".$this->table_meta."");
        $rows = $query->result_array();
        return $rows;
	}
	
	public function meta_list(){
        $query = $this->db->query('SELECT * FROM johor_meta');
        $rows = $query->result_array();
        return $rows;
    }
}

/* End of file demo_auth_admin_model.php */
/* Location: ./application/models/demo_auth_admin_model.php */