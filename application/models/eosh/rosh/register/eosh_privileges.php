<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eosh_privileges extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Account Registration
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * register_account
	 * Create a new user account. 
	 * Then if defined via the '$instant_activate' var, automatically log the user into their account.
	 */
	function register_account()
	{
		$this->load->library('form_validation');
		$register = $this->input->post('register');


		// Set validation rules.
		// The custom rules 'identity_available' and 'validate_password' can be found in '../libaries/MY_Form_validation.php'.
		
		$validation_rules = array(
			array('field' => 'register[first_name]', 'label' => 'First Name', 'rules' => 'required'),
			array('field' => 'register[last_name]', 'label' => 'Last Name', 'rules' => 'required'),
			array('field' => 'register[hp_number]', 'label' => 'Phone Number', 'rules' => 'required'),
			array('field' => 'register[email]', 'label' => 'Email Address', 'rules' => 'required|valid_email|identity_available'),
			array('field' => 'register[ukmper]', 'label' => 'UKMPER', 'rules' => 'required|min_length[4]|identity_available'),
			array('field' => 'register[password]', 'label' => 'Password', 'rules' => 'required|validate_password'),
		/*	array('field' => 'register[confirm_password]', 'label' => 'Confirm Password', 'rules' => 'required|matches[register[password]]')*/
		);

		$this->form_validation->set_rules($validation_rules);

		// Run the validation.
		if ($this->form_validation->run())
		{
			// Get user login details from input.
			$email 	  = $register['email'];
			$username = $register['ukmper'];
			$password = $register['password'];
			
			// Get user profile data from input.
			// You can add whatever columns you need to customise user tables.
			$profile_data = array(
				'upro_first_name' 	=> $register['first_name'],
				'upro_last_name' 	=> $register['last_name'],
				'upro_phone' 		=> $hp_number = '6'.$register['hp_prefix'].$register['hp_number'],
				'upro_extension'	=> $register['office_extension'],
				'upro_position'		=> $register['position'],
				'rosh_nama'			=> $register['first_name']." ".$register['last_name'],
				'rosh_ukmper'		=> $register['ukmper'],
				'rosh_jawatan'		=> $register['position']
			);
			
			// Set whether to instantly activate account.
			// This var will be used twice, once for registration, then to check if to log the user in after registration.
			$instant_activate = TRUE;
	
			// The last 2 variables on the register function are optional, these variables allow you to:
			// #1. Specify the group ID for the user to be added to (i.e. 'Moderator' / 'Public'), the default is set via the config file.
			// #2. Set whether to automatically activate the account upon registration, default is FALSE. 
			// Note: An account activation email will be automatically sent if auto activate is FALSE, or if an activation time limit is set by the config file.
			
			$sql_select 			= 'ugrp_id';
			$sql_where['ugrp_name'] = $register['user_group'];
			$user_group 			= $this->flexi_auth->get_groups($sql_select, $sql_where)->row()->ugrp_id;
			
			$response = $this->flexi_auth->insert_user($email, $username, $password, $profile_data, $user_group, $instant_activate);

			if ($response)
			{
				// This is an example 'Welcome' email that could be sent to a new user upon registration.
				// Bear in mind, if registration has been set to require the user activates their account, they will already be receiving an activation email.
				// Therefore sending an additional email welcoming the user may be deemed unnecessary.
				$email_data = array('identity' => $email, 'first_name' => $register['first_name'], 'last_name' => $register['last_name'],'username' =>$username, 'password' => $password );
				$this->flexi_auth->send_email($email, 'Welcome', 'registration_welcome.tpl.php', $email_data);
				// Note: The 'registration_welcome.tpl.php' template file is located in the '../views/includes/email/' directory defined by the config file.
				
				###+++++++++++++++++###
				
				// Save any public status or error messages (Whilst suppressing any admin messages) to CI's flash session data.
				$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
				
				// This is an example of how to log the user into their account immeadiately after registering.
				// This example would only be used if users do not have to authenticate their account via email upon registration.
			/*	if ($instant_activate && $this->flexi_auth->login($email, $password))
				{
					// Redirect user to public dashboard.
					redirect('auth_public/dashboard');
				}*/
				
				// Redirect user to login page
				redirect('eosh_admin_register/eosh_register_rosh');
			}
		}

		// Set validation errors.
		$this->data['message'] = validation_errors('<p class="error_msg">', '</p>');

		return FALSE;
	}
	
	

}
/* End of file Eosh_previleges.php */
/* Location: ./application/models/eosh/admin/register/Eosh_previleges.php */